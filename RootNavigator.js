import { createAppContainer ,createSwitchNavigator} from 'react-navigation';
    import { createStackNavigator ,Header} from 'react-navigation-stack';
    import { createDrawerNavigator } from 'react-navigation-drawer';
     import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
  import React from 'react';
import { colors,urls,fonts} from './src/Globals';
import {StatusBar,TouchableOpacity,Image,Text,Platform,ToastAndroid} from 'react-native';


import Splash from './src/screens/Splash';
import Login from './src/screens/Login';
import Register from './src/screens/Register';
import Forgot from './src/screens/Forgot';
import Otp from './src/screens/Otp';
import ResetPassword from './src/screens/ResetPassword';
import Home from './src/screens/Home';
import Drawer from './src/screens/Drawer';
import Welcome from './src/screens/Welcome';
import SellerProfile from './src/screens/SellerProfile';
import Discover from './src/screens/Discover';
import MyProfile from './src/screens/MyProfile';
import Vibes from './src/screens/Vibes';
import Vibing from './src/screens/Vibing';
import News from './src/screens/News';
import EditProfile from './src/screens/EditProfile';
import ContactAdmin from './src/screens/ContactAdmin';
import ImageView from './src/screens/ImageView';
import VideoView from './src/screens/VideoView';
import Upgrades from './src/screens/Upgrades';
import NewsDetail from './src/screens/NewsDetail';
import AddNews from './src/screens/AddNews';
import Packages from './src/screens/Packages';
import AddPitch from './src/screens/AddPitch';
import PostImageDetail from './src/screens/PostImageDetail';
import InvestmentHub from './src/screens/InvestmentHub';
import InvestmentHubDetails from './src/screens/InvestmentHubDetails';
import Chats from './src/screens/Chats';
import ChatPackages from './src/screens/ChatPackages';
import ChatLists from './src/screens/ChatLists';
import Requests from './src/screens/Requests';
import Search from './src/screens/Search';
import NewsPackages from './src/screens/NewsPackages';
import EditPost from './src/screens/EditPost';
import Transactions from './src/screens/Transactions';
import Legal from './src/screens/Legal';

    console.disableYellowBox = true;

      const loginStack = createStackNavigator({
        Login : { screen: Login },
        Register : { screen: Register},
        Forgot : { screen: Forgot},
        Otp : { screen: Otp},
        ResetPassword : { screen: ResetPassword},
        Welcome : { screen: Welcome},
        



      }, {
       // headerMode: 'none',
           initialRouteName: 'Welcome',
           navigationOptions: {
            gesturesEnabled: false
          }
      })


      const sellerStack = createStackNavigator({
      
        SellerProfile :{ screen: SellerProfile},
       
      }, {
         
           initialRouteName: 'SellerProfile',
           navigationOptions:()=>{
            return {
              tabBarVisible:false,
            };
         }
      })


      const vibesStack = createStackNavigator({
        Vibes :{ screen: Vibes},
        SellerProfile :{ screen: SellerProfile,
          navigationOptions: {
            tabBarVisible: false
          }},
       
      }, {
         
           initialRouteName: 'Vibes',
          //  navigationOptions: {
          //   gesturesEnabled: true
          // }
      })

 const vibesTabs = createMaterialTopTabNavigator({
  Vibes: {screen : Vibes,
    navigationOptions: ({ navigation }) => ({ 
      tabBarLabel: navigation.getParam('title','Vibes')
    }
    )},
  Vibing:  {screen : Vibing,
    navigationOptions: ({ navigation }) => ({ 
      tabBarLabel: navigation.getParam('title','Vibing')
    }
    )},

},
{
  initialRouteName: 'Vibes',
  //initialRouteName: navigation.getParam('route_name','Vibes') ,
  labelStyle: {
    fontSize: 12,
  },
  tabStyle: {
    width: 100,
   
  },
  tabBarOptions: {
    activeTintColor:colors.color_primary,
    keyboardHidesTabBar:true,
    inactiveTintColor:'black',
      style: {
          backgroundColor: 'white',
          // marginTop: StatusBar.currentHeight 
      }
  },

  tabBarPosition: 'top',
  swipeEnabled: true,
  animationEnabled: true,

})


       const vibeHeaderStack = createStackNavigator({


        VibeHeader :{ screen: vibesTabs,

         navigationOptions: ({ navigation }) => ({  //navigation is used for setting custiom title from prev
            headerTitleStyle: {  textAlign:"left" },
            headerLayoutPreset: 'center',
            title:navigation.getParam('name'),

            headerLeft: (
            <TouchableOpacity onPress={() => {navigation.navigate('MyProfile')}}>
            <Image style={{width: 25, height: 25,margin:10}} 
             source={require('./src/assets/left-arrow-white.png')} />
            </TouchableOpacity>

            ),
            headerRight:(
                   <TouchableOpacity></TouchableOpacity>
            ),
              headerStyle:{
                backgroundColor:colors.color_primary,
                elevation:10,paddingTop:Platform.OS === 'ios' ? 0 :  30,
                //paddingBottom:Header.HEIGHT / 2,
                paddingBottom:25,
              },
               headerTitleStyle: { color: 'white' ,fontSize:16},

    }),
     },
        SellerProfile :{ screen: SellerProfile,
        },
       
        
      }, {
           headerMode: 'screen',
           initialRouteName: 'VibeHeader',
          
      })

      const requestStack = createStackNavigator({
        Requests :{ screen: Requests},
        SellerProfile :{ screen: SellerProfile},
       
      }, {
         
           initialRouteName: 'Requests',
           navigationOptions: {
            gesturesEnabled: true
          }
      })

      const searchStack = createStackNavigator({
        Search :{ screen: Search},
        SellerProfile :{ screen: SellerProfile},
       
      }, {
         
           initialRouteName: 'Search',
           navigationOptions: {
            gesturesEnabled: true
          }
      })


      const cannabisBusinessStack = createStackNavigator({
        Upgrades :{ screen: Upgrades},
        Packages :{ screen: Packages},
        AddPitch :{ screen: AddPitch},
       
      }, {
         
           initialRouteName: 'Upgrades',
           navigationOptions: {
            gesturesEnabled: true
          }
      })



       const myProfileStack = createStackNavigator({
        MyProfile :{ screen: MyProfile},
        MyVibes :{ screen: vibeHeaderStack,
          navigationOptions : ({ navigation }) => ({  
            //navigation is used for setting custom title from previous screen
          
            header: () => null
       })},
         Discover :{ screen: Discover},
         EditProfile :{ screen: EditProfile},
         ImageView :{ screen: ImageView},
         VideoView :{ screen: VideoView},
         Upgrades :{ screen: Upgrades},
         Packages :{ screen: Packages},
         AddPitch :{ screen: AddPitch},
         Requests :{ screen: requestStack ,
          navigationOptions : ({ navigation }) => ({  
         
          header: () => null
       })},
       
       
        
      }, {
          // headerMode: 'none',
           initialRouteName: 'MyProfile',
           navigationOptions: {
            gesturesEnabled: true
          }
      })

     

      
      const chatStack = createStackNavigator({
        ChatLists : {screen : ChatLists},
        Chats :{ screen: Chats},
        ImageView :{ screen: ImageView},
       
       
      }, {
         
           initialRouteName: 'ChatLists',
           navigationOptions: {
            gesturesEnabled: true
          }
      })

      const homeStack = createStackNavigator({
        Home :{ screen: Home},
        SellerProfile :{ screen: SellerProfile},
        EditPost :{ screen: EditPost},
        Discover :{ screen: Discover},
        Requests :{ screen: requestStack ,
          navigationOptions : ({ navigation }) => ({  
         
          header: () => null
       })},
       Search :{ screen: searchStack ,
        navigationOptions : ({ navigation }) => ({  
       
        header: () => null
     })},
        PostImageDetail :{ screen: PostImageDetail},
        Chats :{ screen: chatStack ,
          navigationOptions : ({ navigation }) => ({  
          header: () => false
        })},
        
        ChatPackages : {screen : ChatPackages},
        ImageView :{ screen: ImageView},
        VideoView :{ screen: VideoView},
        ChatOutside : {screen : Chats},

        
      }, {
          //  headerMode: 'none',
           initialRouteName: 'Home',
           //headerLayoutPreset: 'center',
           navigationOptions: {
            gesturesEnabled: true
          }
      })
      const contactAdminStack = createStackNavigator({
        ContactAdmin :{ screen: ContactAdmin},
      
        
      }, {
           //headerMode: 'none',
           initialRouteName: 'ContactAdmin',
           navigationOptions: {
            gesturesEnabled: true
          }
      })


      const legalStack = createStackNavigator({
        Legal :{ screen: Legal},
      
        
      }, {
           //headerMode: 'none',
           initialRouteName: 'Legal',
           navigationOptions: {
            gesturesEnabled: true
          }
      })



      const investmentHubStack = createStackNavigator({
        InvestmentHub :{ screen: InvestmentHub},
        InvestmentHubDetails :{ screen: InvestmentHubDetails},
      
        
      }, {
           //headerMode: 'none',
           initialRouteName: 'InvestmentHub',
           navigationOptions: {
            gesturesEnabled: true
          }
      })



      const entrepreneurStack = createStackNavigator({
        Entrepreneur :{ screen: AddPitch},
       
      
        
      }, {
           //headerMode: 'none',
           initialRouteName: 'Entrepreneur',
           navigationOptions: {
            gesturesEnabled: true
          }
      })




      
      



      const newsStack = createStackNavigator({
        News :{ screen: News},
        NewsDetail :{ screen: NewsDetail},
        AddNews :{ screen: AddNews},
        NewsPackages :{ screen: NewsPackages},
        
      
        
      }, {
          // headerMode: 'none',
           initialRouteName: 'News',
           navigationOptions: {
            gesturesEnabled: true
          }
      })

      const transactionsStack = createStackNavigator({
        Transactions :{ screen: Transactions},
       
      
        
      }, {
          // headerMode: 'none',
           initialRouteName: 'Transactions',
           navigationOptions: {
            gesturesEnabled: true
          }
      })


      const drawer = createDrawerNavigator({
         HomePage :{ screen: homeStack},
         MyProfile :{ screen: myProfileStack},
         ContactAdmin :{ screen: contactAdminStack},
         News :{ screen: newsStack},
         InvestmentHub :{ screen: investmentHubStack},
         Entrepreneur:{ screen: entrepreneurStack},
         Upgrades : {screen : cannabisBusinessStack},
         Transactions : {screen : transactionsStack},
         Legal :{ screen: legalStack},
      }
      , {
         initialRouteName: 'HomePage',
         gesturesEnabled: true,
         mode: 'modal',
         contentComponent: props => <Drawer {...props}/>
      })


    const AppStack = createSwitchNavigator({
      Splash : { screen: Splash },
      Login : { screen: loginStack },
      HomePage : { screen:drawer},
    
    }, {
         headerMode: 'none',
         initialRouteName: 'Splash',
         navigationOptions: {
          gesturesEnabled: false
        }
    
    })
    
      
  const RootNavigator = createAppContainer(AppStack)
 export default RootNavigator;