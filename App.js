import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import store from './src/store/store';
import { Provider } from 'react-redux';
import React from 'react';
import RootNavigator from './RootNavigator';
import firebase from 'react-native-firebase';
import type, { Notification, NotificationOpen } from 'react-native-firebase';
import { NavigationActions ,withNavigation} from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from './NavigationService';
//const store = configureStore()

export default class App extends React.Component {

	constructor(props) {
		super(props);
	}



// 	for data-only messages:
// App in foreground : onMessage triggered

// App in background/App closed : Background Handler (HeadlessJS)

// for notification + data messages:
// App in foreground : onNotification triggered

// App in background/App closed : onNotificationOpened triggered if the notification is tapped

	


	  async createNotificationListeners() {

		const channel = new firebase.notifications.Android.Channel(
		  'notification_channel_name', // To be Replaced as per use
		  'Notifications', // To be Replaced as per use
		  firebase.notifications.Android.Importance.Max
	  ).setDescription('A Channel To manage the notifications related to Application');
	  firebase.notifications().android.createChannel(channel);




			/*
			* Triggered when a particular notification has been received in foreground
			* */
			this.notificationListener = firebase.notifications().onNotification((notification) => {
				const { title, body } = notification;
			this.display(notification)

			});


			/*
			* If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
			* */
			const notificationOpen = await firebase.notifications().getInitialNotification();

			if (notificationOpen) {
			 const { title, body } = notificationOpen.notification;
			 //console.log("NOTIFICATION CLICK",JSON.stringify(notificationOpen.notification))
        //this.showAlert(title, body);
		// ToastAndroid.show("Called when notification is clicked",ToastAndroid.LONG)
		


		


		var titles = notificationOpen.notification._title

		var result = notificationOpen.notification._data
		var seen = [];
		console.log("NOTIFICATION CLICK BACKGROUND",JSON.stringify(notificationOpen, (key, val) => {
			if (val != null && typeof val == "object") {
				 if (seen.indexOf(val) >= 0) {
					 return;
				 }
				 seen.push(val);
			 }
			 return val;
		 }))


		 if( titles === undefined){

			if(result.to_id == undefined){
				setTimeout(() => {
			
					NavigationService.navigate("Requests");
	
					// this.props.navigation.navigate('HomePage')
			  
			  
					  }, 2000)
			}
			else{

			
				 
				   
				let obj={
					'to_id':result.to_id,
					'to_name':result.to_name,
					'to_image':result.to_image,
					'to_status':'offline',
					'to_typing':false
				}
				  //this.props.navigation.navigate("Chats",{result:obj})
				  
					console.log("PASSING",JSON.stringify(obj))
				  
					setTimeout(() => {
				
					 NavigationService.navigate("ChatOutside",{result:obj});
	 
					 // this.props.navigation.navigate('HomePage')
			   
			   
					   }, 2000)
	
	
			}
			}
			 
		 
		 else{
			if(titles.includes('requested') ){
				//UNDEFINED BECAUSE ON BACKGROUND CLICK NO TITLE IS COMIN
			console.log("GOING RECENTS BACKGROUND")
			setTimeout(() => {
			
				NavigationService.navigate("Requests");

				// this.props.navigation.navigate('HomePage')
		  
		  
				  }, 2000)
		}

		else{

			let obj={
				'to_id':result.to_id,
				'to_name':result.to_name,
				'to_image':result.to_image,
				'to_status':'offline',
				'to_typing':false
			}
			  //this.props.navigation.navigate("Chats",{result:obj})
			  
				console.log("PASSING",JSON.stringify(obj))
			  
				setTimeout(() => {
			
				 NavigationService.navigate("ChatOutside",{result:obj});
 
				 // this.props.navigation.navigate('HomePage')
		   
		   
				   }, 2000)


		}


		 }

		

		

			
		firebase.notifications().removeAllDeliveredNotifications()

			



				//Alert.alert("ANdroid")
				// ToastAndroid.show("Called when notification is clicked in background android",ToastAndroid.LONG)

				//take action here

			}


			/*
			* If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
			* */
			this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {

				var seen = [];
			//ToastAndroid.show("this is called when app is open in android",ToastAndroid.LONG)
			console.log("NOTIFICATION CLICK",JSON.stringify(notificationOpen, (key, val) => {
				if (val != null && typeof val == "object") {
					 if (seen.indexOf(val) >= 0) {
						 return;
					 }
					 seen.push(val);
				 }
				 return val;
			 }))

			 firebase.notifications().removeAllDeliveredNotifications()

				 
			
			var title = notificationOpen.notification._title

			 var result = notificationOpen.notification._data

			 if(title.includes('requested')){

				console.log("GOING RECENTS FOREGROUND")
				NavigationService.navigate("Requests");
			 }

			 else{

				//got to chats

				let obj={
					'to_id':result.to_id,
					'to_name':result.to_name,
					'to_image':result.to_image,
					'to_status':'offline'
				}
				  //this.props.navigation.navigate("Chats",{result:obj})
				  
					//console.log("PASSING",JSON.stringify(obj))
				  


			 NavigationService.navigate("Chats",{result:obj});


			 }

			 
				
			
			//Alert.alert("IOS")

			//take action her
			});


			/*
			* Triggered for data only payload in foreground
			* */
			this.messageListener = firebase.messaging().onMessage((message) => {
			//this is called when app is in foregrpund
			this.displayNotification(message)
			});
		}

	displayNotification = (notification) => {
	//ToastAndroid.show("notification inintalss....",ToastAndroid.LONG)
   
   
   
   
		 if (Platform.OS === 'android') {
   
   
			 const localNotification = new firebase.notifications.Notification({
				 sound: 'default',
				 show_in_foreground: true,
			 }).setNotificationId(notification._from)
			 .setTitle(notification._data.result)
			 .setSubtitle(notification.subtitle)
			 .setBody(notification._data.content)
			 .setData(notification.data)
				 .android.setChannelId('notification_channel_name') // e.g. the id you chose above
				 .android.setSmallIcon('logo') // create this icon in Android Studio
				 .android.setColor('#D3D3D3') // you can set a color here
				 .android.setPriority(firebase.notifications.Android.Priority.High);
   
				 //ToastAndroid.show("notification ...",ToastAndroid.LONG)
   
			 firebase.notifications()
				 .displayNotification(localNotification)
				 .catch(err => ToastAndroid.show(JSON.stringify(err),ToastAndroid.LONG));
   
		 }
		 else if (Platform.OS === 'ios') {
		   console.log(notification);
		  // Alert.alert("IOS 2")
		   const localNotification = new firebase.notifications.Notification()
			   .setNotificationId(notification._from)
			   .setTitle(notification._data.result)
			 .setSubtitle(notification.subtitle)
			 .setBody(notification._data.content)
			 .setData(notification.data)
			   //.ios.setBadge(notification.ios.badge);
   
		   firebase.notifications()
			   .displayNotification(localNotification)
			   .catch(err => console.error(err));
   
	   }
	 }
	 display = (notification) => {
   
	//ToastAndroid.show('sdsd',ToastAndroid.LONG)
   
   console.log(notification)
   
	   if (Platform.OS === 'android') {
		 const { title, body } = notification;
		   const localNotification = new firebase.notifications.Notification({
			   sound: 'default',
			   show_in_foreground: true,
		   }).setNotificationId(notification.notificationId)
		   .setTitle(title)
		   //.setSubtitle(title)
		   .setBody(body)
		   .setData(notification.data)
			   .android.setChannelId('notification_channel_name') // e.g. the id you chose above
			   .android.setSmallIcon('logo') // create this icon in Android Studio
			   .android.setColor('#D3D3D3') // you can set a color here
			   .android.setPriority(firebase.notifications.Android.Priority.High);
   
		   firebase.notifications()
			   .displayNotification(localNotification)
			   .catch(err => ToastAndroid.show(err.message,ToastAndroid.LONG));
   
	   }
	   else if (Platform.OS === 'ios') {
		 //Alert.alert("called when app is in foreground ios")
		 const { title, body } = notification;
		 const localNotification = new firebase.notifications.Notification()
			 .setNotificationId(notification.notificationId)
			 .setTitle(title)
		   //.setSubtitle(title)
		   .setBody(body)
		   .setData(notification.data)
			// .ios.setBadge(notification.ios.badge);
   
		 firebase.notifications()
			 .displayNotification(localNotification)
			 .catch(err => console.error(err));
   
	 }
   	}

	componentDidMount() {
		this.createNotificationListeners(); //add this line

	}

	componentWillUnmount() {
		this.notificationListener();
		this.notificationOpenedListener();
	  }

	render() {
		return (
			<Provider store={store}>
				<RootNavigator 
					ref={navigatorRef => {
						NavigationService.setTopLevelNavigator(navigatorRef);
						}}
				/>
			</Provider>
		);
	}
}

//NotificationOpen Object

// {
// 	"action": null,
// 	"notification": {
// 	  "_data": {
		
// 	  },
// 	  "_notificationId": "0:1576263650242095%29af227629af2276",
// 	  "_sound": "default",
// 	  "_title": "Weedify has requested to vibing you!",
// 	  "_android": {
// 		"_actions": [
		  
// 		],
// 		"_channelId": "notification_channel_name",
// 		"_color": "#D3D3D3",
// 		"_people": [
		  
// 		],
// 		"_priority": 1,
// 		"_smallIcon": {
// 		  "icon": "logo"
// 		}
// 	  },
// 	  "_ios": {
// 		"_attachments": [
		  
// 		]
// 	  }
// 	}
//   }



//Notifcation Object



// {
// 	"_android": {
// 	  "_actions": [
		
// 	  ],
// 	  "_autoCancel": undefined,
// 	  "_badgeIconType": undefined,
// 	  "_bigPicture": undefined,
// 	  "_bigText": undefined,
// 	  "_category": undefined,
// 	  "_channelId": undefined,
// 	  "_clickAction": undefined,
// 	  "_color": undefined,
// 	  "_colorized": undefined,
// 	  "_contentInfo": undefined,
// 	  "_defaults": undefined,
// 	  "_group": undefined,
// 	  "_groupAlertBehaviour": undefined,
// 	  "_groupSummary": undefined,
// 	  "_largeIcon": undefined,
// 	  "_lights": undefined,
// 	  "_localOnly": undefined,
// 	  "_notification": [
// 		Circular
// 	  ],
// 	  "_number": undefined,
// 	  "_ongoing": undefined,
// 	  "_onlyAlertOnce": undefined,
// 	  "_people": [
		
// 	  ],
// 	  "_priority": undefined,
// 	  "_progress": undefined,
// 	  "_remoteInputHistory": undefined,
// 	  "_shortcutId": undefined,
// 	  "_showWhen": undefined,
// 	  "_smallIcon": {
// 		"icon": "ic_launcher"
// 	  },
// 	  "_sortKey": undefined,
// 	  "_tag": undefined,
// 	  "_ticker": undefined,
// 	  "_timeoutAfter": undefined,
// 	  "_usesChronometer": undefined,
// 	  "_vibrate": undefined,
// 	  "_visibility": undefined,
// 	  "_when": undefined
// 	},
// 	"_body": undefined,
// 	"_data": {
	  
// 	},
// 	"_ios": {
// 	  "_attachments": [
		
// 	  ],
// 	  "_notification": [
// 		Circular
// 	  ]
// 	},
// 	"_notificationId": "0:1576263154719241%29af227629af2276",
// 	"_sound": "default",
// 	"_subtitle": undefined,
// 	"_title": "Weedify has requested to vibing you!"
//   }