import React , { useState,useEffect }from 'react';
import { colors, urls } from '../Globals';
import {Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import AsyncStorage from '@react-native-community/async-storage';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';




// itss type definition from Flow, it means that constant App is of type function 
//and it returns ReactNode.

// ReactNode is one of these types: ReactChild | ReactFragment | ReactPortal | boolean | null | undefined

// This means the function App can return, any valid JSX (in react native its anything from View, Text, .etc), ReactFragment, React.Portal, boolean, null, undefined


const PhotoBottomsheet : () => React$Node = (props) => {

 

 // onPress={()=> setCount(count + 1)}

  const [count, setCount] = useState(0);

  
  //component did mount 
  useEffect(() => {
    console.log("Compoennet Did Moiunt")
    console.log("ddddddddd-----",JSON.stringify(props.inputref))

    // componentWillUnmount
    //return a callback in useEffect's 
    //callback argument and it will be called before unmounting
    return () => {
      console.log("Component will unouunt")
   }
  },
  []// passing an empty array as second argument triggers the callback in useEffect only after 
  //the initial render thus replicating `componentDidMount` lifecycle behaviour
  )

 

  return (
    <RBSheet
    ref={ref => {
      props.inputref = ref;
    }}
   // ref={props.ref}
    //ref={props.inputref}
    height={240}
    duration={0}
    customStyles={{
      container: {
        justifyContent: "center",
        alignItems: "center",
        borderTopLeftRadius:15,
        borderTopRightRadius:15
      },
      // wrapper:{
      //   backgroundColor:'grey'
      // }
    }}
    animationType='fade'
   
    minClosingHeight={10}

    >
      <View style={styles.photosheetmain}>
      
      
      <Image source={props.photo}
                              style={styles.sheetimage}  />
      
      </View>

      <Text style={styles.sheetlabel}>Do you want to add this image to add to post ?</Text>

      <View style={styles.sheetbuttonparent}>

          <View style={styles.sheetbuttonview}>
          <TouchableOpacity onPress={()=>{
           this.setImage()
          }}
          style={styles.sheetbuttonviewtouch}>
          <Text style={styles.yestext}>Yes</Text>
          </TouchableOpacity>
          </View>


          <View style={styles.sheetbuttonview}>
          <TouchableOpacity onPress={()=> {this.PhotoSheet.close()}}
          style={styles.sheetbuttonviewtouch}>
          <Text style={styles.notext}>No</Text>
          </TouchableOpacity>
          </View>
      
      </View>

</RBSheet>
  )
};

const styles = StyleSheet.create({

 
     
  scrollviewcontainer:{justifyContent:'center',alignItems:'center'},
  scrollview:{backgroundColor:'white',width:'100%',flex:1},
  postsectionmain:{backgroundColor:'white',
  height:Dimensions.get('window').height * 0.18,width:'100%',flexDirection:'row',
 alignItems:'center',padding:10,flex:2,marginBottom:0},
 postmain:{flex:1.2,height:'100%',flexDirection:'row'},
 userimagemain:{flex:0.3,height:'100%',alignItems:'center',justifyContent:'center'},
 userimagepost:{ height: 50, width: 50,borderRadius:65,overflow:'hidden'},
  postinputmain:{flex:0.9,height:'100%'},
  postinput:{margin:10,width:'85%',height:'85%',borderColor:'grey',
  borderWidth:1,borderRadius:10,padding:10,textAlignVertical:'top'},
  postimagemain:{flex:0.8,height:'100%',flexDirection:'row'},
  postphoto:{flex:0.7,height:'100%',justifyContent:'center',alignItems:'center'},
  photopost:{width:30,height:30},
  postvideomain:{flex:0.7,height:'100%',justifyContent:'center',alignItems:'center'},
  postbuttonmain:{flex:1,height:'100%',justifyContent:'center',alignItems:'center'},
  postbutton:{margin:3,backgroundColor:'blue',height:35,justifyContent:'center',
  alignItems:'center',padding:11,borderRadius:5},
  postbuttoncolor:{color:'white'},
  borderview:{width:'100%',height:0.5,backgroundColor:'grey',marginBottom:15},

  errorsheetmain:{width:Dimensions.get('window').width * 0.94,
  height:'20%',
  backgroundColor:'white',padding:10,
  flexDirection:'column',backgroundColor:'white'},
 postcard:{
   width: Dimensions.get('window').width* 0.95,
 height:null,backgroundColor:'white',
 margin:10,padding:0,borderWidth:0.5,elevation:3
},
sheetvideo:{height:'100%',width:'90%'},
videosheetmain:{width:Dimensions.get('window').width * 0.94,
        height:'60%',
        backgroundColor:'white',padding:10,
        flexDirection:'row',backgroundColor:'white'},
notext:{color:colors.color_primary},
yestext:{color:'white'},
sheetbuttonview:{flex:1,margin:10,elevation:10},
sheetbuttonparent:{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
flex:2},
  sheetlabel:{margin:10},
  sheetbuttonviewtouch:{width:'100%',backgroundColor:colors.color_primary,padding:15,
              justifyContent:'center',alignItems:'center',elevation:10},
  postcardtouch:{
    flexDirection:'row',
    justifyContent:'flex-start',alignItems:'center'
  },
   userimage:{height:40,width:40,
    overflow:'hidden',borderRadius:20,margin:10},

    username:{marginBottom:5,fontWeight:'bold'},
    clockimage:{width:17,height:17,marginRight:6},
    timeview:{flexDirection:'row',width:'90%',alignItems:'center'},

    timetext:{color:'grey',fontSize:13},

    descriptiontext:{margin:5
    },
    postimage:{alignSelf:'center',height:'100%',width:'90%',
    overflow:'hidden',flex:1},
    timeviewmain:{marginLeft:8},
    postborder:{width:'100%',backgroundColor:'grey',height:0.5},
    liketouch:{flexDirection:'row',alignItems:'center'},
    likeimagemain:{height:25,width:25,
      overflow:'hidden',borderRadius:12,margin:10},

      reaction:{position:'absolute',
      bottom:10,left:10,
      backgroundColor:'white',
      width:null,flexDirection:'row',
       alignItems:'center',borderRadius:10,
      padding:5,borderWidth:1},
      reactionimage:{width:20,height:20,marginRight:10},
      photosheetmain:{width:'60%',height:'80%',
      padding:0,flex:3,flexDirection:'row',
      backgroundColor:'white',justifyContent:'center',alignItems:'center'},

      sheetimage:{height:'90%',width:'60%',
    },
});

export default PhotoBottomsheet;

