import React, { useState,useEffect } from 'react';
import { colors, urls } from '../Globals';

import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';


import { Header } from 'react-navigation-stack';


// itss type definition from Flow, it means that constant App is of type function 
//and it returns ReactNode.

// ReactNode is one of these types: ReactChild | ReactFragment | ReactPortal | boolean | null | undefined

// This means the function App can return, any valid JSX (in react native its anything from View, Text, .etc), ReactFragment, React.Portal, boolean, null, undefined


const CustomHeader : () => React$Node = (props) => {

  
  useEffect(() => {
    console.log("component did mount  "+ Header.HEIGHT)

    return () => {
      console.log("componentWillUnmount")
   }
  },[])



  return (
    <View style={styles.container}>
     <Text onPress={()=> {props.navigation.navigate("Discover")}}
            style={{
              color: 'white',
              textAlign: 'center',
              fontWeight: 'bold',
              fontSize: 18,
            }}>
             {props.user.email} 
          </Text>
     
    </View>
  );
};
const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(CustomHeader);

const styles = StyleSheet.create({

    container:{
       
        backgroundColor:'red',
        height:Header.HEIGHT,
        justifyContent:'center'
    }
 
});



