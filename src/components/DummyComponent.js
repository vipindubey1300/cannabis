import React from 'react';
import { colors, urls } from '../Globals';
import {Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';

import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import Colors from '../../Constants/Colors';

// itss type definition from Flow, it means that constant App is of type function 
//and it returns ReactNode.

// ReactNode is one of these types: ReactChild | ReactFragment | ReactPortal | boolean | null | undefined

// This means the function App can return, any valid JSX (in react native its anything from View, Text, .etc), ReactFragment, React.Portal, boolean, null, undefined


const DummyComponent : () => React$Node = () => {

  // const Eader = (props) => {
  //   return (
  //     <Text onPress={()=> setCount(count + 1)}>{props.value}</Text>
  //   )
  // }

  const [count, setCount] = useState(0);
  function s(){


    Snackbar.show({
      title: 'Hello world',
      duration: Snackbar.LENGTH_INDEFINITE,
      action: {
        title: 'UNDO',
        color: 'green',
        onPress: () => { /* Do something. */ },
      },
    });
  };
  
  //component did mount 
  useEffect(() => {
    console.log("Eneteredddddd")

    // componentWillUnmount
    //return a callback in useEffect's 
    //callback argument and it will be called before unmounting
    return () => {
      console.log("Ejjjjjjjjjjjjjjjjjj")
   }
  },
  []// passing an empty array as second argument triggers the callback in useEffect only after 
  //the initial render thus replicating `componentDidMount` lifecycle behaviour
  )

 



  return (
    <View>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
       
            <Eader value={'hhhhh'}></Eader>
            <Text onPress={()=> s()}>{count}</Text>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};
const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Discover);

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});



