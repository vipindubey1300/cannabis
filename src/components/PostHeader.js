
import { colors, urls } from '../Globals';
import {Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';
import PhotoBottomsheet from '../components/PhotoBottomsheet';
import React from 'react';


// itss type definition from Flow, it means that constant App is of type function 
//and it returns ReactNode.

// ReactNode is one of these types: ReactChild | ReactFragment | ReactPortal | boolean | null | undefined

// This means the function App can return, any valid JSX (in react native its anything from View, Text, .etc), ReactFragment, React.Portal, boolean, null, undefined


class PostHeader extends React.Component  {

  constructor(props){
      super(props);
      this.state = {
          post_error:false,
          post_text:"",
          imageSourceDummy:null,
          photo:"",
          imageSource:null,
          videoSource:null,
          videoSourceDummy:null,
          post_text_error:false,
          post_error_msg:"Your Post should contain description with only one Image / Video .Kindly input all to upload your post."
      }
  }


    addPhoto() {
      const options = {
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };
   
      //launchCamera
      //showImagePicker
      //launchImageLibrary
      ImagePicker.launchImageLibrary(options, (response) => {
        console.log('Response = ', response);
   
        if (response.didCancel) {
          console.log('User cancelled photo picker');
         // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};
  
          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
          };
         
  
          this.setState({
            imageSourceDummy: source ,
            photo:photo,
   
          });
        //  this.PhotoSheet.open();
        this.PhotoSheet.open()
       
  
          {/*photo is a file object to send to parameters */}
        }
      });
   }

   addVideo() {
    const options = {
      title: 'Video Picker', 
      mediaType: 'video', 
      videoQuality: 'medium',
      durationLimit: 30,
      takePhotoButtonTitle: 'Take Video...',
      allowsEditing: true,
     // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true
      }
    };
 
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);

       // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};
        console.log("VIDEO>>>>",JSON.stringify(response))
        var video={
          name: 'name.mp4',
          uri: source.uri,
          type: 'video/mp4'
        }
       

        this.setState({
          videoSourceDummy: source ,
         video:video
 
        });

        this.VideoSheet.open();

   
      

       
      }
    });
 }

     
isValid(){
  const { post_text, imageSource , videoSource } = this.state;

  console.log(post_text)

    let valid = false;

      if (post_text.toString().trim().length > 0 && 
      (imageSource != null || videoSource != null)
      ) {

        console.log("True")
          
            valid = true;
            //return true
        }

        if(imageSource != null && videoSource != null){
    
          console.log("SADFDSF")
       //   this.ErroSheet.open()
          //open error sheet 
          this.setState({post_error:true,post_text_error:true})
          this.setState({post_error_msg:"Please Add Photo Image 1 or Photo Image 2"})
          return false;
        }

        else if (post_text.toString().trim().length === 0) {

        // this.setTimeout(this.setState({post_text_error:true}), 5000);
        // this.setState({post_error:true,post_text_error:true})
        this.setState({post_error:true,post_text_error:true})

         setTimeout(() => {

          this.setState({post_text_error:false,post_error:false})
         
    
        }, 2000);
      
    
          
          return false;
        }

        else  if(imageSource == null && videoSource == null){

          this.setState({post_error:true,image_video_error:true})
          setTimeout(() => {

            this.setState({post_text_error:false,image_video_error:false})
           
      
          }, 2000);
        
          console.log("imagesssssssss")
          return false;
        }

      
      return valid

}

    
   makePost(){
      if(this.isValid()){
   
   
        console.log("Making posts")
   
       AsyncStorage.getItem("user_id").then((item) => {
         if (item) {
           var formData = new FormData();
   
           formData.append('user_id',item);
           formData.append('title','title');
           formData.append('description',this.state.post_text);
           {
             this.state.imageSource != null
             ?    formData.append('image',this.state.photo)
             : formData.append('video',this.state.video)
           }
           {
             this.state.imageSource != null
             ?    formData.append('media_type',1)
             : formData.append('media_type',2)
           }
           
   
                         this.setState({loading_status:true})
                         let url = urls.base_url +'api/create_post'
                         fetch(url, {
                         method: 'POST',
                         headers: {
                           'Accept': 'application/json',
                           'Content-Type': 'multipart/form-data',
                         },
                     body: formData
   
                       }).then((response) => response.json())
                             .then((responseJson) => {
                     this.setState({loading_status:false})
                     console.log(JSON.stringify(responseJson))
                //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
   
                
                Platform.OS === 'android' 
                ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                : Alert.alert(responseJson.message)
   
                
                                 if(!responseJson.error){
   
                                   this.setState({imageSource:null,
                                   post_text:'',
                                    videoSource:null,
                                   video:null,photo:null})
   
                                  this.getPosts()
                                
                               }
                               else{
   
                                
                               }
                             }).catch((error) => {
                               this.setState({loading_status:false})
                               
                               Platform.OS === 'android' 
                         ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                         : Alert.alert("Try Again !")
   
   
                             });
         }
         else{
           ToastAndroid.show("User not found !",ToastAndroid.LONG)
         }
       });
      }
     }

     setImage(){
      this.setState({imageSource:this.state.imageSourceDummy})
     }

     setVideo(){
      this.setState({videoSource:this.state.videoSourceDummy})
    }
  
  

  render(){

    const PhotoSheet= () =>{
     
      return (
        <RBSheet
        ref={ref => {
          this.PhotoSheet = ref;
        }}
        height={240}
        duration={0}
        customStyles={{
          container: {
            justifyContent: "center",
            alignItems: "center",
            borderTopLeftRadius:15,
            borderTopRightRadius:15
          },
          // wrapper:{
          //   backgroundColor:'grey'
          // }
        }}
        animationType='fade'
       
        minClosingHeight={10}

        >
          <View style={styles.photosheetmain}>
          
          
          <Image source={this.state.imageSourceDummy}
                                  style={styles.sheetimage}  />
          
          </View>

          <Text style={styles.sheetlabel}>Do you want to add this image to add to post ?</Text>

          <View style={styles.sheetbuttonparent}>

              <View style={styles.sheetbuttonview}>
              <TouchableOpacity onPress={()=>{
               this.setImage()
              }}
              style={styles.sheetbuttonviewtouch}>
              <Text style={styles.yestext}>Yes</Text>
              </TouchableOpacity>
              </View>


              <View style={styles.sheetbuttonview}>
              <TouchableOpacity onPress={()=> {this.PhotoSheet.close()}}
              style={styles.sheetbuttonviewtouch}>
              <Text style={styles.notext}>No</Text>
              </TouchableOpacity>
              </View>
          
          </View>

    </RBSheet>
      )
    }

    const VideoSheet= () =>{
     
      return (
        <RBSheet
        ref={ref => {
          this.VideoSheet = ref;
        }}
        height={220}
        duration={0}
        customStyles={{
          container: {
            justifyContent: "center",
            alignItems: "center",
            borderTopLeftRadius:15,
            borderTopRightRadius:15
          },
          // wrapper:{
          //   backgroundColor:'grey'
          // }
        }}
        animationType='fade'
       
        minClosingHeight={10}

        >
          <View style={styles.videosheetmain}>
          
          
          <Video source={this.state.videoSourceDummy}
              disableBack
                disableFullscreen
                disableVolume
                disableSeekbar
                muted={true}
                autoplay={false}
                paused={false}
                onPause={()=> console.log("Pause")}
                onPlay={()=> console.log("Play..")}
                resizeMode='stretch'
                showOnStart={false}
                seekColor={colors.color_primary}
        
                                  style={styles.sheetvideo}  />
          
          </View>


          <Text style={styles.sheetlabel}>Do you want to add this video to add to post ?</Text>

          <View style={styles.sheetbuttonparent}>

              <View style={styles.sheetbuttonview}>
              <TouchableOpacity onPress={()=>{this.setVideo()}}
              style={styles.sheetbuttonviewtouch}>
              <Text style={styles.yestext}>Yes</Text>
              </TouchableOpacity>
              </View>


              <View style={styles.sheetbuttonview}>
              <TouchableOpacity onPress={()=> {this.VideoSheet.close()}}
              style={styles.sheetbuttonviewtouch}>
              <Text style={styles.notext}>No</Text>
              </TouchableOpacity>
              </View>
          
          </View>




    </RBSheet>
      )
    }

    const ErroSheet= () =>{
     
      return (
        <RBSheet
        ref={ref => {
          this.ErroSheet = ref;
        }}
        height={180}
        duration={0}
        customStyles={{
          container: {
            justifyContent: "center",
            alignItems: "center",
            borderTopLeftRadius:15,
            borderTopRightRadius:15
          },
          // wrapper:{
          //   backgroundColor:'grey'
          // }
        }}
        animationType='fade'
       
        minClosingHeight={10}

        >
        <View style={styles.errorsheetmain}>
        
       <Text>Looks like you have added both image and video in your post.You can post with only one image or video. </Text>
       
    
        
        </View>

        <View style={styles.sheetbuttonparent}>

            <View style={styles.sheetbuttonview}>
            <TouchableOpacity onPress={()=>{ this.clearImage()}}
            style={styles.sheetbuttonviewtouch}>
            <Text style={styles.yestext}>Remove Image</Text>
            </TouchableOpacity>
            </View>


            <View style={styles.sheetbuttonview}>
            <TouchableOpacity onPress={()=> {this.clearVideo()}}
            style={styles.sheetbuttonviewtouch}>
            <Text style={styles.notext}>Remove Video</Text>
            </TouchableOpacity>
            </View>
        
        </View>



    </RBSheet>
      )
    }

      return(
          <View style={{flex:1}}>
          <PhotoSheet></PhotoSheet>
          <VideoSheet></VideoSheet>
          <ErroSheet></ErroSheet>

           <PhotoBottomsheet photo={this.state.imageSourceDummy}
              inputref={'photoSheet'}
              ref={ref => this.photoSheet = ref}/>

              <View style={styles.postsectionmain}>

                  <View style={styles.postmain}>

                      <View style={styles.userimagemain}>
                          <Image style={styles.userimagepost}
                         source={{uri:urls.base_url + this.props.user.image}}
                          ></Image>
                      </View>

                      <View style={styles.postinputmain}>
                          <TextInput
                          style={styles.postinput}
                          multiline={true}
                          placeholder={'Write Something here ..'}
                          value={this.state.post_text}
                          onChangeText={(post_text) => this.setState({post_text:post_text}) }
                          >
                          
                          </TextInput>    
                      </View>
                  </View>

                  <View style={styles.postimagemain}>

                      <View style={styles.postphoto}>
                          <Text>Photo</Text>
                          <TouchableOpacity  onPress={()=> this.addPhoto()}>
                              <Image 
                          
                          source={require('../assets/photo.png')} style={styles.photopost} resizeMode='contain'/>
                          </TouchableOpacity>

                      </View>

                      <View style={styles.postvideomain}>
                          <Text>Video</Text>
                          <TouchableOpacity  onPress={()=> this.addVideo()}>
                              <Image source={require('../assets/video.png')}
                              style={styles.photopost} resizeMode='contain'/>
                          </TouchableOpacity>
                      </View>


                      <View style={styles.postbuttonmain}>
                          <TouchableOpacity onPress={()=> this.makePost()}>
                              <View style={styles.postbutton}>
                                  <Text style={styles.postbuttoncolor}>Post</Text>
                              </View>
                          </TouchableOpacity>
                      </View>


                  </View>



               
                  </View>

                  <View style={styles.borderview}></View>
                  {
                      this.state.post_error
                      ?
                      <View style={{height:null,width:Dimensions.get('window').width * 0.98,
                      backgroundColor:'white',borderColor:'red',borderWidth:0.5,elevation:6,
                      padding:15,margin:5}}>
                          {
                          this.state.post_text_error
                          ?   <Text style={{color:colors.color_primary,
                              fontWeight:'bold',alignSelf:'center',margin:5}}>Please Enter Post Description.</Text>
                          : <Text style={{color:colors.color_primary,
                              fontWeight:'bold',alignSelf:'center',margin:5}}>Please Enter one image/video .</Text>
                          }
                      

                          <View style={{flexDirection:'row',
                      flex:4,justifyContent:'space-around',alignItems:'center'}}>

                          <Text style={{flex:3,fontSize:13}}>{this.state.post_error_msg}</Text>
                          <TouchableOpacity onPress={()=> this.setState({post_error:false})}>
                              <View style={{padding:10,justifyContent:'center',alignItems:'center',
                              backgroundColor:colors.color_primary,margin:5,elevation:3,borderRadius:10,marginLeft:9}}>
                              <Text style={{color:'white'}}>OK</Text>
                              </View>
                          </TouchableOpacity>
                      </View>
                      

                      </View>
                      : null

                      }


          </View>
          
        

      )
  }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(PostHeader);


const styles= StyleSheet.create({

  postsectionmain:{
      backgroundColor:'white',
     height:Dimensions.get('window').height * 0.18,
      width:'100%',flexDirection:'row',
      alignItems:'center',
      
      padding:10,flex:2,marginBottom:0},

  postmain:{flex:1.2,height:'100%',flexDirection:'row'},

  userimagemain:{flex:0.3,height:'100%',alignItems:'center',justifyContent:'center'},
  userimagepost:{ height: 50, width: 50,borderRadius:65,overflow:'hidden'},

  postinputmain:{flex:0.9,height:'100%'},
  postinput:{margin:10,width:'85%',height:'85%',borderColor:'grey',
  borderWidth:1,borderRadius:10,padding:10,textAlignVertical:'top'},

  postimagemain:{flex:0.8,height:'100%',flexDirection:'row'},

  postphoto:{flex:0.7,height:'100%',justifyContent:'center',alignItems:'center'},
  photopost:{width:30,height:30},
  postvideomain:{flex:0.7,height:'100%',justifyContent:'center',alignItems:'center'},
  postbuttonmain:{flex:1,height:'100%',justifyContent:'center',alignItems:'center'},
  postbutton:{margin:3,backgroundColor:'blue',height:35,justifyContent:'center' ,alignItems:'center',padding:11,borderRadius:5},
  postbuttoncolor:{color:'white'},
  borderview:{width:'100%',height:0.5,backgroundColor:'grey',marginBottom:15},

  errorsheetmain:{width:Dimensions.get('window').width * 0.94,
  height:'20%',
  backgroundColor:'white',padding:10,
  flexDirection:'column',backgroundColor:'white'},
 postcard:{
   width: Dimensions.get('window').width* 0.95,
 height:null,backgroundColor:'white',
 margin:10,padding:0,borderWidth:0.5,elevation:3
},
sheetvideo:{height:'100%',width:'90%'},
videosheetmain:{width:Dimensions.get('window').width * 0.94,
        height:'60%',
        backgroundColor:'white',padding:10,
        flexDirection:'row',backgroundColor:'white'},
notext:{color:'white'},
yestext:{color:'white'},
sheetbuttonview:{flex:1,margin:10,elevation:10},
sheetbuttonparent:{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
flex:2},
  sheetlabel:{margin:10},
  sheetbuttonviewtouch:{width:'100%',backgroundColor:colors.color_primary,padding:15,
              justifyContent:'center',alignItems:'center',elevation:10},
  postcardtouch:{
    flexDirection:'row',
    justifyContent:'flex-start',alignItems:'center'
  },
   userimage:{height:40,width:40,
    overflow:'hidden',borderRadius:20,margin:10},

    username:{marginBottom:5,fontWeight:'bold'},
    clockimage:{width:17,height:17,marginRight:6},
    timeview:{flexDirection:'row',width:'90%',alignItems:'center'},

    timetext:{color:'grey',fontSize:13},

    descriptiontext:{margin:5
    },
    postimage:{alignSelf:'center',height:'100%',width:'90%',
    overflow:'hidden',flex:1},
    timeviewmain:{marginLeft:8},
    postborder:{width:'100%',backgroundColor:'grey',height:0.5},
    liketouch:{flexDirection:'row',alignItems:'center'},
    likeimagemain:{height:25,width:25,
      overflow:'hidden',borderRadius:12,margin:10},

      reaction:{position:'absolute',
      bottom:10,left:10,
      backgroundColor:'white',
      width:null,flexDirection:'row',
       alignItems:'center',borderRadius:10,
      padding:5,borderWidth:1},
      reactionimage:{width:20,height:20,marginRight:10},
      photosheetmain:{width:'60%',height:'80%',
      padding:0,flex:3,flexDirection:'row',
      backgroundColor:'white',justifyContent:'center',alignItems:'center'},

      sheetimage:{height:'90%',width:'60%',
    },

})

