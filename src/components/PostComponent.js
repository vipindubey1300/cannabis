import React from 'react';
import { colors, urls } from '../Globals';
import {Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';
import { Item } from 'native-base';

 class PostComponent extends React.Component{

  _menu = null;
 
  setMenuRef = ref => {
    this._menu = ref;
  };
 
  hideMenu = () => {
    this._menu.hide();
  };
 
  showMenu = () => {
    this._menu.show();
  };

  // shouldComponentUpdate(nextProps) {
  //   //const { isSelected } = this.props;
  //     //console.log(nextProps)
  //     //return this.props.posts.likes_count !== nextProps.posts.likes_count;
  // }

    showReaction(index){
        //this.props.posts[index].smiley = true
                 // this.setState({posts:  this.state.posts})

                 let ids = [...this.props.posts];     // create the copy of state array
                  ids[index].smiley  = true                  //new value
                this.props.setPosts(ids)          //update the value
      }

      hideReaction(index){
                 let ids = [...this.props.posts];     // create the copy of state array
                  ids[index].smiley  = false                  //new value
                this.props.setPosts(ids)     
      }

      next(){
         //this.props.navigation.navigate("SellerProfile")
         let user_id = this.props.posts[this.props.index].user_id
        
          let obj={
            'user_id':user_id
          }
            this.props.navigation.navigate("SellerProfile",{result:obj})
         
         
      }

      edit = () => {

        if( this.props.posts[this.props.index].user_id == this.props.userProps.user.user_id){
            this._menu.hide();
            let post = this.props.posts[this.props.index]
            let obj={
              'post':post
            }
              this.props.navigation.navigate("EditPost",{result:obj})

       }
       else{
         this._menu.hide();
         ToastAndroid.show("This Post had been Posted By Another User",ToastAndroid.SHORT)
       }
       
       

      }


      change(like_id){



       // console.log("Click")
       // this.props.posts[this.props.index].likes_count = 6
        this.props.handler(this.props.posts[this.props.index].id,like_id)
      }


      deletePost =()=>{
        //console.log(this.props.userProps.user.user_id)

        if( this.props.posts[this.props.index].user_id == this.props.userProps.user.user_id){
           this._menu.hide();

        this.props.deletePost(this.props.posts[this.props.index].id)

        }
        else{
          this._menu.hide();
          ToastAndroid.show("This Post had been Posted By Another User",ToastAndroid.SHORT)
           
        }
       
      }

     _checkCondition(){
        let img;
        if(this.props.posts[this.props.index].like_flag == 1){
            img = require('../assets/p1.png')
        }else if (this.props.posts[this.props.index].like_flag == 2){
          img = require('../assets/p2.png')
        }else if (this.props.posts[this.props.index].like_flag == 3){
          img = require('../assets/p3.png')
        }
        else if (this.props.posts[this.props.index].like_flag == 4){
        img = require('../assets/p4.png')
      }else{
          img = require('../assets/p1.png')
        }

        
        return img;   
    };


    _setSmileyText = () =>{
    
      if(this.props.posts[this.props.index].like_flag == 1){
         return (
           <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'blue'}}>(Like)</Text>
         )
      }else if (this.props.posts[this.props.index].like_flag == 2){
        return (
          <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'red'}}>(Cool)</Text>
        )
      }else if (this.props.posts[this.props.index].like_flag == 3){
        return (
          <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'#CDCD00'}}>(Awesome)</Text>
        )
      }
      else if (this.props.posts[this.props.index].like_flag == 4){
        return (
          <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'#CDCD00'}}>(Fantastic)</Text>
        )
     }else{
          return (
            <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'blue'}}>(Like)</Text>
          )
      }

      
     

    }


    _setLikeText(item){

      

      let text = '';
      if(item.likes_count == 0){
        text = '0 Likes'
      }
      else if(item.likes_count >= 2){
        if(item.like_flag >= 1){
          //means i liked this
            text = "You and " + (parseInt(item.likes_count) - 1) + ' others liked this'
        }
        else{
          //means i haven't reacted to this
          text = item.liked_by+ " and " + (parseInt(item.likes_count) - 1) + ' others liked this'
        }
      }

      // else if(item.likes_count == 1){
      //     //means only one reacted 
      //     if(item.like_flag > 1){
      //       //means i liked this
      //         text = "You liked this"
      //     }
      //     else{
      //       //means i haven't reacted to this
      //       text = item.liked_by+ ' liked this'
      //     }
      // }
      else if(item.likes_count == 1){
        //means only one reacted 
        if(item.like_flag >= 1){
          //means i liked this
            text = "You liked this"
        }
        else{
          //means i haven't reacted to this
          text = item.liked_by+ ' liked this'
        }
    }

      return text

    }
    

    render(){

     

        // console.log("props",this.props.posts);
        // console.log("index",this.props.index)
        return(

            <View style={{width: Dimensions.get('window').width* 0.99,
            height:null,backgroundColor:'white',
            padding:0,borderWidth:0.5,elevation:7,
            alignSelf:'center',marginBottom:5,
            marginTop:5,marginLeft:2,marginRight:2,
            shadowOpacity:0.2}}>
    
            <TouchableOpacity onPress={()=>{
              if( this.props.posts[this.props.index].smiley == true){
                //this.props.posts[this.props.index].smiley = false
               // this.setState({posts:  this.state.posts})
               this.hideReaction(this.props.index)
              }
           
            }}>

            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
           
            
            <View style={{flexDirection:'row',
            justifyContent:'flex-start',alignItems:'center'}}>
    
                <TouchableOpacity
                        style={{width:'auto'}}
                        onPress={()=>{
                            this.next()
                            }}
                        >
           
                       <Image source={{uri:urls.base_url+this.props.posts[this.props.index].user_pic}}
                    style={{height:40,width:40,
                    overflow:'hidden',borderRadius:20,margin:10}}  />
                    </TouchableOpacity>
    
                    <View style={{marginLeft:8}}>
                    <Text style={{marginBottom:5,fontWeight:'bold'}}>{this.props.posts[this.props.index].user_name}</Text>
                    <View style={{flexDirection:'row',width:'90%',alignItems:'center'}}>
                    <Image source={require('../assets/clock.png')}
                                      style={{width:17,height:17,marginRight:6}} resizeMode='contain'/>
    
                          <Text style={{color:'grey',fontSize:13}}>{this.props.posts[this.props.index].created_at}</Text>
                    
                    </View>
                    </View>
    
                   
            </View>
          

            {
              this.props.posts[this.props.index].user_id == this.props.userProps.user.user_id
              ?

        
                <View style={{marginTop:10}}>
                  <Menu
                  ref={this.setMenuRef}
                  button={
                  <TouchableOpacity onPress={this.showMenu}>
                  <Image source={require('../assets/menu-options.png')}
                                  style={{width:17,height:17,marginRight:6,flex:1}} resizeMode='contain'/>


                  </TouchableOpacity>

                  }
                  >
                  <MenuItem onPress={this.deletePost}>Delete</MenuItem>
                  <MenuItem onPress={this.edit}>Edit</MenuItem>

                  </Menu>

                </View>
     
        : 
      null

            }



            </View>
            
              
                <Text style={{margin:5
                }}>{this.props.posts[this.props.index].description}</Text>
    
    
                <TouchableOpacity onPress={() => this.props.seeImage(this.props.posts[this.props.index])}>
                <View style={[
                    { width: '100%'},
                 
                    { height:Dimensions.get('window').height *0.25,
                      margin: 10,alignSelf:'center'
                  }]} >
    
                 
                 {
                  this.props.posts[this.props.index].media_type == '2'
                    ?
                    <Video source={{uri:urls.base_url+ this.props.posts[this.props.index].post_video}} 
                    disableBack
                    disableFullscreen
                    disableVolume
                    disableSeekbar
                    muted={true}
                    autoplay={false}
                    paused={true}
                    onPause={()=> console.log("Pause")}
                    onPlay={()=> console.log("Play..")}
                    onProgress={()=> console.log("Progress")}
                    onLoadStart={()=> console.log("Load starst" + this.props.index)}
                    onLoad={()=> console.log("")}
                 
                    resizeMode='stretch'
                    showOnStart={false}
                    seekColor={colors.color_primary}
                    style={{width:'99%',height:'100%',alignSelf:'center'}} />
                    : 

                  
                    <Image source={{uri:urls.base_url+ this.props.posts[this.props.index].post_img}}
                     resizeMode='cover'
                    style={{alignSelf:'center',height:'100%',width:'99%',
                    overflow:'hidden',flex:1}}  />
                   
                   
                  } 
                  {
                    this.props.posts[this.props.index].media_type == '2'
                    ?
                    <View style={{position:'absolute',top:10,
                   right:10,backgroundColor:colors.color_primary,padding:5
                    }}>
        <Image source={require('../assets/play.png')} 
        style={{width:20,height:20}} resizeMode='stretch'/>
        </View>
        :null
                  }
               
    
                  </View>
                  </TouchableOpacity>
    
                  <View style={{width:'100%',backgroundColor:'grey',height:0.5}}></View>
    
                  
               


                
              


                   
    
                   {/* <Text>{this.props.posts[this.props.index].likes_count} likes</Text>*/} 
                   <View style={{flexDirection:'row',padding:3,alignItems:'center'}}>

                   
                    {
                      this.props.posts[this.props.index].likes_count == 0
                      ?
                      null
                      :

                      <View style={{flexDirection:'row',alignItems:'center'}}>
                      <Image source={this._checkCondition()}
                        style={{height:25,width:25,
                        overflow:'hidden',borderRadius:12,marginRight:5}}  />


                          {
                            this._setSmileyText()
                          }

                      </View>
                     

                    }

                    <Text>{this._setLikeText(this.props.posts[this.props.index])}</Text>

                    
                   </View>
                  


    
                    </TouchableOpacity>
    
    
                    <View style={{width:'100%',backgroundColor:'grey',height:0.5}}></View>

                   

                    <View style={{padding:3}}>

                    <TouchableOpacity activeOpacity={0.6}
                        style={{flexDirection:'row',alignItems:'center',width:'60%'}}
                      // onPress={()=> this.likePosts(this.props.posts[this.props.index].id,1)}
                        onLongPress={()=> {
                          //console.log("sdfhsdbfjhdsfbjhdsfbdhjfdhj"
                        this.showReaction(this.props.index)
                        }}
                        onPress={()=> this.change(1)}
                       >
                        <Image source={require('../assets/thumb-up.png')}
                        resizeMode='contain'
                        style={{height:25,width:25,
                        overflow:'hidden',borderRadius:12,margin:0}}  />


                    
                    </TouchableOpacity>

                    </View>

                    {
                      this.props.posts[this.props.index].smiley
                      ?
                      <View style={{position:'absolute',
                      bottom:30,left:5,
                      backgroundColor:'white',
                      width:null,flexDirection:'row',
                       alignItems:'center',borderRadius:15,
                      padding:5,borderWidth:0.4}}>
        
    
                      <TouchableOpacity  onPress={()=> {
                        this.change(1)
                        this.hideReaction(this.props.index)
                      }}>
                      <Image source={require('../assets/p1.png')}
                      style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                      </TouchableOpacity>
                    
                      <TouchableOpacity onPress={()=> {
                       this.change(2)
                        this.hideReaction(this.props.index)
                      //  this.setState({posts:  this.state.posts})
                      }}>
                    <Image source={require('../assets/p2.png')}
                      style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                      </TouchableOpacity>
    
                      <TouchableOpacity onPress={()=> {
                      this.change(3)
                        this.hideReaction(this.props.index)
                      }}>
                     <Image source={require('../assets/p3.png')}
                      style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                      </TouchableOpacity>
    
    
                      <TouchableOpacity onPress={()=> {
                       this.change(4)
                        this.hideReaction(this.props.index)
                      }}>
                     <Image source={require('../assets/p4.png')}
                      style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                      </TouchableOpacity>
    
    
                      </View>
                      : null 
                    }

    
                    
                 
                  
    
                
          </View>
        )
    }
}



// withNavigation returns a component that wraps ChildComponent and passes in the
// navigation prop
export default withNavigation(PostComponent);

/*
<Menu
    ref={ref => (this[`menu${index}`] = ref)}
    button={
       <Text>Show Menu</Text>
    }
>
    <MenuItem onPress={() => this[`menu${index}`].hide()}>
       Menu 1
    </MenuItem>
    <MenuDivider />
    <MenuItem onPress={() => this[`menu${index}`].hide()}>
       Menu 2
    </MenuItem>
</Menu>

*/