// reducer taking the initial state as the first parameter.
//  As a second parameter we’ll provide action.




const initialState = {
  user: {  }
};
function rootReducer(state = initialState, action) {
  switch (action.type) {
      case "ADD_USER":
          return {
              ...state,
              user: action.payload
          };
      case "REMOVE_USER":
          return {
              ...state,
              user: {}
          };

      case "INVESTOR_USER":
            return {
                ...state,
                user: {
                    ...state.user,
                    'investor':1
                    }
            };
            case "ENTREPRENEUR_USER":
            return {
                ...state,
                user: {
                    ...state.user,
                    'entrepreneur':1
                    }
            };

     case "REMOVE_INVESTOR_USER":

         let delEnt = {...state.user};

                    
         delete delEnt['investor'];

         return {
            ...state,
                user:{...state.user}

        };



      case "UPDATE_USER":
          /* so you return a new state object with all the data from old state
          user also contain the data from old state.user but you update some of his parameters
          like this.props.updateUser({  email:'abc@gmail.com' }); update only email field 
          this.props.updateUser({   user_name : 'sdfd' , email:'abc@gmail.com' }); 
          update only user_name field

          change the name of action too, 
          because later you will add more item in the redux and "change" dont say it 
          change what ( just an idea )
          */
          return {
              ...state,
              user: {
              ...state.user,
              ...action.payload
              }
          };

      default: return state;
  }
}
export default rootReducer;

// only way to change the state is by sending a signal to the store.
// This signal is an action.
//  “Dispatching an action” is the process of sending out a signal

//Redux actions are nothing more than JavaScript objects
// {
//   type: 'ADD_ARTICLE',
//   payload: { title: 'React Redux Tutorial', id: 1 }
// }

// getState for accessing the current state of the application
// dispatch for dispatching an action
// subscribe for listening on state changes





// const adrian = {
//     fullName: 'Adrian Oprea',
//     occupation: 'Software developer',
//     age: 31,
//     website: 'https://oprea.rocks'
//   };
//   Let’s assume you want to create a new object(person) with a different name and website, but the same occupation and age.
  
//   You could do this by specifying only the properties you want and use the spread operator for the rest, like below:
  
//   const bill = {
//     ...adrian,
//     fullName: 'Bill Gates',
//     website: 'https://microsoft.com'
//   };

//FOR ARRAYYYYYYYYYYY......................

// [{index:1,value:'item1',done:false}, {index:2,value:'item2',done:false}]

// export default function todoApp(state=[], action){
//     switch(action.type){
//         case 'ADD_TODO':
//             return [...state, action.item];
//         case 'TOGGLE_TODOS':
//             const index = action.index;
//             return state.map((item) => (
//                 item.index===index? {...item, done: !item.done}: item
//             ))
//         default:
//             return state;    
//     }
// }





// const initialState = {
//     fetchData: {},
//     cartData: {}
//    }
   
//    function cartReducer(state = initialState, action) {
//      switch(action.type) {
//        case 'ADD_ITEM':
//        return {...state, cartData: {...state.cartData, ...action.payload}}
       
//        case 'EDIT_ITEM':
//        return {...state, cartData: {...state.cartData, ...action.payload}}
       
//        case 'REMOVE_ITEM':
//        let newState = Object.keys(state.cartData).reduce((r, e) => {
//          if(!action.payload[e]) r[e] = state.cartData[e];
//          return r
//        }, {})
       
//        return {...state, cartData: newState}
       
//        default:
//        return state;
//      }
//    }
   
//    var state = {}
   
//    state = cartReducer(undefined, {
//      type: 'ADD_ITEM',
//      payload: {"React":{'name': 'React', 'quantity': 1}}
//    })
//    console.log(state)
   
//    state = cartReducer(state, {
//      type: 'ADD_ITEM',
//      payload: {"Node":{'name': 'Node', 'quantity': 2}}
//    })
//    console.log(state)
   
//    state =  cartReducer(state, {
//      type: 'EDIT_ITEM',
//      payload: {"React":{'name': 'React', 'quantity': 4}}
//    })
//    console.log(state)
   
//    state = cartReducer(state, {
//      type: 'REMOVE_ITEM',
//      payload: {"React":{'name': 'React', 'quantity': 1}}
//    })
//    console.log(state)


//ADDING KEY VALUE TO OBJECT ------


// let initialState = {
//     reminders: {}
//   }
  
//   const remindersReducer = (state = initialState, action) => {
//     switch(action.type) {
//       case "fetchReminders":
//         return Object.assign({}, state, {
//           reminders: {
//             ...state.reminders,
//             [action.key]: action.value
//           }
//         });
//       default: return state;
//     }
//   };


//--COMBINE REDUCERS...........-------------



// As our app grows our reducer function is getting more complex so that we can split the single function into a multiple independent reducer functions which manage their own state.

// consider we have two reducers in our react app.

// counterReducer.js
// const counterReducer = (state={num:0},action)=>{

//     switch (action.type){

//         case "INCREMENT":
//          return { num : state.num+1 }

//         case "DECREMENT":
//           return {num: state.num-1}

//          default:
//            return state
//     }


// }

// export default counterReducer
// In the above code, we have created our first counterReducer function.

// Now, we are going to define the second reducer function.

// namesReducer.js
// const namesReducer = (state = { allNames: [] }, action) => {
//     switch (action.type) {

//         case "ALLNAMES":
//             return { allNames: state.allNames.concat(action.name) }

//         default:
//             return state
//     }

// }

// export default namesReducer
// Next, we need to import these two reducer functions inside the index.js file.

// combineReducers example

// index.js
// import React from 'react';
// import ReactDOM from 'react-dom';
// import { createStore, combineReducers } from 'redux'
// import {Provider} from 'react-redux'
// import counterReducer from './counterReducer';
// import namesReducer from './namesReducer';

// // combining two reducers into a single reducer
// const reducer = combineReducers({
//     counter: counterReducer,
//     name: namesReducer
// })


// const store = createStore(reducer)

// ReactDOM.render(<Provider store={store}><App /></Provider>,
//  document.getElementById('root'))
// In the above, we have imported two reducer functions which are counterReducer and namesReducer then we imported a combineReducer function from the ‘redux’ library.

// combineReducer function takes multiple reducer functions as an argument and turns down into a single reducer function.

// We are namespacing the reducer functions as counter for the counterReducer and name for the namesReducer.

// Let’s see how can we access the state from the Components.

// App.js
// import React from 'react'
// import {connect} from 'react-redux'

// class App extends React.Component{


//    render(){
//        return (
//            <div>
//             <h1>{this.props.num}</h1>
//            </div>
//        )
//    }

// }

// const mapStatetoProps = (state)=>{
//     return {
//         num : state.counter.num
//     }
// }

// export default connect(mapStatetoProps)(App)
// In the above component, we used state.counter.num because we namespaced it in the combineReducer function.

// If you open your redux dev tools you can see the whole app state in a single object with namespa