export const colors = {
  status_bar_color: "#6698FF",
  color_primary:'#3B62FF',
  color_secondary:'#FFFFFF',


};

export const fonts = {
  font_size: 16,
};

export const urls = {
  base_url: "http://webmobril.org/dev/cannabis/",
};