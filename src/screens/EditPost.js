import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import Snackbar from 'react-native-snackbar';
//import {  StackActions, NavigationActions} from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {  StackActions, NavigationActions} from 'react-navigation';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';


const dimension = Dimensions.get('window')


class EditPost extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      post:null,
      description:'',
      media_type:0,
      newPhoto:null,
      newPhotoSource:null,
      newVideo:null,
      newVideoSource:null,

   
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
       
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Edit Post</Text>
     </View>
    ),
  });

  isValid(){
    const { description } = this.state;

      let valid = false;

        if (
           description.toString().trim().length > 0  
        ) {
              valid = true;
             
          }

          if(description.toString().trim().length == 0){
      
            // Platform.OS === 'android' 
            //           ?   ToastAndroid.show('Enter Title', ToastAndroid.SHORT)
            //           : Alert.alert("Enter Title!")

            Snackbar.show({
              title: "Enter Post Description",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });
            valid = false
            return false;
          }

        

        
        return valid

  }



  editPost(){
    if(this.isValid()){


      console.log("Making posts")
 
     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
         var formData = new FormData();
 
         formData.append('post_id',this.state.post.id);
         formData.append('user_id',item);
         formData.append('description',this.state.description);
         formData.append('media_type',this.state.media_type);
         {
          this.state.media_type == 1
          ?    formData.append('image',this.state.newPhoto)
          : formData.append('video',this.state.newVideo)
        }
         


       // console.log(JSON.stringify(formData))
         
 
                       this.setState({loading_status:true})
                       let url = urls.base_url +'api/edit_post'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
 
                     }).then((response) => response.json())
                           .then((responseJson) => {
                   this.setState({loading_status:false})
                  
             
              
              // Platform.OS === 'android' 
              // ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
              // : Alert.alert(responseJson.message)

              Snackbar.show({
                title: responseJson.message,
                duration: Snackbar.LENGTH_SHORT,
                backgroundColor:'black',
                color:'red',
                action: {
                  title: 'OK',
                  color: 'green',
                  onPress: () => { /* Do something. */ },
                },
              });

              
 
              
                         if(!responseJson.error){

                          this.props.navigation.navigate('Home');
                          const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'Home' })],
                          });
                          this.props.navigation.dispatch(resetAction);
                              
                              
                             }
                             else{
 
                              
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                             
                      //        Platform.OS === 'android' 
                      //  ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      //  : Alert.alert("Try Again !")

                      Snackbar.show({
                        title: "Try Again !",
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });
 
 
                           });
       }
       else{
         ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });
    }
  }

  addPhoto() {
    const options = {
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
 
    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};

        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };
       

        this.setState({
          newPhotoSource: source ,
          newPhoto:photo,
 
        }); 
        
       

       
      }
    });
 }




 addPhotoCamera() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchCamera(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     

      this.setState({
        newPhotoSource: source ,
        newPhoto:photo,
        media_type:1

      }); 
      
     

     
    }
  });
}
addPhotoGallery() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchImageLibrary(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     

      this.setState({
        newPhotoSource: source ,
        newPhoto:photo,
        media_type:1
      }); 
      
     

     
    }
  });
}
addVideoCamera() {
  const options = {
    title: 'Video Picker', 
    mediaType: 'video', 
    videoQuality: 'medium',
    durationLimit: 30,
    takePhotoButtonTitle: 'Take Video...',
    allowsEditing: true,
   // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true
    }
  };


   //launchCamera
    //showImagePicker
    //launchImageLibrary
  ImagePicker.launchCamera(options, (response) => {
   // console.log('Response = ', response);

    if (response.didCancel) {
     // console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      //console.log('ImagePicker Error: ', response.error);

     // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};
     // console.log("VIDEO>>>>",JSON.stringify(response))
      var video={
        name: 'name.mp4',
        uri: source.uri,
        type: 'video/mp4'
      }
     

      this.setState({
        newVideoSource: source ,
        newVideo:video,
        media_type:2

      });

    }
  });
}


addVideoGallery() {


  const options = {
    title: 'Video Picker', 
    mediaType: 'video', 
    videoQuality: 'medium',
    durationLimit: 30,
    takePhotoButtonTitle: 'Take Video...',
    allowsEditing: true,
   // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true
    }
  };

   //launchCamera
    //showImagePicker
    //launchImageLibrary

  ImagePicker.launchImageLibrary(options, (response) => {
   // console.log('Response = ', response);

    if (response.didCancel) {
      //console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      //console.log('ImagePicker Error: ', response.error);

     // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
    }
    else if (response.customButton) {
     // console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};
      //console.log("VIDEO>>>>",JSON.stringify(response))
      var video={
        name: 'name.mp4',
        uri: source.uri,
        type: 'video/mp4'
      }
     

      this.setState({
        newVideoSource: source ,
        newVideo:video,
        media_type:2

      });

     

 
    

     
    }
  });
}


  



  componentWillMount(){
    
   let result = this.props.navigation.getParam('result')
   var post = result['post']
   var description = post['description']

    this.setState({post:post,description:description})

  }


        //
      resetMedia =()=>{
        this.setState({
          newPhoto:null,
          newPhotoSource:null,
          newVideo:null,
          newVideoSource:null,
          media_type:0
         

        })
      }

        imageChoosen = () =>{
       
        
          
          this.setState({
            newPhoto:null,
            newPhotoSource:null,
            newVideo:null,
            newVideoSource:null,
            media_type:0
           
  
          },() => this.ImageSheet.open())

        }

        videoChoosen = () =>{
          this.setState({
            newPhoto:null,
            newPhotoSource:null,
            newVideo:null,
            newVideoSource:null,
           
  
          },() => this.VideoSheet.open())

         
          
          
        }

        checkMediaType(){
          const {newPhoto,newVideo,post} = this.state
          const dimension = Dimensions.get('window')


          if(newPhoto != null){
            return (
              <Image source={this.state.newPhotoSource}
              resizeMode='contain'
              style={styles.imageStyle}/>
            )
          }
          else if(newVideo != null){
            return (
              <Video source={this.state.newVideoSource}
              disableBack
              disableFullscreen
              disableVolume
              disableSeekbar
              muted={true}
              autoplay={false}
              paused={false}
              onPause={()=> {}}
              onPlay={()=> {}}
              resizeMode='stretch'
              showOnStart={true}
              seekColor={colors.color_primary}
              style={styles.videoStyle} />
            )
          }
          else if( post.media_type == '2'){
            return(
              <Video source={{uri:urls.base_url+post.post_video}} 
              disableBack
              disableFullscreen
              disableVolume
              disableSeekbar
              muted={true}
              autoplay={false}
              paused={false}
              onPause={()=> {}}
              onPlay={()=> {}}
              resizeMode='stretch'
              showOnStart={true}
              seekColor={colors.color_primary}
              style={styles.videoStyle} />
            )
          }
          else{
            return(
              <Image source={{uri:urls.base_url+post.post_img}} resizeMode='contain'
              style={styles.imageStyle}  />
            )
          }
        }



    render(){

  
      const ImageSheet= () =>{
     
        return (
          <RBSheet
          ref={ref => {
            this.ImageSheet = ref;
          }}
          height={200}
          duration={0}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              borderTopLeftRadius:15,
              borderTopRightRadius:15
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType='fade'
          closeOnDragDown={true}
          minClosingHeight={10}

>
            <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>

                <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                      <Image style={{ height: 40, width: 40}}
                      resizeMode='contain'
                      source={require('../assets/logo.png')}>
                      </Image>

                      <Text style={{fontWeight:'bold',fontSize:16}}>Choose Image Source ! </Text>
                
                </View>


                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
              flex:2}}>

                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.addPhotoCamera()}
                  style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:'white'}}>Camera</Text>
                  </TouchableOpacity>
                  </View>


                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.addPhotoGallery()}
                  style={{width:'100%',backgroundColor:'white',padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:colors.color_primary}}>Gallery</Text>
                  </TouchableOpacity>
                  </View>
              
              </View>
            </View>
</RBSheet>

        )
      }
      const VideoSheet= () =>{
     
        return (
          <RBSheet
          ref={ref => {
            this.VideoSheet = ref;
          }}
          height={200}
          duration={0}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              borderTopLeftRadius:15,
              borderTopRightRadius:15
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType='fade'
          closeOnDragDown={true}
          minClosingHeight={10}

>
            <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>

                <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                      <Image style={{ height: 40, width: 40}}
                      resizeMode='contain'
                      source={require('../assets/logo.png')}>
                      </Image>

                      <Text style={{fontWeight:'bold',fontSize:16}}>Choose Video Source ! </Text>
                
                </View>


                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
              flex:2}}>

                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.addVideoCamera()}
                  style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:'white'}}>Camera</Text>
                  </TouchableOpacity>
                  </View>


                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.addVideoGallery()}
                  style={{width:'100%',backgroundColor:'white',padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:colors.color_primary}}>Gallery</Text>
                  </TouchableOpacity>
                  </View>
              
              </View>
            </View>
</RBSheet>

        )
      }

      const ChooseMediaSheet= () =>{
     
        return (
          <RBSheet
          ref={ref => {
            this.ChooseMediaSheet = ref;
          }}
          height={200}
          duration={0}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              borderTopLeftRadius:15,
              borderTopRightRadius:15
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType='fade'
          closeOnDragDown={true}
          minClosingHeight={10}

>
            <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>

                <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                      <Image style={{ height: 40, width: 40}}
                      resizeMode='contain'
                      source={require('../assets/logo.png')}>
                      </Image>

                      <Text style={{fontWeight:'bold',fontSize:16}}>Choose Type Of Media </Text>
                
                </View>


                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
              flex:2}}>

                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.imageChoosen()}
                  style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:'white'}}>Image</Text>
                  </TouchableOpacity>
                  </View>


                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.videoChoosen()}
                  style={{width:'100%',backgroundColor:'white',padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:colors.color_primary}}>Video</Text>
                  </TouchableOpacity>
                  </View>
              
              </View>
            </View>
</RBSheet>

        )
      }


      const{post} = this.state
      const dimension = Dimensions.get('window')


     
    
     
        return(
          <ImageBackground
          source={require('../assets/news_bg.jpg')}
          resizeMode="cover"
          style={styles.imagebackground}>

          <SafeAreaView style={styles.container}>
          <ImageSheet></ImageSheet>
          <ChooseMediaSheet/>
          <VideoSheet/>

          
     

      

        <ScrollView style={{width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

             <View style={styles.outsideview}>


             <TextField
              label='Description'
              value={post.description}
              tintColor={colors.color_primary}
              baseColor='white'
              multiline={true}
              textColor='white'
              onChangeText={(description) => this.setState({ description})}
              ref={(input) => { this.description = input; }}
             
            />

    
            </View>


            {
              this.checkMediaType()
  
              }



              <TouchableOpacity style={{flex:1}} onPress={() => {this.ChooseMediaSheet.open()}}>
                  <View style={{
                    width: dimension.width * 0.9, height: 50, backgroundColor: 'white', justifyContent: 'center',
                    alignItems: 'center', alignSelf: 'center', marginTop: 50, borderRadius: 5,
                    shadowColor: 'gray',
                    shadowRadius: 10,
                    shadowOpacity: 1,
                    elevation:10

                  }}>
                    <Text style={{ color: 'blue', fontWeight: 'bold' }}>Replace Media</Text>
                  </View>
                 </TouchableOpacity>

           

            <TouchableOpacity style={{width:'100%'}} onPress={() => {this.editPost()}}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 30, borderRadius: 5,
              shadowColor: 'gray',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Edit Post</Text>
            </View>
          </TouchableOpacity>


        </ScrollView>

    

          
              
    
            {this.state.loading_status &&
                    <View style={[
                      StyleSheet.absoluteFill,
                      { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
                    ]}>
                    <WaveIndicator color={colors.color_primary}/>
                    </View>
            }
           
          </SafeAreaView>
          </ImageBackground>
         

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditPost);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    imagebackground:{
    position: 'absolute',
    flex: 1,
    backgroundColor:'rgba(0,0,0,0.45)',
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,zIndex:-2
  },

  outsideview:{ 
    width: '90%', 
    justifyContent: 'center',
     alignSelf: 'center' ,
     marginBottom:30},

   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },
  imageStyle:{alignSelf:'center',
  height:dimension.height * 0.3,width:'90%',
  overflow:'hidden',flex:1},
  videoStyle:{width:'93%',height:dimension.height * 0.3,alignSelf:'center'},
  });