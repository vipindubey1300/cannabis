import React from 'react';
import { colors, urls } from '../Globals';
import { Button } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';
import { StackActions, NavigationActions} from 'react-navigation';
import firebase from 'react-native-firebase';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Image,
    TouchableOpacity,
    Alert,
    Dimensions,
    ToastAndroid
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import RBSheet from "react-native-raw-bottom-sheet";
import Snackbar from 'react-native-snackbar';



class Drawer extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         loading_status:false,
        };

    }


    changeStatus(){
      //status == 0 ..offline
      //status == 1 .. online
    
      console.log("Changing status")
    
      // firebase.database().ref('Users/'+ this.props.user.user_id).set({
      //   'active' : status == 1 ? 'online' :'offline',
      // }).then((data)=>{
      //     //success callback
      //     console.log('data written---' , JSON.stringify(data))
      // }).catch((error)=>{
      //     //error callback
      //     console.log('error ' , error)
      // })
    
    
      firebase.database().ref('Users/'+ this.props.user.user_id).update({
        'active' : 'offline',
    });
    
    
    
    }

    logout = async() =>{
      this.changeStatus()
        try {
         
      
          this.props.remove({});
      
      
      
          await AsyncStorage.removeItem("user_id");
          await AsyncStorage.removeItem("name");
          await AsyncStorage.removeItem("image");
          await AsyncStorage.removeItem("email");
      
          this.props.navigation.navigate("Login")
         
         
      
      
          return true;
        }
        catch(exception) {
          
          return false;
        }
      }
      
      
      remove = async() =>{
      
        Alert.alert(
          'Logout',
          'Are you sure to logout ?',
          [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
          },
          {
            text: 'OK',
            onPress: () => this.logout()
          }
          ],
          {
          cancelable: false
          }
        );
      
       
        
      }
      

    render() {
        return (
                <SafeAreaView style={{flex:1,backgroundColor:colors.color_primary}}>

                <RBSheet
                        ref={ref => {
                          this.RBSheet = ref;
                        }}
                        height={200}
                        duration={0}
                        customStyles={{
                          container: {
                            justifyContent: "center",
                            alignItems: "center",
                            borderTopLeftRadius:15,
                            borderTopRightRadius:15
                          },
                          // wrapper:{
                          //   backgroundColor:'grey'
                          // }
                        }}
                        animationType='fade'
                        closeOnDragDown={true}
                        minClosingHeight={10}

        >
          <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>

                  <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                        <Image style={{ height: 40, width: 40}}
                        resizeMode='contain'
                        source={require('../assets/logo.png')}>
                        </Image>

                        <Text style={{fontWeight:'bold',fontSize:16}}>Are you sure to Logout ? </Text>
                  
                  </View>


                  <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
                flex:2}}>

                    <View style={{flex:1,margin:10,elevation:10, shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                    <TouchableOpacity onPress={()=> this.logout()}
                    style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                      justifyContent:'center',alignItems:'center',elevation:10}}>
                    <Text style={{color:'white'}}>Yes</Text>
                    </TouchableOpacity>
                    </View>


                 <View style={{flex:1,margin:10,elevation:10,
                             shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                    <TouchableOpacity onPress={()=> this.RBSheet.close()}
                    style={{width:'100%',backgroundColor:'white',padding:20,
                      justifyContent:'center',alignItems:'center',elevation:10}}>
                    <Text style={{color:colors.color_primary}}>No</Text>
                    </TouchableOpacity>
                    </View>
                
                </View>
          </View>
        </RBSheet>




                <ScrollView style={{backgroundColor:colors.color_primary,flex:1}}>

                <View style={{justifyContent:'flex-end',alignItems:'flex-end',marginTop:40,marginRight:15}}>
                <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                    <Image style={{ height: 20, width: 30}}
                        source={require('../assets/cross.png')}>
                    </Image>
                </TouchableOpacity>
                </View>


                <View style={{justifyContent:'space-between',alignItems:'center',elevation:20}}>
                <TouchableOpacity onPress={()=> {
                   this.props.navigation.navigate('MyProfile');
          
                    const resetAction = StackActions.reset({
                    index: 0,
                    key: 'MyProfile',
                    actions: [NavigationActions.navigate({ routeName: 'MyProfile' })],
                  });
      
                  this.props.navigation.dispatch(resetAction);
                 }}>
                            <Image style={{ height: 130, width: 130, marginTop: 20,borderRadius:65,overflow:'hidden'}}
                            source={{uri:urls.base_url + this.props.user.image}}
                            ></Image>

                      </TouchableOpacity>
                </View>





                <View style={{flexDirection:"column", justifyContent:'center',alignItems:'center',marginTop:20}}>
                <Text style={{color:'white',marginTop:0}}>
                {this.props.user.name}
                </Text>
                        <Text style={{color:'white',fontSize:16,fontWeight:'bold',marginTop:10}}>
                        {this.props.user.email}
                        </Text>
                   

                 </View>

                 <View style={{marginTop:25}}></View>

                 <TouchableOpacity onPress={()=> {
                  this.props.navigation.navigate('Home');
                  {/* key is the key of stacnavogator inside drawer */}
                  {/* drawer ke andr jo h wo yaani */}
                    const resetAction = StackActions.reset({
                    index: 0,
                    key: 'HomePage',
                    actions: [NavigationActions.navigate({ routeName: 'Home' })],
                  });
      
                  this.props.navigation.dispatch(resetAction);
                 }}>
                 <View style={styles.viewStyle}>
                 <Text style={styles.textStyle}>Home</Text>
                 </View>
                 </TouchableOpacity>

               



                {/*  onPress={()=> {
                   this.props.navigation.navigate('MyProfile');
          
                    const resetAction = StackActions.reset({
                    index: 0,
                    key: 'MyProfile',
                    actions: [NavigationActions.navigate({ routeName: 'MyProfile' })],
                  });
      
                  this.props.navigation.dispatch(resetAction);
                 }}*/}
                 <TouchableOpacity onPress={()=> {
                   this.props.navigation.navigate('MyProfile');
          
                    const resetAction = StackActions.reset({
                    index: 0,
                    key: 'MyProfile',
                    actions: [NavigationActions.navigate({ routeName: 'MyProfile' })],
                  });
      
                  this.props.navigation.dispatch(resetAction);
                 }}>
                 <View style={styles.viewStyle}>
                 <Text style={styles.textStyle}>My Profile</Text>
                 </View>
                 </TouchableOpacity>

                  {
                    this.props.user.investor == 1
                    ?
                    <TouchableOpacity onPress={()=> {
                   this.props.navigation.navigate('InvestmentHub');
          
                    const resetAction = StackActions.reset({
                    index: 0,
                    key: 'InvestmentHub',
                    actions: [NavigationActions.navigate({ routeName: 'InvestmentHub' })],
                  });
      
                  this.props.navigation.dispatch(resetAction);
                 }}>
                 <View style={styles.viewStyle}>
                 <Text style={styles.textStyle}>Invest Cannabis</Text>
                 </View>
                 </TouchableOpacity>
                 :
                  null

                  }

                  {
                    this.props.user.entrepreneur == 1
                    ?
                    <TouchableOpacity onPress={()=> {
                   this.props.navigation.navigate('Entrepreneur');
          
                    const resetAction = StackActions.reset({
                    index: 0,
                    key: 'Entrepreneur',
                    actions: [NavigationActions.navigate({ routeName: 'Entrepreneur' , params: {route_name:'Home'}})],
                  });
      
                  this.props.navigation.dispatch(resetAction);
                 }}>
                 <View style={styles.viewStyle}>
                 <Text style={styles.textStyle}>Entrepreneur Cannabis</Text>
                 </View>
                 </TouchableOpacity>
                 :
                  null

                  }
              


               


                   {/* 
                onPress={()=> {
                  this.props.navigation.navigate('CannabisBusiness');
         
                   const resetAction = StackActions.reset({
                   index: 0,
                   key: 'News',
                   actions: [NavigationActions.navigate({ routeName: 'News' })],
                 });
     
                 this.props.navigation.dispatch(resetAction);
                }}*/}

                <TouchableOpacity onPress={()=> {
                      this.props.navigation.navigate('Upgrades');
         
                      const resetAction = StackActions.reset({
                      index: 0,
                      key: 'Upgrades',
                      actions: [NavigationActions.navigate({ routeName: 'Upgrades' })],
                     });

                     this.props.navigation.dispatch(resetAction);
                 
                }}>
                <View style={styles.viewStyle}>
                <Text style={styles.textStyle}>Cannabis Business </Text>
                </View>
                </TouchableOpacity>


                <TouchableOpacity onPress={()=> {
                      this.props.navigation.navigate('Transactions');
         
                      const resetAction = StackActions.reset({
                      index: 0,
                      key: 'Transactions',
                      actions: [NavigationActions.navigate({ routeName: 'Transactions' })],
                     });

                     this.props.navigation.dispatch(resetAction);
                 
                }}>
                <View style={styles.viewStyle}>
                <Text style={styles.textStyle}>Cannabis Transactions </Text>
                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=> {
                  this.props.navigation.navigate('News');
         
                   const resetAction = StackActions.reset({
                   index: 0,
                   key: 'News',
                   actions: [NavigationActions.navigate({ routeName: 'News' , params: {route_name:'Home'}})],
                 });
     
                 this.props.navigation.dispatch(resetAction);
                }}>
                <View style={styles.viewStyle}>
                <Text style={styles.textStyle}>Cannabis News</Text>
                </View>
                </TouchableOpacity>



                <TouchableOpacity onPress={()=> {
                this.props.navigation.navigate('Legal');
         
         const resetAction = StackActions.reset({
         index: 0,
         key: 'Legal',
         actions: [NavigationActions.navigate({ routeName: 'Legal' })],
        });

        this.props.navigation.dispatch(resetAction);
                }}>
                <View style={styles.viewStyle}>
                <Text style={styles.textStyle}>Legal</Text>
                </View>
                </TouchableOpacity>


                


                 {/* <TouchableOpacity onPress={()=> {}}>
                 <View style={styles.viewStyle}>
                 <Text style={styles.textStyle}>Privacy Policy</Text>
                 </View>
                 </TouchableOpacity> */}



                 {/*  onPress={()=> {   this.props.navigation.navigate('ContactAdmin');
         
                 const resetAction = StackActions.reset({
                 index: 0,
                 key: 'ContactAdmin',
                 actions: [NavigationActions.navigate({ routeName: 'ContactAdmin' })],
               });
   
               this.props.navigation.dispatch(resetAction);
              }}*/}
                 <TouchableOpacity onPress={()=> {   this.props.navigation.navigate('ContactAdmin');
         
         const resetAction = StackActions.reset({
         index: 0,
         key: 'ContactAdmin',
         actions: [NavigationActions.navigate({ routeName: 'ContactAdmin' })],
       });

       this.props.navigation.dispatch(resetAction);
      }}>
                 <View style={styles.viewStyle}>
                 <Text style={styles.textStyle}>Contact Us</Text>
                 </View>
                 </TouchableOpacity>


                 <TouchableOpacity onPress={()=> {this.RBSheet.open();
                this.props.navigation.closeDrawer()}}>
                 <View style={styles.viewStyle}>
                 <Text style={styles.textStyle}>Logout</Text>
                 </View>
                 </TouchableOpacity>

     




                </ScrollView>
                
                </SafeAreaView>
        )
    }
}


const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (userinfo) => dispatch(removeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Drawer);

const styles = StyleSheet.create({

        textStyle:{
            marginLeft:15,
            color:'white',
            fontSize:16,
            textAlignVertical:'center',
           
        },
       viewStyle: { 
        borderBottomColor:'white',
       borderTopColor:'white',
       borderWidth:0.3,
       height:null,
       padding:9,
    }
    
});