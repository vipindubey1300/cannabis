import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



class SellerProfile extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      posts:[],
      vibes_count:0,
      vibing_count:0,
      name:'',
      image:null,
      seller_flag:0,
      chat_flag:0,
      vibe_status:false
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
        {/* <TouchableOpacity onPress={()=>{ navigation.navigate("Discover",{route_name:'SellerProfile'}) }}>
                  <Image source={require('../assets/user.png')} style={{width:30,height:30}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={()=>{  navigation.navigate("Chats",{route_name:'SellerProfile'})}}>
                  <Image source={require('../assets/chat.png')} style={{width:30,height:30,marginLeft:20}} resizeMode='contain'/>
                  </TouchableOpacity> */}
            
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Profile</Text>
     </View>
    ),
  });
  
 
  vibeUser(){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {

        let result = this.props.navigation.getParam('result')
        var user_id = result['user_id']


        var formData = new FormData();

        formData.append('user_id',item);
        formData.append('follow_user_id',user_id);
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/vibe_user'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);

             Snackbar.show({
              title: responseJson.message,
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'UNDO',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


                              if(!responseJson.error){

                            this.setState({vibes_count:this.state.vibes_count + 1})
                            this.setState({vibe_status:true})
                            }
                            else{

                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: 'Try Again',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'UNDO',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }

  unVibeUser(user_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
        let result = this.props.navigation.getParam('result')
        var user_id = result['user_id']

  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('follow_user_id',user_id);
        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/unvibe_user'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
            //  Platform.OS === 'android' 
            //                     ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
            //                     : Alert.alert(responseJson.message)



                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
  
  
                              if(!responseJson.error){
  
                                this.setState({vibe_status:false})
                            }
                            else{
  
                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  getProfile(user_id){

   
   
        var formData = new FormData();

        formData.append('user_id',user_id);
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/get_profile'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){
                                var vibe = responseJson.vibes
                                var vibing = responseJson.vibing
                                var name = responseJson.user.name
                                var image = responseJson.user.user_image
                                var posts = responseJson.posts
                                
                                var seller_flag = responseJson.user.seller_flag
                                var chat_flag = responseJson.user.chat_flag

                                this.setState({
                                  vibes_count:vibe,
                                  vibing_count:vibing,
                                  name:name,
                                  image:image,
                                  posts:posts,
                                  seller_flag:seller_flag,
                                  chat_flag:chat_flag
                                })

                            }
                            else{

                             
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'UNDO',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: 'Try Again',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'UNDO',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
    
  }


  checkVibeStatus(user_id){

   
   
    var formData = new FormData();

    formData.append('to_id',user_id);
    formData.append('from_id',this.props.user.user_id);
    

                 // this.setState({loading_status:true})
                  let url = urls.base_url +'api/vibing_status'
                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData

                }).then((response) => response.json())
                      .then((responseJson) => {
              // this.setState({loading_status:false})
              console.log(JSON.stringify(responseJson))
         //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                          if(!responseJson.error){
                            this.setState({vibe_status:responseJson.vibe_status})

                           }
                        else{

                         
                          // Snackbar.show({
                          //   title: responseJson.message,
                          //   duration: Snackbar.LENGTH_SHORT,
                          //   backgroundColor:'black',
                          //   color:'red',
                          //   action: {
                          //     title: 'UNDO',
                          //     color: 'green',
                          //     onPress: () => { /* Do something. */ },
                          //   },
                          // });
                        }
                      }).catch((error) => {
                        this.setState({loading_status:false})
                        
                        Snackbar.show({
                          title: 'Try Again',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'UNDO',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });


                      });

}






  componentWillMount(){
   
 
   let result = this.props.navigation.getParam('result')
   var user_id = result['user_id']
   console.log("IDDDDddddddddddddddddddddd",user_id)
   console.log("IDDDD",this.props.navigation.state.params.result.user_id)

   this.getProfile(user_id)
   this.checkVibeStatus(user_id)
  
  }

  checkBuyPackagesChat(){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {

        let result = this.props.navigation.getParam('result')
   var user_id = result['user_id']


       var formData = new FormData();

        formData.append('from_id',item);
        formData.append('to_id',user_id);

        console.log(formData)
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/check_chat_status'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                              //     Snackbar.show({
                              //   title: "Chats Coming Soon",
                              //   duration: Snackbar.LENGTH_SHORT,
                              //   backgroundColor:'black',
                              //   color:'red',
                              //   action: {
                              //     title: 'OK',
                              //     color: 'green',
                              //     onPress: () => { /* Do something. */ },
                              //   },
                              // });


                              //niche wala 

                              let obj={
                                'to_id':user_id,
                                'to_name':this.state.name,
                                'to_image':this.state.image,
                                'to_status':'offline',
                                'to_typing':false
                                
                              }
                              this.props.navigation.navigate("ChatOutside",{result:obj})
                              

                                 
                              }


                           
                               else{

                                   
                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });



                                 if(responseJson.request_status == ''){

                                  
                               if(responseJson.to_buy == 0){
                                      //means samne wale ne chat ka package nahi kharida hai
                               }
                                else{
                                    let obj={
                                      'to_id':user_id,
                                      'to_name':this.state.name,
                                      'to_image':this.state.image,
                                      'to_status':'offline'
                                    }
                                    this.props.navigation.navigate("ChatPackages",{result:obj})

                               }
                             

                                 
                                 }
                                 else{
                                  
                                 }
                             
                            
                                }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                          
                            Snackbar.show({
                              title: 'Try Again',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  chatStatus(){

    let result = this.props.navigation.getParam('result')
    var user_id = result['user_id']

    

    if(this.state.seller_flag == 1){
    //  if(this.state.chat_flag == 1){

    //   let obj={
    //     'to_id':user_id,
    //     'to_name':this.state.name,
    //     'to_image':this.state.image,
    //     'to_status':'offline'
        
    //   }
    //   this.props.navigation.navigate("Chats",{result:obj})

    //  }
    //  else{
    //     //check if user buyed packages
      
    //  }

    this.checkBuyPackagesChat()

    }
    else{
      Snackbar.show({
        title: 'This User Is Not Seller',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
      });
    }
    
  }

  nextPage(item){
    // console.log('sdfsdfsdfsdfsdfsdfsdfsd',item.media_type)
     if(item.media_type == 1){
       //image 
       let obj={
         'image_url':urls.base_url + item.post_img
       }
       this.props.navigation.navigate("ImageView",{result:obj})
     }
     else if(item.media_type == 2){
       //image 
       let obj={
         'video_url':item.post_video
       }
       this.props.navigation.navigate("VideoView",{result:obj})
     }
   }
 



  renderItem = ({item, index}) => {
    return(
      <TouchableWithoutFeedback onPress={()=>{this.nextPage(item)}}>

      <View style={{width:'31%',margin:3,height:90,borderWidth:0.5,borderColor:'grey',
      overflow:'hidden',elevation:1}}>
      {
        item.media_type == '2'
        ?
        <Video source={{uri:urls.base_url+item.post_video}} 
        disableBack
        disableFullscreen
        disableVolume
        disableSeekbar
        disablePlayPause
        disableTimer
        muted={true}
        autoplay={false}
        paused={true}
        onPause={()=> console.log("Pause")}
        onPlay={()=> console.log("Play..")}
        resizeMode='stretch'
        showOnStart={true}
        seekColor={colors.color_primary}
        style={{width:'100%',height:'100%'}} />
        : 
        <Image source={{uri:urls.base_url+item.post_img}} resizeMode='stretch'
        style={{alignSelf:'center',height:'100%',width:'100%',
        overflow:'hidden',flex:1}}  />

      }

      {
        item.media_type == '2'
        ?
        <View style={{position:'absolute',top:0,bottom:0,left:0,right:0,justifyContent:'center',alignItems:'center'}}>
        <Image source={require('../assets/play.png')} style={{width:30,height:30}} resizeMode='contain'/>
        </View>
        : 
       null

      }
     
     

       

      </View>
      </TouchableWithoutFeedback>
    )
   }

   
 

    render()
    {


  
      
        return(
          <SafeAreaView style={styles.container}>

    

         
    
    
            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

{/* 
            <View style={{
              height:Dimensions.get('window').height * 0.25,
              width:'95%',
              flexDirection:'row',justifyContent:'space-between',padding:10,marginRight:40
            }}>
            <Image source={require('../assets/award.png')} 
            style={{width:60,height:60,marginLeft:0}} resizeMode='contain'/>
           
                  <View style={{height:'100%'}}>


                      <Image source={{uri:urls.base_url+ this.state.image}}
                      style={{width:100,height:100,marginLeft:0,borderRadius:50,overflow:'hidden'}} resizeMode='contain'/>

                      <View style={{position:'absolute',
                      top:Dimensions.get('window').height * 0.1,
                      left:Dimensions.get('window').width * 0.2,backgroundColor:'white',padding:2,borderRadius:20}}>
                      <Image source={require('../assets/add.png')} 
                      style={{width:25,height:25,marginLeft:0,overflow:'hidden'}} resizeMode='contain'/>
                      </View>

                      <Text style={{alignSelf:'center',margin:13}}>{this.state.name}</Text>
                  
                  </View>


          <Text></Text>
            </View>

            */}

            <View style={{
              height:Dimensions.get('window').height * 0.25,
              width:'95%',
              flexDirection:'row',justifyContent:'space-between',padding:10,flex:3
            }}>

            {
              this.state.seller_flag == 1

              ? 
            

              <View style={{flex:1,paddingBottom:20}}>
              <Image source={require('../assets/award.png')} 
                 style={{width:50,height:50}} resizeMode='contain'/>
                 </View>
              :

              <View style={{flex:1}}></View>

             
        

            }

           

            
                  
          
           
                  <View style={{height:'100%',marginLeft:0,backgroundColor:'white',flex:1,justifyContent:'center',alignItems:'center'}}>
 
                      <Image source={{uri:urls.base_url+ this.state.image}}
                      style={{width:100,height:100,marginLeft:0,borderRadius:50,overflow:'hidden'}} resizeMode='cover'/>

                    

                      <Text style={{alignSelf:'center',margin:13,alignSelf:'center',width:'100%',textAlign:'center'}}>{this.state.name}</Text>
                  
                  </View>

                  <View style={{flex:1,width:'100%'}}>
                    
                 </View>
           

        
            </View>

            {
              this.state.vibe_status == false
              ?
              <TouchableOpacity style={{width:'90%'}} onPress={()=> this.vibeUser()}>
            <View style={{width:'95%',height:null,padding:10,
          borderColor:'grey',borderWidth:0.5,borderRadius:6,alignSelf:'center',
          justifyContent:'center',alignItems:'center',marginBottom:20,backgroundColor:colors.color_primary}}>
            <Text style={{color:'white'}}>Vibe This user</Text>
          </View>
          </TouchableOpacity>
          :
          <TouchableOpacity style={{width:'90%'}} onPress={()=> this.unVibeUser()}>
            <View style={{width:'95%',height:null,padding:10,
          borderColor:'grey',borderWidth:0.5,borderRadius:6,alignSelf:'center',
          justifyContent:'center',alignItems:'center',marginBottom:20,backgroundColor:'red'}}>
            <Text style={{color:'white'}}>Unvibe This User</Text>
          </View>
          </TouchableOpacity>

            }

           


          {
            this.state.seller_flag == 1 ?
            <TouchableOpacity style={{width:'90%'}} onPress={()=> {
            this.chatStatus()
          }}>
            <View style={{width:'95%',height:null,padding:10,
          borderColor:'grey',borderWidth:0.5,borderRadius:6,alignSelf:'center',
          justifyContent:'center',alignItems:'center',marginBottom:20,backgroundColor:colors.color_primary}}>
            <Text style={{color:'white'}}>Secure Chat with User in The Vault</Text>
          </View>
          </TouchableOpacity>
          : null

          }

        


          <View style={{width:'100%',height:null,padding:10,
          borderColor:'grey',borderWidth:0.5,borderRadius:0,
          justifyContent:'space-around',alignItems:'center',marginBottom:20,flex:2,flexDirection:'row',borderColor:'grey'}}>



            <TouchableOpacity 
         onPress={()=> this.props.navigation.navigate("MyVibes",{name: this.state.name,
         id:this.props.navigation.getParam('result')['user_id']})}
 
          style={{flex:1,margin:10}}>
            <View style={{flex:1,alignItems:'center',justifyContent:'center',margin:10}}>
            <Text style={{fontSize:15,color:'grey',margin:3}}>{this.state.vibes_count}</Text>
            <Text style={{fontSize:17,color:'black',fontWeight:'bold',margin:3}}>Vibes</Text>
            </View>

            </TouchableOpacity>
            

            <TouchableOpacity 
         onPress={()=> this.props.navigation.navigate("MyVibes",{name:this.state.name,
         id:this.props.navigation.getParam('result')['user_id']})}
 
          style={{flex:1,margin:10}}>

            <View style={{flex:1,alignItems:'center',justifyContent:'center',margin:10}}>
            <Text style={{fontSize:15,color:'grey',margin:3}}>{this.state.vibing_count}</Text>
            <Text style={{fontSize:17,color:'black',fontWeight:'bold',margin:3}}>Vibing</Text>
            </View>

            </TouchableOpacity>
           
          </View>



          <Text style={{alignSelf:'flex-start',margin:10,fontWeight:'bold',color:'grey',fontSize:17}}>Posts</Text>


          <FlatList
          style={{marginBottom:10,width:'100%',padding:4}}
          data={this.state.posts}
         
          nestedScrollEnabled={true}
          numColumns={3}
          showsVerticalScrollIndicator={false}
          scrollEnabled={false}
          renderItem={(item,index) => this.renderItem(item,index)}
        />
              
    
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(SellerProfile);



const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:colors.color_primary
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });