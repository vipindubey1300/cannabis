import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { throwStatement } from '@babel/types';
import { StackActions, NavigationActions} from 'react-navigation';


class MyProfile extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      posts:[],
      vibes_count:0,
      vibing_count:0,
      name:'',
      image:null,
      imageSource:null,
      photo:null,
      dob:'',
      country_id:0,
      country_name:'',
      state_id:0,
      state_name:'',
      city_id:0,
      city_name:'',
      seller_flag:0
      
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
           
        <TouchableOpacity onPress={()=>{ navigation.navigate("Discover") }}>
          <Image source={require('../assets/user.png')} style={{width:20,height:20}} resizeMode='contain'/>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>{ navigation.navigate("Requests",{route_name:'MyProfile'}) }}>
          <Image source={require('../assets/notification.png')} 
          style={{width:20,height:20,marginLeft:20,marginRight:5}} 
          resizeMode='contain'/>
          {
            navigation.getParam('count') > 0
            ?
            <View style={{position:'absolute',
            top:-10,
            height:navigation.getParam('count').toString().length === 1 ? 20 : null,
            width:navigation.getParam('count').toString().length === 1 ? 20 : null,
            backgroundColor:'red',
            left:28,
           borderRadius:10,
            padding:4,
            justifyContent:'center',
            alignItems:'center'}}>
            <Text style={{color:'white',fontSize:11}}>{navigation.getParam('count')}</Text>

          </View>

          : null

            
          }
          
          </TouchableOpacity>


            {/* navigation.navigate("Chats")*/}
          <TouchableOpacity onPress={()=>{  navigation.navigate("Chats",{route_name:'MyProfile'})}}>
          <Image source={require('../assets/chat.png')} style={{width:20,height:20,marginLeft:15}} resizeMode='contain'/>
          </TouchableOpacity>

            
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{
                  navigation.navigate('Home');
                
                    const resetAction = StackActions.reset({
                    index: 0,
                    key: 'HomePage',
                    actions: [NavigationActions.navigate({ routeName: 'Home' })],
                  });
      
              navigation.dispatch(resetAction);
                  }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>My Profile</Text>
     </View>
    ),
  });

   foo =  (id,name,email,image) => {
  
    this.props.change({ user_id : id, name : name, image:image, email:email });
   }
  
  save1 = async () => {
   
    await this.foo("2","sadf","sadfksdfllflflfl","asdfdsf")
  
    let a = JSON.stringify(this.props.user)
    console.log("val----gggg-------",a)  
  }

  
  addPhoto() {
    const options = {
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
 
    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};

        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };
       

        this.setState({
          imageSource: source ,
          photo:photo,
 
        }); 
        
        this.changeUserImage(photo)

       
      }
    });
 }

 addPhotoCamera() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchCamera(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     

      this.setState({
        imageSource: source ,
        photo:photo,

      }); 
      
      this.changeUserImage(photo)

     
    }
  });
}
addPhotoGallery() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchImageLibrary(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     

      this.setState({
        imageSource: source ,
        photo:photo,

      }); 
      
      this.changeUserImage(photo)

     
    }
  });
}

 
next(){
  let obj={
    'name':this.state.name,
    'dob':this.state.dob,
    'country_id':this.state.country_id,
    'country_name':this.state.country_name,
    'state_id':this.state.state_id,
    'state_name':this.state.state_name,
    'city_id':this.state.city_id,
    'city_name':this.state.city_name
  }
  this.props.navigation.navigate("EditProfile",{result:obj})
}

changeUserImage= async (photo) =>{
    console.log("Get profile")
   AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       var formData = new FormData();

        formData.append('user_id',item);
         formData.append('image',photo);
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/update_profile_picture'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                     body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                
                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });

                                 var  user_id = responseJson.result.id
                                var name = responseJson.result.name
                                var email = responseJson.result.email
                                var image = responseJson.result.user_image


                                try {
                                  AsyncStorage.setItem('image', image);
                                 
                                 } catch (error) {
                                  
                                   //ToastAndroid.show(error.message,ToastAndroid.LONG)
                                 }

                                 this.foo(user_id,name,email,image)
  
                               this.getProfile()

                            }
                            else{

                             
                              
                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                  

                      
                      Snackbar.show({
                        title: "Try Again !",
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
    
  }

  getProfile(){
    console.log("Get profile")
   AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       var formData = new FormData();

        formData.append('user_id',item);
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/get_profile'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){
                                var vibe = responseJson.vibes
                                var vibing = responseJson.vibing
                                var name = responseJson.user.name
                                var image = responseJson.user.user_image
                                var posts = responseJson.posts

                                  var dob = responseJson.user.dob
                                  var country_id = responseJson.user.country.id
                                  var country_name = responseJson.user.country.name
                                  var state_id = responseJson.user.state.id
                                  var state_name = responseJson.user.state.name
                                  var city_id = responseJson.user.city.id
                                  var city_name = responseJson.user.city.name

                                  var seller_flag = responseJson.user.seller_flag



                                this.setState({
                                  vibes_count:vibe,
                                  vibing_count:vibing,
                                  name:name,
                                  image:image,
                                  posts:posts,
                                  dob:dob,
                                  country_id:country_id,
                                  country_name:country_name,
                                  state_id:state_id,
                                  state_name:state_name,
                                  city_id:city_id,
                                  city_name:city_name,
                                  seller_flag:seller_flag
                                })

                                var requestCount = responseJson.request_count
                                this.props.navigation.setParams({count:requestCount})

                            }
                            else{

                             
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                          
                            Snackbar.show({
                              title: 'Try Again',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
    
  }



  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  onSelect = data => {
    this.getProfile()
  };

  componentWillMount(){

    // this.props.navigation.addListener(
    //   'willFocus',
    //   () => {
    //     ToastAndroid.show("sdfsd",ToastAndroid.LONG)
    //     this.getProfile()
        
    //   }
    // );

    this.props.navigation.setParams({count:0})



    this.getProfile()
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getProfile()
      }
    );
  
  
  
  }

  nextPage(item){
   // console.log('sdfsdfsdfsdfsdfsdfsdfsd',item.media_type)
    if(item.media_type == 1){
      //image 
      let obj={
        'image_url':urls.base_url + item.post_img
      }
      this.props.navigation.navigate("ImageView",{result:obj})
    }
    else if(item.media_type == 2){
      //image 
      let obj={
        'video_url':item.post_video
      }
      this.props.navigation.navigate("VideoView",{result:obj})
    }
  }



  renderItem = ({item, index}) => {
    return(
      <TouchableWithoutFeedback onPress={()=>{this.nextPage(item)}}>

<View style={{width:'31%',margin:3,height:90,borderWidth:0.5,borderColor:'grey',
      overflow:'hidden',elevation:1}}>
      {
        item.media_type == '2'
        ?
        <Video source={{uri:urls.base_url+item.post_video}} 
        disableBack
        disableFullscreen
        disableVolume
        disableSeekbar
        disablePlayPause
        disableTimer
        muted={true}
        autoplay={false}
        paused={true}
        onPause={()=> console.log("Pause")}
        onPlay={()=> console.log("Play..")}
        resizeMode='stretch'
        showOnStart={true}
        seekColor={colors.color_primary}
        style={{width:'100%',height:'100%'}} />
        : 
        <Image source={{uri:urls.base_url+item.post_img}} resizeMode='stretch'
        style={{alignSelf:'center',height:'100%',width:'100%',
        overflow:'hidden',flex:1}}  />

      }

      {
        item.media_type == '2'
        ?
        <View style={{position:'absolute',top:0,bottom:0,left:0,right:0,justifyContent:'center',alignItems:'center'}}>
        <Image source={require('../assets/play.png')} 
        style={{width:30,height:30}} resizeMode='stretch'/>
        </View>
        : 
       null

      }
     
     

       

      </View>
      </TouchableWithoutFeedback>
    )
   }

   
 

    render()
    {


      const ImageSheet= () =>{
     
        return (
          <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={200}
          duration={0}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              borderTopLeftRadius:15,
              borderTopRightRadius:15
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType='fade'
          closeOnDragDown={true}
          minClosingHeight={10}

>
            <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>

                <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                      <Image style={{ height: 40, width: 40}}
                      resizeMode='contain'
                      source={require('../assets/logo.png')}>
                      </Image>

                      <Text style={{fontWeight:'bold',fontSize:16}}>Choose Image Source ! </Text>
                
                </View>


                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
              flex:2}}>

                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.addPhotoCamera()}
                  style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:'white'}}>Camera</Text>
                  </TouchableOpacity>
                  </View>


                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.addPhotoGallery()}
                  style={{width:'100%',backgroundColor:'white',padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:colors.color_primary}}>Gallery</Text>
                  </TouchableOpacity>
                  </View>
              
              </View>
            </View>
</RBSheet>

        )
      }
      
        return(
          <SafeAreaView style={styles.container}>

         <ImageSheet></ImageSheet>

      
        
    
            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>


            <View style={{
              height:Dimensions.get('window').height * 0.25,
              width:'95%',
              flexDirection:'row',justifyContent:'space-between',padding:10,flex:3
            }}>

          {
              this.state.seller_flag == 1

              ? 
            

              <View style={{flex:1,paddingBottom:20}}>
              <Image source={require('../assets/award.png')} 
                 style={{width:50,height:50}} resizeMode='contain'/>
                 </View>
              :

              <View style={{flex:1}}></View>

             
        

            }

            
                    {/*
            <Image source={require('../assets/award.png')} 
            style={{width:60,height:60,marginLeft:0}} resizeMode='contain'/>
            */}
           
                  <View style={{height:'100%',marginLeft:0,backgroundColor:'white',flex:1,justifyContent:'center',alignItems:'center'}}>
 
                      <Image source={{uri:urls.base_url+ this.state.image}}
                      style={{width:100,height:100,marginLeft:0,borderRadius:50,overflow:'hidden'}} resizeMode='cover'/>

                      <View style={{position:'absolute',
                      top:Dimensions.get('window').height * 0.1,
                      left:Dimensions.get('window').width * 0.2,backgroundColor:'white',padding:2,borderRadius:20}}>
                      <TouchableOpacity onPress={()=> this.RBSheet.open()}>
                      <Image source={require('../assets/add.png')} 
                      style={{width:25,height:25,marginLeft:0,overflow:'hidden'}} resizeMode='contain'/>
                      </TouchableOpacity>
                      </View>

                      <Text style={{alignSelf:'center',margin:13,alignSelf:'center',width:'100%',textAlign:'center'}}>{this.state.name}</Text>
                  
                  </View>

                  <View style={{flex:1,width:'100%'}}>
                    <Text style={{textDecorationLine: 'underline',textShadowColor:'grey',alignSelf:'flex-end'}}
                    onPress={()=> this.next()}
                    >Edit profile</Text>
                 </View>
           

        
            </View>


            <TouchableOpacity style={{width:'90%'}} onPress={()=>this.props.navigation.navigate("Upgrades",{ onSelect: this.onSelect,route_name:'MyProfile'})}>
            <View style={{width:'95%',height:null,padding:10,
          borderColor:'grey',borderWidth:0.5,borderRadius:6,alignSelf:'center',
          justifyContent:'center',alignItems:'center',marginBottom:20,backgroundColor:colors.color_primary,
          elevation:3,shadowOpacity:0.5}}>
            <Text style={{color:'white',fontWeight:'bold'}}>Upgrade Your Profile For More Features</Text>
          </View>
          </TouchableOpacity>


        
          <View style={{width:'100%',height:null,padding:10,
          borderColor:'grey',borderWidth:0.5,borderRadius:0,
          justifyContent:'space-around',alignItems:'center',marginBottom:20,flex:2,flexDirection:'row',borderColor:'grey'}}>

          {/*  onPress={()=> this.props.navigation.navigate("MyVibes",{name: this.props.user.name})} */}

          { /* onPress={()=> this.props.navigation.navigate("MyVibes",{name: this.props.user.name,route_name:'MyVibes'})} */}
          <TouchableOpacity onPress={()=> this.props.navigation.navigate("MyVibes",{name: this.props.user.name,id:this.props.user.user_id})}
           style={{flex:1,margin:10}}
         >
            <View style={{alignItems:'center',justifyContent:'center'}}>
            <Text style={{fontSize:15,color:'grey',margin:3}}>{this.state.vibes_count}</Text>
            <Text style={{fontSize:17,color:'black',fontWeight:'bold',margin:3}}>Vibes</Text>
            </View>
         </TouchableOpacity>



          {/*  onPress={()=> this.props.navigation.navigate("MyVibes",{name: this.props.user.name})} */}

         <TouchableOpacity 
         onPress={()=> this.props.navigation.navigate("MyVibes",{name: this.props.user.name,
         id:this.props.user.user_id})}
 
          style={{flex:1,margin:10}}>
            <View style={{alignItems:'center',justifyContent:'center'}}>
            <Text style={{fontSize:15,color:'grey',margin:3}}>{this.state.vibing_count}</Text>
            <Text style={{fontSize:17,color:'black',fontWeight:'bold',margin:3}}>Vibing</Text>
            </View>
            </TouchableOpacity>
           
          </View>
         
          <Text style={{alignSelf:'flex-start',margin:10,fontWeight:'bold',color:'grey',fontSize:17}}>Posts</Text>


          <FlatList
          style={{marginBottom:10,width:'100%',padding:4}}
          data={this.state.posts}
         
          nestedScrollEnabled={true}
          numColumns={3}
          showsVerticalScrollIndicator={false}
          scrollEnabled={false}
          renderItem={(item,index) => this.renderItem(item,index)}
        />
              
    
             
    
            </ScrollView>

              {/* floating action button */}
                     
              <View style={{position:'absolute',bottom:20,right:20}}>
              <TouchableOpacity onPress={()=> this.props.navigation.navigate('HomePage')}>
                      <Image style={{width: 60, height: 60,borderRadius:15,overflow:'hidden',marginRight:15}}  
              source={require('../assets/add1.png')} />
              </TouchableOpacity>

              </View>

    
              {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });