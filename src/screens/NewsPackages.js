import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import stripe from 'tipsi-stripe'


stripe.setOptions({
  publishableKey: 'pk_test_7BfmJjAx9dBFO2eGC4GGd1za',
 //sandox:pk_test_lH3nLCADTo6rIyxvns0P8igB008LHXo6qB
 //live : pk_live_j40fxUTzTRVqL0D8SWIIvdBy00NU75hBT2

androidPayMode: 'test',
//androidPayMode:'production'
})

 class NewsPackages extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
     packages:[ ]
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
       
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>News Packages </Text>
     </View>
    ),
  });

  
  getPackages(){
    this.setState({loading_status:true})

   
  
  


                      let url = urls.base_url +'api/news_packages'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
               
                     // body:formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                
                               
                                     this.setState({packages:responseJson.data})

                            }
                            else{

                             
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                : Alert.alert(responseJson.message)
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Platform.OS === 'android' 
                      ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      : Alert.alert("Try Again !")


                          });
  }




 



  stripeApi(token_id,obj){


    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {

    



        var formData = new FormData();
        
         formData.append('user_id',item);
         formData.append('stripe_token',token_id);
          formData.append('news_pack_amt',parseFloat(obj.price))
          formData.append('news_period',obj.period)
          formData.append('news_package_id',obj.id)
        
    console.log(JSON.stringify(formData))

        //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
          this.setState({loading_status:true})


          let url = urls.base_url +'api/make_payment'
        fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type':  'multipart/form-data',
        },
        body: formData

      }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({loading_status:false})
           //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
          // console.log(JSON.stringify(responseJson))

           Snackbar.show({
            title: responseJson.message,
            duration: Snackbar.LENGTH_SHORT,
            backgroundColor:'black',
            color:'red',
            action: {
              title: 'OK',
              color: 'green',
              onPress: () => { /* Do something. */ },
            },
          });


            if(!responseJson.error){
                  //success in inserting data


               
        
                     this.props.navigation.navigate("AddNews")

                 
                    
                 

          }else{

            }


            }).catch((error) => {
             // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
             
                      Snackbar.show({
                        title: 'Try Again',
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });

            });
      }
      else{
        //ToastAndroid.show("User not found !",ToastAndroid.LONG)
        // Platform.OS === 'android'
        // ?  ToastAndroid.show("User not found !", ToastAndroid.SHORT)
        // : Alert.alert("User not found !")
      }
    });

    //  ToastAndroid.show(price+"...."+bid+"..."+pid+"...", ToastAndroid.SHORT);

    }

  async stripe(item){

    //console.log(JSON.stringify(item))
    const options = {
      requiredBillingAddressFields: 'full',
      prefilledInformation: {
        email:this.props.user.email,
        billingAddress: {
          name: this.props.user.name,
          line1: '',
          line2: '',
          city: '',
          state: '',
          country: '',
          postalCode: '',
        },
      },
    }

    const token = await stripe.paymentRequestWithCardForm(options)
  // ToastAndroid.show(JSON.stringify(token.tokenId),ToastAndroid.LONG)
    var cardId = token.card.cardId
    var tokenId = token.tokenId
   this.stripeApi(tokenId,item)

    console.log("GETTTTINGGGGg",JSON.stringify(item))
  
  }




  componentWillMount(){
        this.getPackages()
  
  }

renderItem = ({item, index}) => {
    return(
    
     

          <View style={{width:'70%',height:null,
          borderWidth:0.6,borderColor:'grey',borderRadius:15,
          margin:35,justifyContent:'center',
          alignItems:'center',alignSelf:'center',shadowOpacity:0.3,backgroundColor:'#F8F8FF'}}>

            <View style={{backgroundColor:'blue',padding:6,borderRadius:35,
            height:70,width:70,alignItems:'center',justifyContent:'center',marginTop:-35,elevation:6,shadowOpacity:0.3}}>
            <Text style={{fontSize:14,fontWeight:'bold',color:'white'}}>${item.price}</Text>

            </View>

            <Text style={{margin:8,fontWeight:'bold',fontSize:16}}>{item.period} months</Text>

            <Text style={{margin:8,fontWeight:'300',fontSize:14}}>Can Post : {item.access} news</Text>

            <TouchableOpacity onPress={()=> this.stripe(item)}>
            <View style={{backgroundColor:'blue',padding:6,
            height:null,width:null,alignItems:'center',justifyContent:'center',margin:8,borderRadius:6}}>
            <Text style={{fontSize:14,color:'white'}}>Buy Now</Text>

            </View>
            </TouchableOpacity>

          
          
            


            </View>
         
  
    )
   }


  



   
 

    render()
    {


  
      
        return(
          <SafeAreaView style={styles.container}>

    

          
    
    
            <ScrollView style={{width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

           {/* content */}

           <View style={{backgroundColor:'#BAD5FF',padding:20,width:'100%'}}>

           <Text style={{textAlign:'center',margin:10}}
           >Publish to a global audience your cannabis related news. 
           Choose a publishing package below then proceed !</Text>

           <Text style={{textAlign:'center',margin:10,fontWeight:'bold',fontSize:17}}
           >Buy the Plans.</Text>

           </View>


        {/* content end */}


        <FlatList
          style={{marginBottom:10,width:'100%',padding:4,marginTop:30}}
          data={this.state.packages}
         // ItemSeparatorComponent = { this.FlatListItemSeparator }
          showsVerticalScrollIndicator={false}
          renderItem={(item,index) => this.renderItem(item,index)}
        />
             



         
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}


const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
    investorUser: () => dispatch(investorUser()),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(NewsPackages);





const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });