
import React from 'react';
import { colors,urls } from '../Globals';
import { Button } from 'react-native-elements';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';

class Welcome extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
       header: () => null
    } 
}

	constructor(props) {
		super(props);
	}
  componentWillMount(){
  
    let a = JSON.stringify(this.props.user)
    console.log("val welcome-----------",a)
    // console.log("val-----------",this.props.user[0]['user_id'])
    // console.log("val-----------",this.props.user[0]['user_image'])
    // console.log("val-----------",this.props.user[0]['user_name'])
    // console.log("val-----------",this.props.user[0]['email'])
   
    
     
  }
    
   
    getStarted()
    {
   
        this.props.navigation.navigate('Register')

    }

    login()
    {
       this.props.navigation.navigate('Login')
 
    }

    

	render() {
    const d = Dimensions.get("window")
        
        return (
            <ImageBackground
            source={require('../assets/bg.jpg')}
            resizeMode='stretch'
            style={{ position: 'absolute',
                flex: 1,
              
                width: d.width,
                height: d.height,zIndex:-2}}>
  
           <View style={styles.container}>
  
           <Image style={{height:'30%',width:270,marginTop:'35%',alignSelf:'center'}}
           source={require('../assets/logo.png')}
           resizeMode="contain"
           ></Image>


            <TouchableOpacity onPress={ ()=> this.getStarted()}>
              <View style={{width:'70%',height:50,backgroundColor:'white',justifyContent:'center',
              alignItems:'center',alignSelf:'center',marginTop:130,borderRadius:10}}>

                      <Text style={{color: 'blue',fontWeight: 'bold'}}>Get Started</Text>

              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=> this.login()}>
            <View style={{width:'70%',height:50,backgroundColor:'blue',justifyContent:'center',
            alignItems:'center',alignSelf:'center',marginTop:30,borderRadius:10}}>

                    <Text style={{color: 'white',fontWeight: 'bold'}}>Login</Text>

            </View>
            </TouchableOpacity>

         
           </View>
           
           </ImageBackground>

          )
	}
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    add: (userinfo) => dispatch(addUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Welcome);

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    buttonLargeContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height:40
      },
    primaryButton: {
        backgroundColor: '#FF0017',
        height:45
      },
    buttonText: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold'
      }

  });