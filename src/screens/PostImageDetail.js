import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import DateTimePicker from 'react-native-modal-datetime-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import Video from 'react-native-video';
//import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { throwStatement } from '@babel/types';



export default class PostImageDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      smiley_status:false,
      item:null,
      loading_status:false,
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'black',
    headerStyle: {
      backgroundColor: 'black',
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
            
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{  navigation.navigate('Home');
                
                const resetAction = StackActions.reset({
                index: 0,
                key: 'HomePage',
                actions: [NavigationActions.navigate({ routeName: 'Home' })],
              });
  
          navigation.dispatch(resetAction); }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

          
     </View>
    ),
  });




  showReaction(){
    
  }

  hideReaction(){
             
  }

  next(){
    
  }


  likePosts(post_id,like_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id',item);
        formData.append('post_id',post_id);
        formData.append('like_type',like_id);
        

                      this.setState({loading_status:true,smiley_status:false})
                      let url = urls.base_url +'api/like_post'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
     
                              if(!responseJson.error){

                                Platform.OS === 'android' 
                                ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                : Alert.alert(responseJson.message)

                                var com = {...this.state.item}
                                 com.like_flag = like_id
                  
                          this.setState({item:com})


                     
                            }
                            else{

                             
                               

                        Snackbar.show({
                          title: responseJson.message,
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                           
                        Snackbar.show({
                          title: 'Try Again !',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }




 _checkCondition(){
    let img;
    const {item} = this.state
    if(item.like_flag == 1){
        img = require('../assets/p1.png')
    }else if (item.like_flag == 2){
      img = require('../assets/p2.png')
    }else if (item.like_flag == 3){
      img = require('../assets/p3.png')
    }else  if (item.like_flag == 4){
      img = require('../assets/p4.png')
    }
    else{
      img = require('../assets/p1.png')
    }

    
    return img;   
};





 componentWillMount(){
  
  let result = this.props.navigation.getParam('result')
  this.setState(prevState => ({
    item: result['item']
  })); 
  
  }



   
 

    render()
    {

      const {item} = this.state
  
      
        return(
          <SafeAreaView style={styles.container}>

    

         
    
    
            <ScrollView style={{backgroundColor:'black',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

          <View style={{ width: '100%',
           justifyContent: 'center', alignSelf: 'center' }}>

          <View style={{flexDirection:'row',
            justifyContent:'flex-start',alignItems:'center'}}>
    
    
           
                 <Image source={{uri:urls.base_url+item.user_pic}}
                    style={{height:40,width:40,
                    overflow:'hidden',borderRadius:20,margin:10}}  />
    
                    <View style={{marginLeft:8}}>
                    <Text style={{marginBottom:5,fontWeight:'bold',color:'white'}}>{item.user_name}</Text>
                    
                    </View>
    
                   
            </View> 


              <Text style={{margin:15,color:'white',alignSelf:'center'
                }}>{item.description}</Text>

{
                  item.media_type == '2'
                    ?
                    <Video source={{uri:urls.base_url + item.post_video}}
                     ref={(ref) => {
                            this.player = ref
                          }} 
                        resizeMode='stretch'
                        controls={true}
                        paused={true}
                        onError={e => console.log(e)}
                        onLoad={()=> console.log()}
                        onBuffer={()=> {}}   
                        style={{width:'100%',height:Dimensions.get('window').height * 0.6}}/>
                    : 

                  
                    <Image source={{uri:urls.base_url+ item.post_img}}
                     resizeMode='cover'
                    style={{alignSelf:'center',height:Dimensions.get('window').height * 0.6,width:'99%',
                    overflow:'hidden',flex:1}}  />
                   
                   
                  } 
               



                  {/* 
                        
                <TouchableOpacity activeOpacity={0.6}
                style={{flexDirection:'row',alignItems:'center',width:50}}
                >


                <Image source={require('../assets/p1.png')}
                    style={{height:25,width:25,
                    overflow:'hidden',borderRadius:12,margin:10}}  />
    
                    <Text style={{color:'white'}}>{item.likes_count} likes</Text>
                </TouchableOpacity>

                */}


                <TouchableOpacity activeOpacity={0.6}
                style={{flexDirection:'row',alignItems:'center',width:50,marginTop:10}}
               // onPress={()=> this.likePosts(this.props.posts[this.props.index].id,1)}
                onLongPress={()=> {
                  //console.log("sdfhsdbfjhdsfbjhdsfbdhjfdhj")
                this.setState({smiley_status:true})
                }}
                onPress={()=>  this.likePosts(item.id,1)}
                >

                <Image source={this._checkCondition()}
                    style={{height:25,width:25,
                    overflow:'hidden',borderRadius:12,margin:10}}  />
    
                    <Text style={{color:'white'}}>{item.likes_count} likes</Text>
    
                    </TouchableOpacity>
    
    
    
                    {
                      this.state.smiley_status
                      ?
                      <View style={{position:'absolute',
                      bottom:10,left:10,
                      backgroundColor:'white',
                      width:null,flexDirection:'row',
                       alignItems:'center',borderRadius:10,
                      padding:5,borderWidth:1}}>
        
    
                      <TouchableOpacity  onPress={()=> {
                       this.likePosts(item.id,1)
                      }}>
                      <Image source={require('../assets/p1.png')}
                      style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                      </TouchableOpacity>
                    
                      <TouchableOpacity onPress={()=> {
                         this.likePosts(item.id,2)
                      //  this.setState({posts:  this.state.posts})
                      }}>
                    <Image source={require('../assets/p2.png')}
                      style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                      </TouchableOpacity>
    
                      <TouchableOpacity onPress={()=> {
                        this.likePosts(item.id,3)
                      }}>
                     <Image source={require('../assets/p3.png')}
                      style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                      </TouchableOpacity>
    
    
                      <TouchableOpacity onPress={()=> {
                        
                        this.likePosts(item.id,4)
                       
                      }}>
                     <Image source={require('../assets/p4.png')}
                      style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                      </TouchableOpacity>
    
    
                      </View>
                      : null 
                    }
    
                    
                 
              

           
           
  
              
              


            </View>
         
    
             
    
            </ScrollView>

             
    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}




const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });