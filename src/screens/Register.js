
import React from 'react';
import { colors,urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import Snackbar from 'react-native-snackbar';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ToastAndroid,
  TouchableWithoutFeedback
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';

import {getFCMToken} from '../config/DeviceTokenUtils';




class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      name: '',
      email: '',
      dob: '',
      password: '',
      cpassword: '',  
      loading_status:false,
      dob:'',
      country_id:'',
      countries:[],
      country_name:'',
      state_id:'',
      states:[],
      state_name:'',
      city_id:'',
      city_name:'',
      cities:[],
      deviceToken:'',

      cnf_password_visible:false,
      password_visible:false,
    };


  }


    
  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { 
      color : 'black', 
      flex: 1,
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 17,
    
      
  },
  headerLayoutPreset: 'center',
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: 'white',
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 5,
  elevation:10,//for android
    },
 // headerTitle: () => <Text>Login</Text>,
title:"Register",
  
    headerLeft: () => (
      <TouchableOpacity onPress={()=>{
       
       navigation.goBack()
      }}>
        <Image source={require('../assets/left-arrow.png')} style={{width:20,height:20,marginLeft:15}} resizeMode='contain'/>
        
        </TouchableOpacity>
    ),
    headerRight: () => (
      <View></View>
    ),
  });

  fetchCountries = async () =>{
    this.setState({loading_status:true})

               let url = urls.base_url +'api/countries'
                    fetch(url, {
                    method: 'POST',

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ countries : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }

  fetchStates = async (countryId) =>{
    this.setState({loading_status:true,state_id:'',city_id:'',city_name:'',state_name:''})

    var formData = new FormData();
    formData.append('country_id', countryId);

               let url = urls.base_url +'api/states'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    body: formData

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ states : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }
  fetchCities = async (stateId) =>{
    this.setState({loading_status:true,city_id:'',city_name:''})
    var formData = new FormData();
    formData.append('state_id',stateId);

    formData.append('country_id',this.state.country_id);


               let url = urls.base_url +'api/cities'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    body: formData

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ cities : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }



  componentDidMount() {
    this.fetchCountries()
  }


  convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
  }


  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
  _hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
    this.password.focus()
  }

  handleDatePicked = (date) => {

    console.log('A date has been picked: ',this.convertDate(date));
    this.setState({date_obj:this.convertDate(date),dob:date.toString().substring(4,15)})
    this._hideDateTimePicker();
  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }


  isValid() {
    const { name, email, password, cpassword,dob,country_id,state_id,city_id } = this.state;

    let valid = false;

    if (name.toString().trim().length > 0 
      && email.toString().trim().length > 0 
      && this.validateEmail(email.trim())
      && password.toString().trim().length > 7
      &&  cpassword.toString().trim().length > 7
      &&  dob.toString().trim().length > 0
      &&  country_id.toString().trim().length > 0
      &&  state_id.toString().trim().length > 0
      &&  city_id.toString().trim().length > 0) {
      valid = true;
    }



    else if (name.toString().trim().length == 0){
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter Cannabis Expression', ToastAndroid.SHORT)
      //   : Alert.alert('Enter Cannabis  Expression')

        Snackbar.show({
          title: 'Enter Cannabis Expression',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }

    else if (email.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter  Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter  Email')

        Snackbar.show({
          title: 'Enter  Email!',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }
    else if (!this.validateEmail(email.toString().trim())) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter valid Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter valid Email')

        Snackbar.show({
          title: 'Enter valid Email !',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }
    else if (dob.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        Snackbar.show({
          title: 'Enter date of birth !',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (password.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter password', ToastAndroid.SHORT)
      //   : Alert.alert('Enter password')

        Snackbar.show({
          title: 'Enter password !',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (password.toString().trim().length < 8) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Password should be 8-16 characters long', ToastAndroid.SHORT)
      //   : Alert.alert('Password should be 8-16 characters long')

        Snackbar.show({
          title: 'Password should be 8-16 characters long !',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (cpassword.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter confirm password', ToastAndroid.SHORT)
      //   : Alert.alert('Enter confirm password')

      Snackbar.show({
        title: 'Enter confirm password!',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });


      return false;
    }
    else if (cpassword.toString().trim().length < 8) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Confirm Password should be 8 characters long', ToastAndroid.SHORT)
      //   : Alert.alert('Confirm Password should be 8 characters long')

        Snackbar.show({
          title: 'Confirm Password should be 8 characters long',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (cpassword.toString().trim() != password.toString().trim()) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Confirm Password & Password should match', ToastAndroid.SHORT)
      //   : Alert.alert('Confirm Password & Password should match')

        Snackbar.show({
          title: 'Confirm Password & Password should match !',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (country_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter Country', ToastAndroid.SHORT)
      //   : Alert.alert('Enter Country')

        Snackbar.show({
          title: 'Select Country  !',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }

    else if (state_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter State', ToastAndroid.SHORT)
      //   : Alert.alert('Enter State')

      Snackbar.show({
        title: 'Select State !',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });


      return false;
    }

    else if (city_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter City', ToastAndroid.SHORT)
      //   : Alert.alert('Enter City')

      Snackbar.show({
        title: 'Select City !',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });
      return false;
    }
    

    return valid
  }


  register(){
   
    if(this.isValid()){
      this.setState({loading_status:true})

      var formData = new FormData();
       // var token = this.checkPermission()
       formData.append('email', this.state.email);
       formData.append('name', this.state.name);
       formData.append('password', this.state.password);
       formData.append('country_id', this.state.country_id);
       formData.append('city_id', this.state.city_id);
       formData.append('state_id', this.state.state_id);
       formData.append('dob', this.state.date_obj);
       formData.append('device_token', this.state.deviceToken);
       Platform.OS =='android'
       ?  formData.append('device_type',1)
       : formData.append('device_type',2)

       console.log("PARA---",JSON.stringify(formData))
     
               let url = urls.base_url +'api/signup'
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
             }).then((response) => response.json())
                   .then((responseJson) => {
                       this.setState({loading_status:false})
                console.log('RESPONSE--',responseJson)
                    if (!responseJson.error) {
                          var  user_id = responseJson.result.id
                          var name = responseJson.result.name
                          var email = responseJson.result.email
                          var image = responseJson.result.user_image
                          var investor = responseJson.result.investor_flag
                          var entrepreneur = responseJson.result.entrepreneur_flag


                          AsyncStorage.multiSet([
                            ["user_id", user_id.toString()],
                            ["email", email.toString()],
                            ["name", name.toString()],
                             ["image", image.toString()],
                             ["investor", investor.toString()],
                             ["entrepreneur", entrepreneur.toString()]
                            
                           ]);

     
                             this.props.add({ user_id, name,image,email,investor,entrepreneur });


                         this.props.navigation.navigate('HomePage');
                        
                          
                     
                       }else{
                         
                          
                          //  Platform.OS === 'android' 
                          //  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          //  : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                         }


                   }).catch((error) => {
                             this.setState({loading_status:false})
                            //  Platform.OS === 'android' 
                            //  ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                            //  : Alert.alert("Try Again !")

                            

                            Snackbar.show({
                              title: error.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                           
                            });
                   });
    }
   
    
  }
  

  backBtn() {
    this.props.navigation.goBack();
  }


  login() {
    this.props.navigation.navigate('Login')
  }

  componentWillMount(){
    getFCMToken().then(response => {  
      this.setState({
        deviceToken : response
      },()=>{
        console.log("Login FCM TOKEN----------",this.state.deviceToken)
      })
     
  });
  }

  render() {

    return (
      <SafeAreaView style={styles.container}>




        <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
        showsVerticalScrollIndicator={false}
       contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

        


          <Text style={{ marginTop: 20, fontWeight: 'bold', fontSize: 20 ,alignSelf:'flex-start',marginLeft:20}}>
          Let's get started !
         </Text>

          <Text style={{ marginTop: 5, fontSize: 13 ,alignSelf:'flex-start',marginLeft:20}}>
            Create an account to Cannabis Expressions to get started
          </Text>

          <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' ,marginBottom:30}}>


              <TextField
              label='Cannabis Expression'
              value={this.state.name}
              onChangeText={(name) => this.setState({ name})}
              ref={(input) => { this.name = input; }}
              onSubmitEditing = {() => this.email.focus()}
            />


            <TextField
              label='Email Address'
              keyboardType={Platform.OS === 'android' ? 'email-address' : 'email-address'}
              value={this.state.email}
              autoCapitalize='none'
              onChangeText={(email) => this.setState({ email })}
              ref={(input) => { this.email = input; }}
              onSubmitEditing = {() => this.setState({ isDateTimePickerVisible: true })}
            />

           

          <DateTimePicker
          mode="date"
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this._hideDateTimePicker}
         maximumDate={new Date()}
          >
          </DateTimePicker>




 {/* 
  date
*/}

<TouchableOpacity  onPress={this._showDateTimePicker}>
<View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',
borderBottomColor:this.state.dob.length > 0 ? 'grey' : 'grey',
borderBottomWidth:0.5,paddingBottom:5,marginTop:10}}>
    <View>
    {
      this.state.dob.length > 0
      ? <Text  style={{color:'grey',fontSize:13}}>Date of Birth</Text>
      : <Text></Text>
    }
    {
      this.state.dob.length > 0
      ?  <Text>{this.state.dob}</Text>
      : <Text style={{color:'grey',fontSize:15}}>Date of Birth</Text>
    }
   

    </View>


    <Image source={require('../assets/calendar.png')} style={{width:20,height:20,marginLeft:-23}} resizeMode='contain'/>
</View>
</TouchableOpacity>



          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
						<View style={{ width: '100%' }}>
            
            <TextField
              label='Password'
              value={this.state.password}
              secureTextEntry={!this.state.password_visible}  
              maxLength={16}
              blurOnSubmit={false}

              onChangeText={(password) => this.setState({ password })}
              ref={(input) => { this.password = input; }}
              onSubmitEditing = {() => this.confirm_password.focus()}
            />
						</View>
						<View style={{ marginLeft: -23 }}>
							<TouchableWithoutFeedback onPress={() => this.setState({ password_visible: !this.state.password_visible })}>
								{
									this.state.password_visible
										?
										<Image source={require('../assets/eye.png')} style={{ width: 30, height: 30, marginLeft: 0 }} resizeMode='contain' />
										:
										<Image source={require('../assets/eye-cross.png')} style={{ width: 30, height: 30, marginLeft: 0 }} resizeMode='contain' />
								}

							</TouchableWithoutFeedback>
						</View>
					</View>


      <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
						<View style={{ width: '100%' }}>
            <TextField
            label='Confirm Password'
            value={this.state.cpassword}
            secureTextEntry={!this.state.cnf_password_visible}  
            maxLength={16}
            blurOnSubmit={false}
            onChangeText={(cpassword) => this.setState({ cpassword })}
            ref={(input) => { this.confirm_password = input; }}
            onSubmitEditing = {() => this.country.focus()}
              
          />
						</View>
						<View style={{ marginLeft: -23 }}>
							<TouchableWithoutFeedback onPress={() => this.setState({ cnf_password_visible: !this.state.cnf_password_visible })}>
								{
									this.state.cnf_password_visible
										?
										<Image source={require('../assets/eye.png')} style={{ width: 30, height: 30, marginLeft: 0 }} resizeMode='contain' />
										:
										<Image source={require('../assets/eye-cross.png')} style={{ width: 30, height: 30, marginLeft: 0 }} resizeMode='contain' />
								}

							</TouchableWithoutFeedback>
						</View>
					</View>


            {/*

            <TextField
              label='Password'
              value={this.state.password}
              secureTextEntry={true}
              maxLength={16}
              onChangeText={(password) => this.setState({ password })}
              ref={(input) => { this.password = input; }}
              onSubmitEditing = {() => this.confirm_password.focus()}
            />


          */}

            {/* value extracor will show only that filed*/}

          <Dropdown
          label='Select Your Country'
          rippleCentered={true}
          valueExtractor={({value})=> value}
          onChangeText = {(value, index, data) =>{
            console.log(`Selected value: ${value} ${index} ${data[index].id}`)
           // value is text name or label
           //index is index of item
           //data[index].id is id of the country
           this.setState({country_id:data[index].id,country_name:data[index].name})
           this.fetchStates(data[index].id)

          }}
          data={this.state.countries}
          value={this.state.country_name}
          ref={(input) => { this.country = input; }}
        />

        <Dropdown
          label='Select Your State'
          data={this.state.states}
          rippleCentered={true}
          valueExtractor={({value})=> value}
          onChangeText = {(value, index, data) =>{
           this.setState({state_id:data[index].id,state_name:data[index].name})
           this.fetchCities(data[index].id)

          }}
          value={this.state.state_name}
        />
        <Dropdown
          label='Select Your City'
          data={this.state.cities}
          rippleCentered={true}
          valueExtractor={({value})=> value}
         
          onChangeText = {(value, index, data) =>{
           this.setState({city_id:data[index].id,city_name:data[index].name})

          }}
          value={this.state.city_name}
        />

            </View>

          

          <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' }}>
            <Text style={{ textAlign: 'center',color:'grey' }}>
              By clicking 'Register' you agree to the
              </Text>
         </View>

         <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
         <Text style={{ color: colors.color_primary, fontWeight: "bold" }}>Terms of Use</Text>
         <Text style={{ color: 'grey', marginLeft:5,marginRight:5}}>and</Text>
         <Text style={{ color: colors.color_primary, fontWeight: "bold" }}>Privacy Policy</Text>
         </View>

             
             


          <TouchableOpacity style={{width:'100%'}} onPress={() => this.register()}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 50, borderRadius: 5,
              shadowColor: 'gray',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Register</Text>
            </View>
          </TouchableOpacity>

        

          <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginBottom:20,marginTop:20}}>
         
          <Text style={{ color: 'black', marginLeft:5,marginRight:5}}> Already have an account?</Text>
          <Text style={{ color: colors.color_primary, fontWeight: "bold",marginBottom:0 }}
          onPress={()=> this.props.navigation.navigate("Login")}>Login</Text>
          </View>

         

        </ScrollView>


        {this.state.loading_status &&
          <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
          ]}>
            <WaveIndicator color={colors.color_primary}/>
          </View>
        }
      </SafeAreaView>

    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    add: (userinfo) => dispatch(addUser(userinfo)),
  
    
  }
}


export default connect(null, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  buttonLargeContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40
  },
  primaryButton: {
    backgroundColor: '#FF0017',
    height: 45
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  containerAct: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }


});