import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback,Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';




class NewsDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
       detail:null,
       comment:'',
       make_comment:''
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
  headerRight: () => (

    <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
      {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
  </View>
  ),
  headerLeft: () => (
    <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
         <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                </TouchableOpacity>

                <Text style={{marginLeft:20,color:'white',fontSize:16}}>News</Text>
   </View>
  ),
  });


  makeComment(news_id){
   if(this.state.comment.toString().trim().length > 0){

    Keyboard.dismiss()
    //var comm = this.state.comment

    


    this.setState({make_comment:this.state.comment,comment:''})


    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       Keyboard.dismiss()
       var comm = this.state.comment


       this.setState({comment:''})
  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('news_id',news_id);
        formData.append('comment',this.state.make_comment);

        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/comment_news'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false ,comment:'',make_comment:''})

                console.log('Comment',JSON.stringify(responseJson))

                  if(responseJson.error == false){

                    var com = {...this.state.detail}
                    com.comments = responseJson.comments
                    //detail.comment_count
                    com.comment_count = this.state.detail.comment_count + 1
                    this.setState({detail:com})


                    // var com = detail.comments
                  //this.setState({detail['comments']: responseJson.comments})
                  }
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
             Snackbar.show({
              title: responseJson.message,
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });
  
  
                              if(!responseJson.error){
  
                                this.setState({comment:''})
                           
                            }
                            else{
  
                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: error.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
   }else{
     return
   }
  }

  
  renderItem = ({item, index}) => {
    return(
      <View style={{width:'50%'}}>
     

          <View style={{height:null ,margin:6,
          padding:10,flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>

          <Image source={{uri:urls.base_url + item.user.user_image}} 
          style={{width:30,height:30,marginRight:10,borderRadius:25,overflow:'hidden'}}/>


  

                <View style={{backgroundColor:'#CDCDCD'
                ,paddingRight:20,paddingTop:10,paddingLeft:20,
                paddingBottom:10,borderRadius:20}}>

                  
                  <Text style={{fontWeight:'bold'}}>{item.user.name}</Text>
                  <Text style={{fontSize:12,margin:3}}>{item.comment} </Text>
                </View>



         </View>

         {/* 
         <Text 
         onPress={()=> this.commentBox.focus()}
         style={{marginTop:-15,alignSelf:'flex-end',
         textDecorationLine: 'underline',fontWeight: 'bold',
         fontStyle: 'italic'}}>Reply</Text>

         */}

          
      </View>
    )
   }


  componentWillMount(){
    let result = this.props.navigation.getParam('result')
    var detail = result['news_detail']

    this.setState({detail})
  
  }


    render()
    {

      const{detail} = this.state
  
      
        return(
          <SafeAreaView style={styles.container}>
  
    
          
    
    
            <ScrollView style={{backgroundColor:'#EBFAFF',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',padding:6,alignSelf:'center'}}>

           <View style={{
             width:Dimensions.get('window').width * 0.92,
             height:Dimensions.get('window').height * 0.23,
             alignSelf:'center'
            
           }}>

           <Image source={{uri:urls.base_url + detail.img}}
          resizeMode="stretch"
           style={{width:'100%',height:'100%'}}>
           </Image>
           </View>

          

               <TouchableOpacity activeOpacity={0.6}
                 style={{flexDirection:'row',alignItems:'center'}}>
            
                    <Image source={require('../assets/p1.png')}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:8}}  />

                      <Text>{detail.likes_count} likes</Text>

                </TouchableOpacity>

                
                <View style={{width:Dimensions.get('window').width * 1,
                backgroundColor:'black',
                 height:0.5,marginTop:2,marginBottom:1}}></View>



                <View style={{flexDirection:'row',
                justifyContent:'space-between',alignItems:'center'}}>

                  <TouchableOpacity>
                  <Image source={require('../assets/thumb-up.png')}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:10}}  />
                  </TouchableOpacity>
                  

                  <TouchableOpacity onPress={()=> this.commentBox.focus()}
                   style={{flexDirection:'row',
                alignItems:'center'}}>
                <Text style={{color:'grey'}}> {detail.comment_count} Comments</Text>
                  <Image source={require('../assets/comment-grey.png')}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:10}}  />
                  </TouchableOpacity>


                </View>


                <Text style={{margin:6,alignSelf:'flex-start',
                color:'grey',fontWeight:'bold',fontSize:17}}>{detail.title}</Text>


          <View style={{width:'55%',flexDirection:'row',alignItems:'center',
            justifyContent:'flex-start',margin:6}}>



                  <View style={{flexDirection:'row',width:'90%',alignItems:'center'}}>
                  <Image source={require('../assets/clock.png')}
                              style={{width:17,height:17,marginRight:6}} resizeMode='contain'/>

                  <Text style={{color:'grey',fontSize:13}}>{detail.created_at}</Text>
            
                </View>


                {/* <View style={{flexDirection:'row',width:'90%',alignItems:'center'}}>

                <Text style={{color:'black',fontSize:13,fontWeight:'bold'}}>Posted By :  </Text>

                  <Text style={{color:'grey',fontSize:13}}>{detail.posted_by}</Text>
            
                </View> */}


         </View>




         
            <Text  adjustsFontSizeToFit 
            style={{margin:5,color:'black',margin:10,paddingBottom:detail.comments.length == 0 ? 50 : 0,
            alignSelf:'flex-start'
              }}>{detail.description.toString()}</Text>

           {
             detail.comments.length == 0
             ? null 
             : 
             <View style={{paddingBottom:40, width:Dimensions.get('window').width * 0.99}}>
             <Text style={{margin:10,color:'black',fontWeight:'bold',fontSize:17}}>Comments</Text>
             <FlatList
              style={{marginBottom:10,width:'100%',padding:4}}
              data={detail.comments}
              showsVerticalScrollIndicator={false}
              renderItem={(item,index) => this.renderItem(item,index)}
            />
            </View>
           }

               
                

    

          





           
       
    
            </ScrollView>

               {/* comment section */}
                     
               <View style={{position:'absolute',bottom:0,right:0,
               left:0,height:60,backgroundColor:'white',elevation:10,shadowOpacity: 5,paddingRight:10,
               paddingLeft:10,paddingTop:7,paddingBottom:7}}>

<View style={{flexDirection:'row',alignItems:'center',
flex:4,borderColor:'black',borderWidth:0.4,marginLeft:4,marginRight:2,borderRadius:20,backgroundColor:'#F2F2F2'}}>


<View style={{flex:2.5}}>
<TextInput
value={this.state.comment}
onSubmitEditing={() => {this.makeComment(detail.id)}}
multiline={true}
ref={(input) => { this.commentBox = input; }}
placeholder={'Write Comment Here..'}
placeholderTextColor={colors.color_primary}
onFocus={()=> {}}
onChangeText={(comment) => this.setState({ comment : comment })}
style={{width:'100%',height:'100%',padding:10}}

/>

{/* onSubmitEditing={() => { this.commentBox.focus(); }} */}

</View>


<View style={{flex:0.5}}>
  
   <TouchableOpacity onPress={() =>{this.makeComment(detail.id)}}>
      <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/send-button-grey.png')} />
      </TouchableOpacity>
</View>






</View>
               
                </View>

    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(NewsDetail);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });