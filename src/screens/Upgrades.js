import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header , Overlay} from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,
  FlatList,TextInput
  ,PermissionsAndroid,
  TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



class Upgrades extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      upgrades:[ ],
     seller_status:false,
     investor_status:false,
     entrepreneur_status:false,
     investorVisible:false,
     entrepreneurVisible:false

     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        
    
    </View>
    ),
    headerLeft: () => (
       <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{
                           navigation.navigate(navigation.getParam('route_name','Home')) 
                       }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Upgrades </Text>
       </View>
    ),
  });

  




  upgradeNow(){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       
  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('upgrade_id',1);
        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/upgrade_as_seller'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  //console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                
                
                              if(!responseJson.error){
                               // this.setState({news:p})

                               this.props.navigation.pop()
                              // console.log("......",this.props.navigation.state.params.onSelect)

                               if(this.props.navigation.getParam('onSelect')  == undefined)
                                {
                                 
                                }
                                else{
                                  this.props.navigation.state.params.onSelect()
                                }
                             
                              
                           
                            }
                            else{
                              //this.getNews()
                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                      //       Platform.OS === 'android' 
                      // ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      // : Alert.alert("Try Again !")
                          console.log("ERRROR",this.props.navigation.state.params.onSelect)
  
                      Snackbar.show({
                        title: 'Try Again !',
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });
  
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  next(upgrade_id){
    //2 .. means investor
    //3 .. means enterpreneur
    let obj ={
      'upgrade_id': upgrade_id
    }
    this.props.navigation.navigate("Packages",{result:obj})

  }
  checkProfileRenewel(){
   
    var formData = new FormData();
 
    formData.append('user_id',this.props.user.user_id);


                      let url = urls.base_url +'api/package_statuss'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
               
              // console.log("NEWS",JSON.stringify( responseJson.status.news_flag))
          
                              if(!responseJson.error){
                                var chat_flag = responseJson.status.notify_chat
                                var seller_flag = responseJson.status.notify_seller
                                var investor_flag = responseJson.status.notify_investor
                                var entrepreneur_flag = responseJson.status.notify_entrepreneur
                                var news_flag = responseJson.status.notify_news

                                  //1 means buyed packages and its on deadline day
                               
                                  this.setState({
                                    investorVisible:investor_flag == 1 ? true : false,
                                    entrepreneurVisible:entrepreneur_flag == 1 ? true : false
                                  })
                                 

                               
                               
                              

                            }
                            else{

                            
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                      //       Platform.OS === 'android' 
                      // ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      // : Alert.alert("Try Again !")

                      Snackbar.show({
                        title: 'Try Again',
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });

                         this.setState({loading_status:false})
                          });
  }



  
  getProfile(){
    console.log("Get profile")
   AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       var formData = new FormData();

        formData.append('user_id',item);
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/get_profile'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                 // console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){
                               

                                  var seller_flag = responseJson.user.seller_flag
                                  var investor_flag = responseJson.user.investor_flag
                                  var entrepreneur_flag = responseJson.user.entrepreneur_flag
                                  


                                this.setState({
                                  seller_status : seller_flag == 0 ? false : true,
                                  investor_status : investor_flag == 0 ? false : true,
                                  entrepreneur_status : entrepreneur_flag == 0 ? false : true,
                                })

                            }
                            else{

                             
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                          
                            Snackbar.show({
                              title: 'Try Again',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
    
  }




  componentWillMount(){
    console.log('nnnnn',this.props.navigation.getParam('onSelect')  == undefined ? 1: 2)
      this.getProfile()
      this.checkProfileRenewel()
  
  }

renderItem = ({item, index}) => {
    return(
      <TouchableWithoutFeedback onPress={()=>{}}>
     

          <View style={{width:'98%',height:null,padding:10,
          borderWidth:0.6,borderColor:'grey',borderRadius:5,margin:5,justifyContent:'center',alignItems:'center'}}>

          <Image style={{height:50,width:50,margin:6}}
          source={{uri:urls.base_url + item.img}}/>

          <Text>{item.title}</Text>

          <Text>{item.description}</Text>

          <Text style={{color:colors.color_primary,fontWeight:'bold'}}>Upgrade Now</Text>

          
          
            


            </View>
         
      </TouchableWithoutFeedback>
    )
   }


  



   
 

    render()
    {

      const InvestorModal = ()=>{
        return(
         
          <Overlay
          isVisible={this.state.investorVisible}
          windowBackgroundColor="rgba(0, 0, 0, .5)"
          overlayBackgroundColor="white"
          width={300}
          height='auto'
          
        >
  
          <View style={{alignItems:'center',justifyContent:'center'}}>
                <View style={{padding:15,borderRadius:30,
                borderColor:'grey',borderWidth:1,marginTop:-35,
                backgroundColor:'white',elevation:4,shadowOpacity:0.5,marginBottom:15}}>
                <Image source={require('../assets/logo.png')} 
                  style={{width:30,height:30}} resizeMode='contain'/>
                
                </View>
  
                <Text style={{textAlign:'center',fontWeight:'bold',margin:6}}>Package Expired.</Text>
                <Text style={{textAlign:'center',color:'grey'}}>Please renew your Investor Package.</Text>
  
  
                  <View style={{flexDirection:'row'}}>
                <TouchableOpacity onPress={()=> {
                  this.setState({investorVisible:false,entrepreneurVisible:false})
                  this.next(2)
                 
                }}
                style={{backgroundColor:colors.color_primary,
                paddingLeft:20,
                paddingRight:20,
                paddingTop:6,
                paddingBottom:6,
                alignItems:'center',justifyContent:'center',margin:10}}>
                    <Text style={{color:'white',fontWeight:'bold'}}>Renew Now</Text>
  
                </TouchableOpacity>
  
                <TouchableOpacity onPress={()=> {
                  this.setState({investorVisible:false})
                 
                }}
                style={{backgroundColor:'red',
                paddingLeft:30,
                paddingRight:30,
                paddingTop:6,
                paddingBottom:6,
                alignItems:'center',justifyContent:'center',margin:10}}>
                    <Text style={{color:'white',fontWeight:'bold'}}>Cancel</Text>
  
                </TouchableOpacity>
  
  
                </View>
          </View>
        </Overlay>
        )
      }

      const EnterpreneurModal = ()=>{
        return(
         
          <Overlay
          isVisible={this.state.entrepreneurVisible}
          windowBackgroundColor="rgba(0, 0, 0, .5)"
          overlayBackgroundColor="white"
          width={300}
          height='auto'
          
        >
  
          <View style={{alignItems:'center',justifyContent:'center'}}>
                <View style={{padding:15,borderRadius:30,
                borderColor:'grey',borderWidth:1,marginTop:-35,
                backgroundColor:'white',elevation:4,shadowOpacity:0.5,marginBottom:15}}>
                <Image source={require('../assets/logo.png')} 
                  style={{width:30,height:30}} resizeMode='contain'/>
                
                </View>
  
                <Text style={{textAlign:'center',fontWeight:'bold',margin:6}}>Package Expired.</Text>
                <Text style={{textAlign:'center',color:'grey'}}>Please renew your Enterpreneur Package.</Text>
  
  
                  <View style={{flexDirection:'row'}}>
                <TouchableOpacity onPress={()=> {
                  this.setState({entrepreneurVisible:false,investorVisible:false})
                  this.next(3)
                 
                }}
                style={{backgroundColor:colors.color_primary,
                paddingLeft:20,
                paddingRight:20,
                paddingTop:6,
                paddingBottom:6,
                alignItems:'center',justifyContent:'center',margin:10}}>
                    <Text style={{color:'white',fontWeight:'bold'}}>Renew Now</Text>
  
                </TouchableOpacity>
  
                <TouchableOpacity onPress={()=> {
                  this.setState({entrepreneurVisible:false})
                 
                }}
                style={{backgroundColor:'red',
                paddingLeft:30,
                paddingRight:30,
                paddingTop:6,
                paddingBottom:6,
                alignItems:'center',justifyContent:'center',margin:10}}>
                    <Text style={{color:'white',fontWeight:'bold'}}>Cancel</Text>
  
                </TouchableOpacity>
  
  
                </View>
          </View>
        </Overlay>
        )
      }


  
      
        return(
          <SafeAreaView style={styles.container}>

                <InvestorModal/>
                <EnterpreneurModal/>

          
    
    
            <ScrollView style={{width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

           {/* one */}

           <TouchableWithoutFeedback onPress={()=>{}}>
     

           <View style={{width:'91%',height:null,padding:10,
           borderWidth:0.6,borderColor:this.state.seller_status ? 'blue' : 'grey',borderRadius:7,
           margin:7,justifyContent:'center',alignItems:'center', 
           backgroundColor:this.state.seller_status ? '#EEEEEE' : 'white'}}>

        <View style={{padding:8,borderColor:'grey',borderRadius:30,borderWidth:1}}>
                          <Image style={{height:40,width:40,margin:0}}
                          source={require('../assets/award.png')}/>

                </View>
 
        
 
          
 
           <Text style={{fontSize:17,fontWeight:'bold',margin:5}}>Seller of Cannabis Related Products </Text>
 
           <Text style={{textAlign:'center',margin:4}}>This upgrade allows you to set up your profile as a seller of cannabis related products and services.</Text>

           {
             this.state.seller_status  == false
             ?
             <Text onPress={()=> this.upgradeNow()}
              style={{color:colors.color_primary,fontWeight:'bold',fontSize:14,margin:9}}>Upgrade Now</Text>
              : null

           }
 
          
 
             </View>
          
       </TouchableWithoutFeedback>

        {/* one end */}

               {/* two */}

           <TouchableWithoutFeedback onPress={()=>{}}>
     

           <View style={{width:'91%',height:null,padding:10,
           borderWidth:0.6,borderColor:this.state.investor_status ? 'blue' : 'grey',borderRadius:7,margin:7,justifyContent:'center',alignItems:'center',
           backgroundColor:this.state.investor_status ? '#EEEEEE' : 'white'}}>

                  <View style={{padding:8,borderColor:'grey',borderRadius:30,borderWidth:1}}>
                          <Image style={{height:40,width:40,margin:0}}
                          source={require('../assets/investor.png')}/>

                </View>
 
        
 
           <Text style={{fontSize:17,fontWeight:'bold',margin:5}}>Investor Seeking Opportunity </Text>
 
           <Text style={{textAlign:'center',margin:4}}>This allows you as an investor to register with our investment hub to seek out the next big thing !.</Text>

              {
                this.state.investor_status  == false ?
                <Text onPress={()=> this.next(2)}
              style={{color:colors.color_primary,fontWeight:'bold',fontSize:14,margin:9}}>Register Now</Text>
           : null

              }
         
 
             </View>
          
       </TouchableWithoutFeedback>

        {/* two end */}


        
               {/* three */}

               <TouchableWithoutFeedback onPress={()=>{}}>
     

               <View style={{width:'91%',height:null,padding:10,
               borderWidth:0.6,borderColor:this.state.entrepreneur_status ? 'blue' : 'grey',borderRadius:7,margin:7,justifyContent:'center',alignItems:'center',
               backgroundColor:this.state.entrepreneur_status ? '#EEEEEE' : 'white'}}>
                <View style={{padding:8,borderColor:'grey',borderRadius:30,borderWidth:1}}>
                  <Image style={{height:40,width:40,margin:0}}
                  source={require('../assets/entrepreneur.png')}/>

                </View>
     
               <Text style={{fontSize:17,fontWeight:'bold',margin:5}}>Entrepreneur Seeking Investment </Text>
     
               <Text style={{textAlign:'center',margin:4}}>You ve got an idea but need funding! This upgrade provides the platform to list your idea in our investment hub.</Text>
     
               {
                 this.state.entrepreneur_status   == false ?
                 <Text onPress={()=> this.next(3)}
                style={{color:colors.color_primary,fontWeight:'bold',fontSize:14,margin:9}}>Read More</Text>
                : null
               }
     
                 </View>
              
           </TouchableWithoutFeedback>
    
            {/* three end */}



         
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Upgrades);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });