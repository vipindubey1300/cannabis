import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions} from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



class Requests extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      requests:[]
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
         
         
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                  <TouchableOpacity onPress={()=>{ 
                   // navigation.navigate(navigation.getParam('route_name','Home')) 

                    navigation.navigate(navigation.getParam('route_name','Home'))
                    {
                      navigation.getParam('route_name') == "Home"
                      ?
                      
                      
          
                        navigation.dispatch(StackActions.reset({
                        index: 0,
                        key: 'HomePage',
                        actions: [NavigationActions.navigate({ routeName: 'Home' })],
                        }))
                        : 
                        navigation.dispatch(StackActions.reset({
                        index: 0,
                        key: 'MyProfile',
                        actions: [NavigationActions.navigate({ routeName: 'MyProfile' })],
                        }))

                    }
                
                    
                    }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Requests</Text>
            </View>
    ),
  });

  requestAction(requestId,follow_user_id,actionType){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       
  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('follow_user_id',follow_user_id);
        formData.append('request_status',actionType);
       

        //here request_status=1 for accepted &
        //request_status=2 for rejected



        console.log("sadsds",JSON.stringify(formData))
        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/accept_reject_request'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
            //  Platform.OS === 'android' 
            //                     ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
            //                     : Alert.alert(responseJson.message)



                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
  
  
                              if(!responseJson.error){

                                this.setState(prevState => ({
                                      requests: prevState.requests.filter(request => request.id != requestId) 
                                    
                                    }));


                        // let filteredArray = this.state.people.filter(item => item !== e.target.value)
                        // this.setState({people: filteredArray});
  
                               
                              }
                            else{
  
                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }

  
  getRequests(){
                  var formData = new FormData();
                
                   formData.append('user_id',this.props.user.user_id);
                   this.setState({loading_status:true})

                   console.log("G-----",JSON.stringify(formData))
                      let url = urls.base_url +'api/request_list'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                      body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                
                               
                                     this.setState({requests:responseJson.data})

                            }
                            else{

                             
                               
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
  }






  componentWillMount(){
   this.getRequests()
  
  }

  next(user_id){
    //this.props.navigation.navigate("SellerProfile")

  
  
      let obj={
        'user_id':user_id
      }
        this.props.navigation.navigate("SellerProfile",{result:obj})
    
    
   
 }

renderItem = ({item, index}) => {
    return(
      <TouchableOpacity onPress={()=> this.next(item.user.id)}>
      <View style={{width:Dimensions.get('window').width * 1,padding:6,marginBottom:2,marginTop:2}}>

      <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>

          {/* left*/}
          <View style={{flexDirection:'row',
          justifyContent:'flex-start',alignItems:'center',}}>

              {
                       item.user.seller_flag == 1
                       ?
                            <View style={{position:'absolute',bottom:20,left:16}}>
                              <Image source={require('../assets/tag.png')} 
                                        style={{width:20,height:20}} resizeMode='contain'/>

                            </View>

                          : null

                     }


          <Image source={{uri:urls.base_url + item.user.user_image}} 
              style={{width:30,height:30,borderRadius:15,overflow:'hidden'}} 
              resizeMode='cover'/>
            
               <Text style={{marginLeft:15}}>{item.user.name}</Text>
              

          </View>
            {/* left end*/}

              {/* right */}
              <View style={{flexDirection:'row',
              justifyContent:'flex-start',alignItems:'center',marginRight:10}}>
             <TouchableOpacity onPress={()=>{this.requestAction(item.id,item.user.id,1)}}>
              <View style={{justifyContent:'center',
              alignItems:'center',
              paddingBottom:5,
              paddingTop:5,
              paddingLeft:10,
              paddingRight:10,backgroundColor:'green'}}>
              <Text style={{color:'white'}}>Accept</Text>

              </View>
              </TouchableOpacity>


              <TouchableOpacity onPress={()=>{this.requestAction(item.id,item.user.id,2)}}>
              <View style={{justifyContent:'center',
              alignItems:'center',
              paddingBottom:5,
              paddingTop:5,
              paddingLeft:10,
              paddingRight:10,backgroundColor:'red',marginLeft:10}}>
              <Text style={{color:'white'}}>Reject</Text>

              </View>
              </TouchableOpacity>
            

            </View>

                {/* right  end*/}
      </View>

      {/* <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#d3d3d3",
          marginBottom:3,marginTop:7
        }}
      /> */}
    


</View>
</TouchableOpacity>



    )
   }


  



   FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#aaa9ad",
          marginTop:3,
          marginBottom:3
        }}
      />
    );
  }

   s(){
    new Promise((resolve, reject) => {
			this.props.change({ user_id : 4, name : 'dssdsdfdsf', image:'image', email:'email' })
		})
		.then( success => {
			Alert.alert("Info", "Successfully Saved User Profile Information!");
		//	this.props.navigation.navigate('MainStackNavigation')
		})
		.catch( error => {
			console.log(error)
			Alert.alert("Error", 'Failed To Save Profile Information, Please try Again!')
		})
	}
    
   
 

    render(){
        return(
          <SafeAreaView style={styles.container}>

    
    
            <ScrollView style={{backgroundColor:'#EBFAFF',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

           {
             this.state.requests.length > 0
             ?

                  
              <FlatList
              style={{marginBottom:10,width:'100%',padding:4}}
              data={this.state.requests}
         ItemSeparatorComponent = { this.FlatListItemSeparator }
              showsVerticalScrollIndicator={false}
              renderItem={(item,index) => this.renderItem(item,index)}
            />
            : 
            this.state.loading_status ?
            null
            :

                <View style={{justifyContent:'center',alignItems:'center',height:'90%'}}>
            <Image source={require('../assets/no-req.png')} 
              style={{width:200,height:200,alignSelf:'center',marginTop:Dimensions.get('window').height * 0.27}} resizeMode='contain'/>
              </View>
                

           }


      
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Requests);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });