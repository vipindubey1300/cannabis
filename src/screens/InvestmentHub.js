import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



export default class InvestmentHub extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
       hubs:[]
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{ navigation.navigate("HomePage") }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Invest Cannabis</Text>
     </View>
    ),
  });

  
 




  getHubs(){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       
        this.setState({loading_status:true})

        var formData = new FormData();
      
        formData.append('user_id',item);
    
    
                          let url = urls.base_url +'api/get_investment_hubs'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                   
                          body:formData
    
                        }).then((response) => response.json())
                              .then((responseJson) => {
                      this.setState({loading_status:false})
                      console.log(JSON.stringify(responseJson))
                 //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                  if(!responseJson.error){
    
                                    
                                   
                                         this.setState({hubs:responseJson.data})
    
                                }
                                else{
    
                                 
                                  Snackbar.show({
                                    title: 'No Pitches Found',
                                    duration: Snackbar.LENGTH_SHORT,
                                    backgroundColor:'black',
                                    color:'red',
                                    action: {
                                      title: 'OK',
                                      color: 'green',
                                      onPress: () => { /* Do something. */ },
                                    },
                                  });
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                Snackbar.show({
                                  title: 'Try Again',
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
    
                              });
      
  
  
                        
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  next(item){

    let obj ={
      'item' : item,
    }

    this.props.navigation.navigate("InvestmentHubDetails",{result:obj})

  }






  componentWillMount(){
   this.getHubs()
  
  }

renderItem = ({item, index}) => {
    return(
    
     
        <TouchableOpacity 
        onPress={()=> this.next(item)}>
          <View style={{width:'93%',height:null,
          borderWidth:0.6,borderColor:'grey',borderRadius:15,
         justifyContent:'center',
          alignSelf:'center',padding:10,margin:10,
          }}>

          <Text style={{color:'black',fontWeight:'bold',margin:1}}>Pitch Title</Text>
          <View style={{flexDirection:'row',justifyContent:'space-between',width:'70%',alignItems:'center'}}>

              <View style={styles.parent}>
                <Text style={styles.title}>Country : </Text>
                <Text style={styles.info}>{item.country.name}</Text>
              </View>

              <View style={{flexDirection:'row'}}>
                <Text style={styles.title}>City : </Text>
                <Text style={styles.info}>{item.city.name}</Text>
              </View>

          </View>

          
          <View style={styles.parent}>
                <Text style={styles.title}>Contact Number : </Text>
                <Text style={styles.info}>{item.contact_no}</Text>
              </View>

              <View style={styles.parent}>
                <Text style={styles.title}>Contact Email : </Text>
                <Text style={styles.info}>{item.contact_email}</Text>
              </View>

              <View style={styles.parent}>
                <Text style={{color:'black',
                  fontSize:13}}>
                <Text style={{fontWeight:'bold',color:'grey'}}>Synopsis Of Idea : </Text>
                {item.synopsis_of_idea}</Text>
                
              </View>

              <Text style={{color:'blue',margin:6,
              fontWeight:'bold',fontSize:12}}>Read More... </Text>

          
            


            </View>
            </TouchableOpacity>
  
    )
   }


  



   
 

    render()
    {


  
      
        return(
          <SafeAreaView style={styles.container}>

    

          
    
    
            <ScrollView style={{width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>



                  <FlatList
                    style={{marginBottom:10,width:'100%',padding:4,marginTop:30}}
                    data={this.state.hubs}
                  // ItemSeparatorComponent = { this.FlatListItemSeparator }
                    showsVerticalScrollIndicator={false}
                    renderItem={(item,index) => this.renderItem(item,index)}
                  />
             



         
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}




const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },
  title:{
    color:'grey',
  fontWeight:'bold',
  marginTop:1,
  marginBottom:1,
  marginLeft:1
},
  info:{
    color:'black',
  fontSize:13,
  marginTop:1,
  marginBottom:1
},

parent:{
  flexDirection:'row',
  alignItems:'center'
}
  });