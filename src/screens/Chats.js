import React from 'react';
import { colors, urls } from '../Globals';
import { Slider,Overlay } from 'react-native-elements';
import firebase from 'react-native-firebase';
import { addUser, changeUser } from '../actions/actions';
import { GiftedChat } from 'react-native-gifted-chat'; 
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import Sound from 'react-native-sound';
import firebaseSDK from '../config/firebaseSDK';
import { Player, Recorder, MediaStates } from '@react-native-community/audio-toolkit';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,
  PermissionsAndroid,
  TouchableWithoutFeedback,
  Clipboard,
  TouchableHighlightBase
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {PacmanIndicator} from 'react-native-indicators';

import { sendNotificationToUser } from '../config/NotificationUtil';
import {showMessage} from '../config/snackmsg';
import ChatAudio from '../components/ChatAudio';


import Swipeable from 'react-native-gesture-handler/Swipeable';

let _sound = null
let _isAudioPlaying = false




const Message = (props) => {

  
  return (
   <Text style={{color:props.sendStatus ? 'black' : 'white',
   width:props.text.length > 20 ? 'auto' : 'auto'}}>{props.text}</Text>
  )
}

const ChatImage = (props) => {
  return (
    <TouchableOpacity onPress={()=> {
       let obj={
        'image_url':props.picture
      }
      props.navigation.navigate("ImageView",{result:obj})
    }}>
    <Image source={{uri:props.picture}}
  
    style={{height:150,width:150,padding:20}}>

    </Image>
    </TouchableOpacity>



                              
  )
}

// const ChatAudio = (props) => {
//   return (
     
//       _isAudioPlaying 
//       ?
//       <TouchableOpacity onPress={()=>{ stopSound(props.audio)}}>
//       <Image source={require('../assets/pause-button.png')} style={{width:40,height:50}} resizeMode='contain'/>
//       </TouchableOpacity>
//       :
//       <TouchableOpacity onPress={()=>{ playSound(props.audio)}}>
//       <Image source={require('../assets/play-button.png')} style={{width:40,height:50}} resizeMode='contain'/>
//       </TouchableOpacity>




    
  
                              
//   )
// }

const ChatOptions = (props)=>{
  return(
   
    <Overlay
    isVisible={props.visibilty}
    windowBackgroundColor="rgba(0, 0, 0, .5)"
    overlayBackgroundColor="white"
    width={300}
    height='auto'
    
  >

    <View style={{alignItems:'center',justifyContent:'center'}}>
         

          <Text style={{textAlign:'center',fontWeight:'bold',margin:6}}>Copy</Text>
          <Text style={{textAlign:'center',color:'grey'}}>Delete.</Text>
        


    </View>
  </Overlay>
  )
}




class Chats extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      messages: [],
      message:'',
      to_id:'',
      to_name:'',
      to_image:'',
      imageSource: null ,
      photo:null,
      to_device_token:'',
      pair_id:0,
      modalVisible:true,
      //path of the audio which is recorder
      audio_path:'',
      //if recorder is recording something
      isAudioRecording:false,
      //if oppeonent is typing
      isOpponentTyping:false,
      //if i am typing 
      isMyselfTyping:false,
      audioPLayingIndex:-1,

      //reply status 
      reply_status:false,
      reply_messageId:-1,


      //chat staus
      chatError:false,
      chatErrorMessage:''
     
     
    };
    this._isMounted = false;
    this._audioRecorder = null;
   // this._isAudioPlaying = false
    this.recordCount = 0
   


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
        {
          navigation.getParam('chatOptions') 
          ? 
          <View style={{flexDirection:'row'}}>
                    {
                      navigation.getParam('chatItem').type == 'text'
                      ?
                          <TouchableOpacity onPress={()=>{
                            Chats.copyText(navigation.getParam('chatItem'),navigation)
                            
                            }}>
                            <Image source={require('../assets/copy.png')} 
                            style={{width:20,height:20}} resizeMode='contain'/>
                            </TouchableOpacity>
                        :
                       
                      null

                    }
                   
                  <TouchableOpacity onPress={()=>{
                    //navigation.getParam('replyFeature')
                    navigation.state.params.replyFeature()
                    
                   

                  }}>
                  <Image source={require('../assets/reply.png')}
                   style={{width:20,height:20,marginLeft:10}} resizeMode='contain'/>
                  </TouchableOpacity>

                  {/* delete chat only for myself */}

                  {
                    navigation.state.params.result.to_id == navigation.getParam('chatItem').receiver_id
                    ?
                    <TouchableOpacity onPress={()=>{
                      Chats.deleteChat(navigation.getParam('chatItem'),navigation)

                    }}>
                    <Image source={require('../assets/delete.png')} style={{width:20,height:20,marginLeft:10}} resizeMode='contain'/>
                    </TouchableOpacity>
                    : null
                    
                   }

                   <TouchableOpacity onPress={()=>{
                   
                      Chats.deselectChat(navigation)
                  }}>
                  <Image source={require('../assets/cross.png')}
                   style={{width:17,height:17,marginLeft:10}} resizeMode='contain'/>
                  </TouchableOpacity>


                   

          </View>

          : null
        }
       
            
    </View>
    ),
    headerLeft: () => (
            <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>


                 <TouchableOpacity onPress={()=>{ navigation.pop() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Image source={{uri:urls.base_url + navigation.state.params.result.to_image}} 
                      style={{width:30,height:30,borderRadius:15,overflow:'hidden',
                      marginLeft:15}} 
                      resizeMode='cover'/>

                   <View>

              

                   <Text style={{marginLeft:10,color:'white',fontSize:13}}>{navigation.state.params.result.to_name}</Text>
                   {
                    navigation.state.params.result.to_typing
                    ?
                    <Text style={{marginLeft:10,color:'white',fontSize:10}}>typing....</Text>
                    : null
                    
                   }
                  

                  </View>
                  {
                    navigation.state.params.result.to_status == 'online'
                    ? <View style={{height:8,width:8,
                    borderRadius:6,backgroundColor:'#7CFC00',
                    marginLeft:7,marginTop:3}}></View>
                    : <View style={{height:8,width:8,
                    borderRadius:6,backgroundColor:'#FF0000'
                    ,marginLeft:7,marginTop:3}}></View>
                  }

                   

     </View>
    ),
  });


  static deselectChat = (navigation) =>{
    //Chats.setState({reply_messageId:-1,reply_status:false},()=> ToastAndroid.show(Chats.state.reply_status,ToastAndroid.SHORT))
    navigation.setParams({chatOptions:false,chatItem:null,replyFeature: this.replyFeature})
   
  }

 


  static copyText= (chatItem,navigation) => {
    if(chatItem.type == 'text'){
      Clipboard.setString(chatItem.message)
      showMessage('Text Message Copied To Clipboard')
    }
    else  if(chatItem.type == 'audio'){
     // Clipboard.setString(textToCopy)
      showMessage('Audio URL Copied To Clipboard')
    }
    else  if(chatItem.type == 'image'){
     // Clipboard.setString(textToCopy)
      showMessage('Image URL Copied To Clipboard')
    }

    navigation.setParams({chatOptions:false,chatItem:null})
   

  }

 static _delete = (chatItem,navigation) =>{
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        let result = navigation.getParam('result')
        var to_id = result['to_id']
        var to_name = result['to_name']
        var to_image = result['to_image']

        const pairId = item  < to_id
        ?  item + '_' + to_id
        : to_id + '_' + item
    
        firebase.database().ref('messages/' + pairId + '/' + chatItem.node_id).remove();
        navigation.setParams({chatOptions:false,chatItem:null})
  
      
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }







  static deleteChat = (chatItem,navigation) => {
   

    Alert.alert(
      'Delete',
      'Are you sure to delete this message ?',
      [
      {
        text: 'Cancel',
        onPress: () => {
          navigation.setParams({chatOptions:false,chatItem:null,replyFeature: this.replyFeature})
        },
        style: 'cancel'
      },
      {
        text: 'OK',
        onPress: () => {
          Chats._delete(chatItem,navigation)
        }
      }
      ],
      {
      cancelable: false
      }
    );
  



   

  }

  replyFeature = () => {
    let item = this.props.navigation.getParam('chatItem')
  
    
    this.setState({ reply_status: !this.state.reply_status ,
      reply_messageId : this.state.reply_messageId == -1 ? item.node_id : -1 });
  };
 
  componentWillUnmount(){

    this._isMounted = false;

    const pairId = this.props.user.user_id  < this.state.to_id
    ?  this.props.user.user_id + '_' + this.state.to_id
    : this.state.to_id + '_' + this.props.user.user_id
   




   firebase.database().ref('messgaes/' + pairId).off('child_added');
    //firebase.database().ref('pair_message/' + this.chatPairId).off('child_changed');
    //firebase.database().ref('pair_message/' + this.chatPairId).off('child_removed');
  
  }


  resetChatOptions(){
    const { navigation } = this.props;
    this.setState({reply_messageId:-1,reply_status:false})

    navigation.setParams({chatOptions:false,chatItem:null,replyFeature: this.replyFeature})

   
  }

  componentDidMount() {
    this._isMounted = true;
    this.resetChatOptions()
    
   // this.props.navigation.setParams({chatItem:true}) _increaseCount = () => {
    //this.setState({ count: this.state.count + 1 });
  };

  //  setTimeout(() => this.myFlatListRef.scrollToEnd(), 3000)

   
    // firebaseSDK.refOn(message =>
    //   this.setState(previousState => ({
    //     messages: GiftedChat.append(previousState.messages, message)
    //   }))
    // );
  


  changeSelfTypingStatus(isTyping){
    //ToastAndroid.show(t)
         var key_from = `${this.props.user.user_id}_typing`
              firebase.database().ref('recents/' + this.state.pair_id).update({
                [key_from] : isTyping
            });
  }
  checkBuyPackagesChat(from_id,to_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {

       


       var formData = new FormData();

        formData.append('from_id',item);
        formData.append('to_id',to_id);

        //console.log(formData)
        

                     // this.setState({loading_status:true})
                      let url = urls.base_url +'api/check_chat_status'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log("RESPONSE",JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                              
                                
                              }


                           
                               else{

                              
                            
                                }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                          
                            Snackbar.show({
                              title: 'Try Again',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }

  initialize(to_id){

    this.checkBuyPackagesChat(this.props.user.user_id,to_id)

    const pairId = this.props.user.user_id  < to_id
    ?  this.props.user.user_id + '_' + to_id
    : to_id + '_' + this.props.user.user_id



    //get device token of the to user

      firebase.database().ref('Users/' + to_id ).on('value', (snapshot)=>  {
          //console.log("TOKEN",snapshot.val().device_token)
          this.setState({
            to_device_token:snapshot.val() == null ? '' : snapshot.val().device_token
          })
      });

      
      firebase.database().ref('recents/' + pairId ).on('value', (snapshot)=>  {
    
        var key = `${to_id}_typing`

       console.log("TYPING",snapshot.val())


        this.setState({
          isOpponentTyping:snapshot.val() === null ? 'false' : snapshot.val()
        })

        let obj ={
          ...this.props.navigation.state.params.result,
          to_typing:snapshot.val() === null ? 'false' : snapshot.val()[key]
        }
        
        this.props.navigation.setParams({result: obj})
    });


            // if(this.state.isMyselfTyping){
            
            //           var key_from = `${this.props.user.user_id}_typing`
            //           firebase.database().ref('recents/' + pairId ).update({
            //             [key_from] : true
            //         });
            // }
            // else{
            //     var key_from = `${this.props.user.user_id}_typing`
            //     firebase.database().ref('recents/' + pairId ).update({
            //     [key_from] : false
            //   });
            // }
 


    //check if user is online or offline

    let uId = this.props.navigation.state.params.result.to_id

    firebase.database().ref('Users/'+ uId).on('value', (snapshot)=> {
      //console.log("STAUS",snapshot.val().active)

      let status = snapshot.val().active === null ? 'offline' : snapshot.val().active

      // let obj = Object.assign({}, this.props.navigation.state.params.result);  // creating copy of state variable jasper
      //   obj.to_status = status         // update the name property, assign a new value                 
      //   

      let obj ={
        ...this.props.navigation.state.params.result,to_status:status 
      }
      
      this.props.navigation.setParams({result: obj})
  });


      //working perfect
  // var ref = firebase.database().ref("messages/5_22");
  //   var query = ref.orderByChild("receiver_id").equalTo(5);
  //   query.on("value", function(snapshot) {
  //     snapshot.forEach(function(child) {
  //       console.log('kkkkkkk',child.key, child.val().receiver_id);
  //     });
  //   });

           

            

            
          

          console.log("FFFFFFFF",pairId)


            firebase.database().ref('messages/' + pairId).once('value',(snapshot)=> {
           //console.log("SNAPSHOT",snapshot.val())

              //this works for every child added and also this fetches previouly
              //and for every node inserted .. this resturns all the data 
              var temp = []

              snapshot.forEach((childSnapshot) =>{
             
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();
                // Display the data
               // console.log("childKey",childKey)
                   // console.log("childData",childData)

                   temp = [...temp , childData]

                  ///THIS WAS PREVIOS

              //   this.setState(previousState => ({
              //     messages: [...previousState.messages, childData]
              // }));


              //END  OF PREVIOS

                // var ref = firebase.database().ref("messages/" + pairId );
                      //   var query = ref.orderByChild("receiver_id").equalTo(this.props.user.user_id);
                      //   query.on("value", function(snapshot) {
                      //     snapshot.forEach(function(child) {
                      //       console.log('kkkkkkk',child.key, child.val().receiver_id);
                      //     });
                      //   });
                            

                     
        

                // this.setState({
                //   messages:childData
                // })
                
                  
               })

               //The shift() command will remove the first element of the array and the unshift() command will add an element to the beginning of the array.

               var arr = temp.reverse()

               //console.log("AAAAAAATRTTTT",arr.splice(0,1))
               arr.splice(0,1)
               if(arr.length == 0){
                 //means first time chat started so make recents
                 this.makeRecent(pairId,0,0,'text','')
               }
               this.setState({messages:arr})
             
          });




              //can be
          //child_changed
          //child_removed
          //child_added
          //value

          //once will return only first data
          //on will return all


          // retrieve the last record from `ref`
        firebase.database().ref('messages/'+ pairId).endAt().limitToLast(1).on('child_added', (snapshot)=> {

          //this will give only that object which was added .
          //.like value this is also called first time
            //console.log("PARENT",snapshot.val())


          let oldList = [...this.state.messages];

          //oldList.push(snapshot.val());
         // oldList.unshift(snapshot.val())
          oldList.splice(0, 0, snapshot.val())
         // oldList.pop()

          this.setState({messages:oldList},()=>{

          // console.log("new message ",this.state.messages);
          })

       

         // this.setState({messages:snapshot.val()},() => console.log(JSON.stringify(this.state.messages)))
          
              snapshot.forEach((subSnapshot) =>{
             
               //console.log("CHILDADDED",subSnapshot)

               //Below code is for seen unseeen features

               //console.log("__________",subSnapshot)

               const { user } = this.props;
               var id = user.user_id+'_read'
               if(snapshot.val().receiver_id == user.user_id){
                
                if(this._isMounted){
                 var ref =  firebase.database().
                    ref('messages/' + pairId + '/' + snapshot.key + '/' )

                    ref.update({read_status:this.props.user.user_id+'_read'});

                    

                  //   let messages = this.state.messages.map(el => (
                  //     el.receiver_id == user.user_id ? {...el, read_status : snapshot.val().receiver_id+'_read'}: el
                  //  ))
                  //    this.setState({ messages });

                    firebase.database().ref('messages/' + pairId + '/' + snapshot.key + '/').on('value',  (snapshot) => {
                      if(snapshot.val() !== null){
                        if(snapshot.val().read_status == this.props.user.user_id+'_read' ){
                          //means that message already read so no updation
                      }
                      }
                     
                      else{
                   
                      }
                    });
                    

                    // if(oldList.receiver_id == user.user_id){
                    //   oldList.read_status = this.props.user.user_id+'_read'
                    //   this.setState({messages:oldList})
  
                    // }
                    
                

                }

              }

              else{
                let messages = this.state.messages.map(el => (
                  el.receiver_id == user.user_id ? {...el, read_status : snapshot.val().receiver_id+'_read'}: el
               ))
                 this.setState({ messages });

              }
                
                  
               })

              //  var query = firebase.database().ref("messages/" + pairId ).
              //  orderByChild("receiver_id").equalTo(this.props.user.user_id);
              //  query.on("child_added", (snapshot) =>{
              //    console.log("AAAA",JSON.stringify(snapshot))
              //    snapshot.ref.update({ read_status:this.props.user.user_id+'_read'})
              //  });

              //  var ref = firebase.database().ref("messages/" + pairId );
              //           var query = ref.orderByChild("receiver_id").equalTo(this.props.user.user_id);

              //           query.on("child_added", (snapshot) => {
              //           //  alert(JSON.stringify(query))
              //             snapshot.forEach((child) => {
              //               alert("doing ")
              //               child.read_status.ref.set({ read_status:this.props.user.user_id+'_read'})
              //             });
              //           });

        
          
      });





      firebase.database().ref('messages/'+ pairId).orderByChild('read_status').on('child_changed',  (snapshot)=> {
  
          //console.log("read_status......",snapshot)
        if(snapshot.val().sender_id == this.props.user.user_id){

          let messages = this.state.messages.map(el => (

             (el.node_id  ==  snapshot.val().node_id) || el.node_id === undefined 
              ? 
              {...el, read_status: snapshot == null ? false : snapshot.val().read_status }
               : el
          
         ))
          this.setState({ messages },()=> console.log('done'));                

           //let messages = [...this.state.messages]
          // let index = this.state.messages.findIndex((el) =>{
          //   //alert(el.node_id)

          //   //console.log("+++++ comparing" ,this.state.messages)
          //   (el.node_id  ==  snapshot.val().node_id) || el.node_id === undefined


          //   console.log(")))))))))))", index)
          // } );

          
          // var tempArr = [...this.state.messages]
          // tempArr[index] = snapshot.val()
          // this.setState({messages:tempArr})

          //last wale me node id nhi aa rha isiliye
          //el.node_id == undefined
          
        //  messages[index] = snapshot.val().read_status                
        //   this.setState({ messages }); 
         }
        //  console.log('Name of the field/node that was modified',snapshot.key); 
        // console.log('Value of the field/node that was modified',snapshot.val());
        // console.log("The updated " + 
        //   snapshot.key + " is " + JSON.stringify(snapshot.val())); 

    
           // var tempArr = [...messages]
           // tempArr = snapshot.val()
            //this.setState({messages:snapshot.val()})

          //  let messages = [...this.state.messages]; 
    
            snapshot.forEach((childSnapshot) =>{
        
        
        
            });
       
    }, (error) => {
      console.log(error)
    });


    //on delete message this callback will be called
    firebase.database().ref('messages/'+ pairId).on('child_removed', (snapshot)=> {
      //remove the deleted message from the state 
       let oldList = this.state.messages

       //oldList.filter(chat => console.log('-------------',chat.node_id ));
       console.log('/////////////////',oldList[0].node_id)



       let list = oldList.filter(chat => (
         chat.node_id != snapshot.val().node_id 
       
       )  
      );

      console.log('list.../////////////////',list)


      //in real chat pic deleting was not reflected in others sird
      
      let enhancedList = list.filter(chat => (
        chat.node_id != undefined
      
      )  
     );

     console.log('enhancedList.../////////////////',enhancedList)



      // console.log('deleted message-----------',list)

           this.setState({messages:enhancedList})

          snapshot.forEach((subSnapshot) =>{
           
              
           })

    });



  }


  componentWillMount() {
    //firebaseSDK.refOff();
    let result = this.props.navigation.getParam('result')
    var to_id = result['to_id']
    var to_name = result['to_name']
    var to_image = result['to_image']



    const pairId = this.props.user.user_id  < to_id
    ?  this.props.user.user_id + '_' + to_id
    : to_id + '_' + this.props.user.user_id



    this.setState({
      to_id:to_id,
      to_name:to_name,
      to_image:to_image,
      pair_id:pairId

    })


    console.log("RECIEVED",JSON.stringify(result))

    this.initialize(to_id)


 

  }



  makeRecent(id,to_id,from_id,messageType,message){
    var d = new Date()
    var hours = d.getHours()
    var minutes = d.getMinutes()
    var time = hours + ':' + minutes


    var key_to = `${this.state.to_id}_typing`
    var key_from = `${this.props.user.user_id}_typing`

    //[] is used in ES6 for the variable name 

    
    firebase.database().ref('recents/' + id).set({
      last_message :message, 
      timestamp:firebase.database.ServerValue.TIMESTAMP,
      time:time,
      msg_type:messageType,
      receiver_id:this.state.to_id,
      receiver_name:this.state.to_name,
      receiver_image:this.state.to_image,
      sender_id:this.props.user.user_id,
      sender_name:this.props.user.name,
      sender_image:this.props.user.image,
      [key_to] : false,
      [key_from]:false


      
     
    }).then((data)=>{
        //success callback
       // this.makeRecent()
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
  }

  upload = async (filepath: string, filename: string, filemime: string) => {
        const metaData = { contentType: filemime };
        const res = await firebase
            .storage()
            .ref(`${this.state.pair_id}/${filename}`)
            .putFile(filepath, metaData)
            
            // put image file to GCS

            //console.log("RES",JSON.stringify(res))
              return res;
    };



    sendAudio = async(id,to_id,from_id) =>{

      let name = this.makeRandomString() + '.aac'
      const res = await this.upload(this.state.audio_path, name, `audio`);
     console.log("AUDIOOSSSSSSS",this.state.audio_path)

      var d = new Date()
      var hours = d.getHours()
      var minutes = d.getMinutes()
      var time = hours + ':' + minutes

      let reply_id = this.state.reply_messageId
      let reply_status = this.state.reply_status

      if(this.state.reply_status) this.resetChatOptions()


        


      const dataMessage = {

        message : '', 
        sender_id:from_id,
        receiver_id:to_id,
        timestamp:Date.now(),
        type:'audio',
        read_status:to_id + '_unread',
        picture:'',
        audio:res.downloadURL,
        time:time,
        reply_status: reply_status,
        reply_id : reply_id
       
      
      }
      var ref = firebase.database().ref('messages/' + id)

                  var ref = firebase.database().ref('messages/' + id)
                  let dbRef =  ref.push(dataMessage).then((data)=>{
                    
                      this.addKey(id,to_id,from_id,data.key,dataMessage)
                      this.makeRecent(id,to_id,from_id,'audio','')
                      this.sendNotification('Audio Recieved !')
                 
                  }).catch((error)=>{
                      //error callback
                      console.log('error ' , error)
                      ToastAndroid.show(error.message,ToastAndroid.LONG)
                  })
      


      

    }
 

  sendImage = async(photo,id,to_id,from_id) => {

                const res = await this.upload(photo.uri, photo.name, `image/jpeg`); // function in step 1
               
          //Content-Type: audio/mpeg"
          //for mp3

                console.log("IMAGERES",res.downloadURL)


                var d = new Date()
                var hours = d.getHours()
                var minutes = d.getMinutes()
                var time = hours + ':' + minutes

                let reply_id = this.state.reply_messageId
                let reply_status = this.state.reply_status
          
                if(this.state.reply_status) this.resetChatOptions()



                const dataMessage = {
        
                  message : '', 
                  sender_id:from_id,
                  receiver_id:to_id,
                  timestamp:Date.now(),
                  type:'image',
                  read_status:to_id + '_unread',
                  picture:res.downloadURL,
                  audio:'',
                  time:time,
                  reply_status: reply_status,
                  reply_id : reply_id
                 
                
              }

                // const data =  {
                //   message : res.downloadURL,
                //   sender_id:from_id,
                //   receiver_id:to_id,
                //   timestamp:firebase.database.ServerValue.TIMESTAMP
                 
                // }

                var ref = firebase.database().ref('messages/' + id)

                // console.log("KEYNEWW",ref.push().key)

                  var ref = firebase.database().ref('messages/' + id)
                  let dbRef =  ref.push(dataMessage).then((data)=>{
                      //success callback
                     // console.log("KEY",JSON.stringify(data.key))
                      this.addKey(id,to_id,from_id,data.key,dataMessage)
                      this.makeRecent(id,to_id,from_id,'image','')
                      //this.setState({message:''})
                      this.sendNotification('Image Recieved !')


                      
                    // ToastAndroid.show('success callback',ToastAndroid.LONG)
                  }).catch((error)=>{
                      //error callback
                      console.log('error ' , error)
                      ToastAndroid.show(error.message,ToastAndroid.LONG)
                  })
      

      //EXTRA

        //firebase.database().ref('users/' + userId).set(data);


        // const ref = firebase.storage().ref('path/to/image.jpg');
        // const url = await ref.getDownloadUrl();
  }

  addPhoto() {
    const options = {
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
 
    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};

        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };
       

        this.setState({
          imageSource: source ,
          photo:photo,
 
        });

    let result = this.props.navigation.getParam('result')
    var to_id = result['to_id']
    var to_name = result['to_name']
    var to_image = result['to_image']

    
    var from_id = this.props.user.user_id



    if(from_id < to_id){
       let str = from_id + '_' + to_id
       this.sendImage(photo,str,to_id,from_id)
    }
    else{
        let str = to_id + '_' + from_id
        this.sendImage(photo,str,to_id,from_id)
    }
       

        {/*photo is a file object to send to parameters */}
      }
    });
 }


  addKey(pairId,to_id,from_id,key,data){

    this.props.navigation.setParams({chatOptions:false,chatItem:null})
   
   

    var o = Object.assign({}, data);
    o.node_id = key;

          firebase.database().ref('messages/'+ pairId + '/' + key).set(o).then((snapshot)=>{
            console.log('////////....',snapshot)

          //   let messages = this.state.messages.map(el => (
          //      // console.log("RESSS",el.timestamp  ==  data.timestamp)
          //      return (el.timestamp  ===  data.timestamp) 
          //      ? 
          //       {...el, node_id: key }
          //       : el

          // ))

          const messages = this.state.messages.map(el => {
            return el.timestamp  ===  data.timestamp ?  {...el, node_id: key } : el 
          })
        //   console.log("+++++++++++++++++",data.timestamp)
        //   let length = this.state.messages.length 
        //   for(let i = 0; i < length; i++){
        //           //console.log(".............",this.state.messages[i].timestamp)

        //    if(this.state.messages[i].timestamp == data.timestamp){
        //       console.log(';;;;;;;;;;;;',this.state.messages[i])
        //    }
         
        //  }
              
              //success callback
             this.setState({ messages : messages},()=> console.log('Added for ...',this.state.messages)); 
             
          }).catch((error)=>{
              //error callback
              console.log('error ' , error)
          })
  }



  sendNotification(currentMessage){
    let title =  "New Message from " + this.props.user.name
    
    const notification = {
      "notification": {
          "title": title,
          "body": currentMessage,
          "sound": "default",
       
      },
      "data" : {
        "to_id" : this.props.user.user_id,
        "to_name":this.props.user.name,
        "to_image":this.props.user.image,
        'to_status':'offline'


      
      },
      "priority": "high",
      //"action" : "Chats"

  }


    sendNotificationToUser(this.state.to_device_token, notification);
  }



  writeChat(id,to_id,from_id){

    

//     const ref = firebase.database().ref('posts').push();
// const key = ref.key;

   // var ref = firebase.database().ref('messages/' + id)

     // console.log("KEYNEWW", this.state.reply_messageId)

      var d = new Date()
      var hours = d.getHours()
      var minutes = d.getMinutes()
      var time = hours + ':' + minutes

      let currentMessage = this.state.message
      let reply_id = this.state.reply_messageId
      let reply_status = this.state.reply_status

      this.setState({message:''})
      if(this.state.reply_status) this.resetChatOptions()
     
    let timestamp = firebase.database.ServerValue.TIMESTAMP

      const dataMessage = {
        
          message : currentMessage, 
          sender_id:from_id,
          receiver_id:to_id,
          timestamp:Date.now(),
          type:'text',
          read_status:to_id + '_unread',
          picture:'',
          audio:'',
          time:time,
          reply_status: reply_status,
          reply_id : reply_id,
         
        
      }


        var ref = firebase.database().ref('messages/' + id)
        let dbRef =  ref.push(dataMessage).then((data)=>{
        //success callback
        //console.log("KEY",JSON.stringify(data.key))

       

          this.makeRecent(id,to_id,from_id,'text',currentMessage)
          this.addKey(id,to_id,from_id,data.key,dataMessage)

          this.sendNotification(currentMessage)
      
          


       // ToastAndroid.show('success callback',ToastAndroid.LONG)
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
        ToastAndroid.show(error.message,ToastAndroid.LONG)
    })
  }


  send(){


   if(this.state.message.length > 0){
    let result = this.props.navigation.getParam('result')
    var to_id = result['to_id']
    var to_name = result['to_name']
    var to_image = result['to_image']

    var from_id = this.props.user.user_id



    if(from_id < to_id){
       let str = from_id + '_' + to_id
       this.writeChat(str,to_id,from_id)
    }
    else{
        let str = to_id + '_' + from_id
        this.writeChat(str,to_id,from_id)
    }

   }

  

    // this.setState({
    //   to_id:to_id,
    //   to_name:to_name,
    //   to_image:to_image

    // })
    

  }

  makeRandomString () {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 7; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

 async checkPermission() {
  if (Platform.OS !== 'android') {
      return Promise.resolve(true);
  }

  let result;
  try {
      result = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
         { title:'Microphone Permission',
          message:'Enter the Cannabis needs access to your microphone so you can chat with voice.'
         });
  } catch(error) {
      console.error('failed getting permission, result:', result);
  }
  console.log('permission result:', result);
  return (result === true || result === PermissionsAndroid.RESULTS.GRANTED);
}



async recordSound(){
    // if(this.checkPermission()){
    //    // Stop recording after approximately 3 seconds
    
    //    let name = this.makeRandomString() + '.mp4'
    //    let rec = new Recorder(name).record();
    //   setTimeout(() => {
    //   rec.stop((err) => {
    //     // NOTE: In a real situation, handle possible errors here
    //     ToastAndroid.show(JSON.stringify(err),ToastAndroid.LONG)
       
    //   });
    // }, 3000);
      
    // }

   
    this.recordCount = 0
    setTimeout(() =>{
      this.recordCount =  this.recordCount + 1
    },1000)


    if(await this.checkPermission()){

      let name = this.makeRandomString() + '.aac'

      this._audioRecorder = new Recorder(name, {
        bitrate: 128000, 
        channels: 2,
         sampleRate: 44100}).
         prepare((err, fsPath) => {
              if (err) {
                console.log('error at as23424')
                console.log(err)
              } else {
              
                this.setState({audio_path:fsPath,isAudioRecording:true})
              }
      }).record()
      
     
  
     

    }
   
  }

  async stopRecordSound(){
     if(this._audioRecorder == null){

     }
     else{
      this._audioRecorder.stop

      this._audioRecorder.stop(() => {
     
        // const sound = new Sound(this.state.audio_path, null, (error) => {
        //   if (error) {
        //     // do something
        //     console.log(error)
        //   }
          
        //   // play when loaded
        //   sound.play();
        // });
      if(this.recordCount > 0){
        let result = this.props.navigation.getParam('result')
        var to_id = result['to_id']
        var to_name = result['to_name']
        var to_image = result['to_image']
    
        
        var from_id = this.props.user.user_id
    
        this.setState({isAudioRecording:false})
    
        if(from_id < to_id){
           let str = from_id + '_' + to_id
           this.sendAudio(str,to_id,from_id)
        }
        else{
            let str = to_id + '_' + from_id
            this.sendAudio(str,to_id,from_id)
        }
      }

        this.setState({isAudioRecording:false})
      
           

      
      });
     }
  }

  handler= (item) =>{
    
   }
    playSound = (url,index) => {
    //ToastAndroid.show(url,ToastAndroid.LONG)
    this.setState({audioPLayingIndex:index})
    
    
    var str = url;
    var newStr = str.replace(/_/g, "");
          _sound = new Sound(str, null, (error) => {
              if (error) {
                // do something
                console.log("EEEEEE",JSON.stringify(error))
              }

                // loaded successfully
              //console.log('duration in seconds: ' + _sound.getDuration() + 'number of channels: ' + _sound.getNumberOfChannels());

              // Loop indefinitely until stop() is called
              //_sound.setNumberOfLoops(-1);


               // Get the current playback point in seconds
              // _sound.getCurrentTime((seconds) => console.log('at ' + seconds));
              
              // play when loaded
              _sound.play((success) => {
                if (success) {
                  console.log('successfully finished playing');
                  //this.child.resetIndex(); 
                  //this.child.resetIndex(); 
                  this.setState({audioPLayingIndex:-1})
                } else {
                 // console.log('playback failed due to audio decoding errors');
                }
              });
             
              _isAudioPlaying = true
            });
    
    }
    
     stopSound = () => {
      this.setState({audioPLayingIndex:-1})
      //_sound.stop()
      _sound.pause()
      _sound.release();
      _isAudioPlaying = false

     

     
    }

  checkType(item,index,send_status){
    if(item.type == 'text'){
      return (
        <Message text={item.message} 
        sendStatus={send_status} />
      )
    }
    else if(item.type == 'image'){
        return (
          <ChatImage navigation={this.props.navigation}
           picture={item.picture}/>
        )
    }
    else if(item.type == 'audio'){
      return (
           <ChatAudio  index={index}
            item ={item}
            selectedAudio={this.state.audioPLayingIndex}
            ref={ref => this.child = ref}
            stopSound={this.stopSound}
            playSound={this.playSound}
           
           />
      )
  }
  }

  makeChatOptions(item,index){
    console.log("SELECTED",JSON.stringify(item))
    console.log("SELECTED INDEX",JSON.stringify(index))
    this.props.navigation.setParams({chatOptions:true,chatItem:item}) 
  }

  makeBackgroundColor(item){
    const chatOpt = this.props.navigation.getParam('chatOptions')
    const chat_item = this.props.navigation.getParam('chatItem')
    const send_status = this.props.user.user_id == item.receiver_id ? false : true

    let color = ''
    if(send_status){
        if(chatOpt){
          //means a mesaage was seelceted
          if(chat_item.node_id == item.node_id){
              //color='#90ee90'
              color='red'
          }
          else{
            color='#00cbff'
          }
        }
        else{
          color='#00cbff'
        }

    }
    else{
      if(chatOpt){
        //means a mesaage was selceted
        if(chat_item.node_id == item.node_id){
            //color='#90ee90'
            color='red'

        }
        else{
          color='#c9c5c5' 
        }
      }
      else{
        color='#c9c5c5' 
      }
      
    }

    return color

  }

  replied(item){
    let obj = null
    for (var i = 0 ; i < this.state.messages.length  ; i ++){
      if(this.state.messages[i].node_id == item.reply_id){
        obj =  this.state.messages[i]
        
      }
    }
    // return(
    //   this.state.messages.filter((element) => {
    //     element.node_id == item.reply_id;
    //   })
    // )

   // console.log("OBJECT",JSON.stringify(obj))

    if(obj !== null){
      if(obj.type == 'text'){
        return (
          <Text style={{color:'white'}}>{obj.message}</Text>
        )
      }
      else if(obj.type == 'image'){
        return (
          <Image source={{uri:obj.picture}}
          style={{height:20,width:20}}></Image>

          
        )
      }

      else  if(obj.type == 'audio'){
        return (
          <Image source={require('../assets/play-button.png')} 
          style={{width:20,height:20}} resizeMode='contain'/>
        )
      }
    }

  }

  checkIfReply(item){
    if(item.reply_status){
      return(
        <View style={{backgroundColor:'green',width:'100%',alignSelf:'flex-start',padding:4}}>

        <Text style={{color:'white',fontWeight:'bold',textDecorationLine:'underline'}}>Replied To : </Text>
        <View>
          {
            this.replied(item)
          }
        </View>

        </View>
      )
    }
    
  }

 

  renderItem = ({item,index}) => {
 

    const width = Dimensions.get("window").width
    const send_status = this.props.user.user_id == item.receiver_id ? true : false
    const chatOpt = this.props.navigation.getParam('chatOptions')
    const chat_item = this.props.navigation.getParam('chatItem')
    return (
      <TouchableOpacity
      onLongPress={()=>{
      this.makeChatOptions(item,index)
      
      }}
       onPress={()=>{}}>
      
    
        <View style={{width : width * 0.7,
        alignSelf : send_status ? 'flex-start' : 'flex-end',margin:5}}>

      

              <View style={{alignItems:'center', 
              alignSelf : send_status ? 'flex-start' : 'flex-end',
              backgroundColor:this.makeBackgroundColor(item),
             borderRadius:6}}>
                <View style={{alignSelf:'stretch'}}>
                      {
                     
                        this.checkIfReply(item)
                      }
                      </View>


                    <View style={{ paddingBottom:5,
                  paddingTop:5,paddingRight : 7,paddingLeft:7,}}>

                          {
                            this.checkType(item,index,send_status)
                          }

                  </View>

            

             </View>
             <Text style={{marginLeft:5,fontSize:10,fontWeight:'500',
              alignSelf : send_status ? 'flex-start' : 'flex-end',marginRight:5}}> {item.time}</Text>

       </View>


      </TouchableOpacity>

    )
  }



  
   onContentOffsetChanged = (distanceFromTop: number) => {
    distanceFromTop === 0 && console.log("TOP")
  
  }

  handleScroll = (event: Object) => {
    console.log("Scroll INdexxx..",event.nativeEvent.contentOffset.y);
   }




   checkTypeReply(){
     let item = this.props.navigation.getParam('chatItem')

     if(item == null){
       this.setState({reply_messageId:-1,
        reply_status:false})

     }
     else{
      if(item.type == 'text'){
        return(
          <Text>{item.message}</Text>
        )
      }
      else if(item.type == 'image'){
       return(
         <Image source={{uri:item.picture}}
   
         style={{height:50,width:50,padding:0}}></Image>
       )
     } 
     else if(item.type == 'audio'){
       return(
         <View style={{flexDirection:'row',alignItems:'center'}}>
        
         <Image source={require('../assets/play-button.png')} style={{width:20,height:20}} resizeMode='contain'/>
      
 
         <Image source={require('../assets/wave.png')} style={{width:60,height:50,marginLeft:5}} />
 
        
 
         </View>
       )
     } 
     }
    

    

   }



    render()
    {

        return(
          <SafeAreaView style={styles.container}>


    
            <View contentContainerStyle={{justifyContent:'center',
            alignItems:'center'}}
            style={{backgroundColor:'#EBFAFF'
            ,width:'100%',flex:1}}>



      
                    <FlatList
                           style={{flex:1,marginBottom:60}}
                           data={this.state.messages}
                           
                            // initialScrollIndex={this.state.messages.length - 1}
                            inverted={-1}
                            numColumns={1}
                            disableVirtualization={false}
                            scrollEventThrottle={16}
                            onScroll={
                                (event: NativeSyntheticEvent<NativeScrollEvent>) =>
                                    this.onContentOffsetChanged(event.nativeEvent.contentOffset.y),this.handleScroll
                            }
                            ref={ (ref) => { this.myFlatListRef = ref } }
                            //onContentSizeChange={ () => { this.myFlatListRef.scrollToEnd({animated:false}) } }

                           // onLayout={ () => { this.myFlatListRef.scrollToEnd({animated:false}) } }
                            onEndReached={()=> console.log("Bottom")}
                            onEndReachedThreshold={0.2}
                            showsVerticalScrollIndicator={false}
                            renderItem={(item,index) => this.renderItem(item,index)}
                             

                         />



    
            </View>
            {
              this.state.reply_status 
              ?
              <View style={{position:'absolute',bottom:60,
              left:0,right:0,padding:7,backgroundColor:'#eee',
            padding:5,borderTopLeftRadius:20,borderTopRightRadius:20,
             borderWidth: 0.6,
             borderColor: 'grey' }}>

             <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
             
                    <Text style={{color:colors.color_primary,fontWeight:'bold',
                    textDecorationLine: 'underline',}}>Reply to :</Text>

                    <TouchableOpacity onPress={()=> {
                      
                        this.resetChatOptions(),
                        this.replyFeature()
                      }}>
                    
                      <Image source={require('../assets/error.png')}
                                  style={{alignSelf:'flex-end',height:20,width:20}} />
                    </TouchableOpacity>


             </View>


             {
              this.checkTypeReply()
             }

              </View>
              : null
            }





            <View style={{position:'absolute',
            bottom:0,
            left:0,right:0,height:60,
            backgroundColor:this.state.reply_status 
              ? '#eee' : '#EBFAFF',paddingLeft:10,paddingRight:10,paddingTop:4}}>



            <View style={{flexDirection:'row',
            padding:5,alignItems:'center',borderRadius: 30,
           borderWidth: this.state.isAudioRecording 
                    ?  3 : 0.6,
             borderColor: this.state.isAudioRecording 
                    ? 'green' : 'grey',backgroundColor:'white'
              }}>

                {
                  this.state.isAudioRecording 
                    ? <View style={{flex:1}}>

                         <Image source={require('../assets/voice-green.png')}
                          style={{alignSelf:'center',height:40,width:40}} />
                  
                     </View>
                    :
                    <View style={{flex:1}}>
                   <TouchableOpacity onPress={()=> {this.addPhoto()}}>
                   <Image source={require('../assets/cam.png')} style={{alignSelf:'center',height:35,width:35}} />
                   </TouchableOpacity>
                   </View>
                }

                 
                  <View style={{flex:5}}>



                  <TextInput
                      value={this.state.message}
                      style={styles.input}
                      ref={(component) => this._textInput = component}
                      blurOnSubmit={false}
                      onEndEditing={()=> this.setState({isMyselfTyping:false},()=>  {
                        this.changeSelfTypingStatus(false)
                      })}
                      onSubmitEditing={()=> this.setState({isMyselfTyping:false},()=> {
                        this.changeSelfTypingStatus(false)
                      })}
                      // onSubmitEditing={()=> this.send()}
                      editable={this.state.isAudioRecording ? false :true}
                      placeholder={this.state.isAudioRecording ? 'Recording......' :'Write Your Message Here...'}
                      placeholderTextColor={this.state.isAudioRecording ? 'green' : colors.color_primary}
                      editable={true}
                      onChangeText={ (message) => {
                        var a = message.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '')
                        this.setState({message:a,isMyselfTyping:true},()=> {
                          this.changeSelfTypingStatus(true)
                        })
                      } }


                 />
                  </View>

                 

                  {
                    this.state.isAudioRecording 
                    ? null
                    :
                    <View style={{flex:1}}>
                   <TouchableOpacity onPress={()=> {this.send()}}>
                   <Image source={require('../assets/send.png')} style={{alignSelf:'center',height:30,width:30}} />
                   </TouchableOpacity>
                   </View>

                  }

                
                
                    <View style={{flex:1}}>
                   <TouchableOpacity 
                  //  onLongPress={()=>{
                  //   this.recordSound()
                  //  }}

                      onPressIn={()=>{
                        this.recordSound()
                      }}
                      onPressOut={()=>{
                        this.stopRecordSound()
                      }}
                   >
                   <Image source={require('../assets/voice.png')} style={{alignSelf:'center',height:30,width:30}} />
                   </TouchableOpacity>
               </View>



             

               

               
             
                  


                  </View>
                  </View>

               
                  {this.state.chatError &&
          <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(192,192,192, 0.5)', justifyContent: 'center',
            alignItems:'center' }
          ]}>
         <Text style={{color:'black',fontWeight:'bold'}}>{this.state.chatErrorMessage}</Text>
          </View>
        }
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Chats);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },
  input: {
    width: "100%",
    height: 40,
    padding:5,
   
  
  },
  });


  /** 
   * In this chat some good concepts has been used to achieve scroll to bottom features
   * Firstly inverted is used inside flatlist and since we have to reverse the list as jo data 
   * firebase se aa rha wo time based sorted hai  to hum usko reverse kiye or set kiye sttate
   * limit to last child_added me taaki
   * aur haa array .splice ya unshift ye dono method array ke start me element push krte hai
   * 
   */