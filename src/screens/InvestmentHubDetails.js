import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



export default class InvestmentHubDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state={
        loading_status:false,
       item:null
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
            
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Pitch Title </Text>
     </View>
    ),
  });

  
 





  componentWillMount(){
    let obj = this.props.navigation.getParam('result', null)
    this.setState({
      'item' : obj
    })
  
  }




  



   
 

    render()
    {

      const { item } = this.state.item;
      
        return(
          <SafeAreaView style={styles.container}>

    

          
    
    
            <ScrollView style={{width:'100%',flex:1,padding:15}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center'}}>

            <Text style={{color:'black',fontWeight:'bold',margin:2}}>Pitch Title</Text>


            <View style={styles.parent}>
                <Text style={styles.title}>Country : </Text>
                <Text style={styles.info}>{item.country.name}</Text>
              </View>

              <View style={styles.parent}>
                <Text style={styles.title}>City : </Text>
                <Text style={styles.info}>{item.city.name}</Text>
              </View> 

              <View style={styles.parent}>
                <Text style={styles.title}>Contact Number : </Text>
                <Text>{item.contact_no}</Text>
              </View>

              <View style={styles.parent}>
                <Text style={styles.title}>Contact Email : </Text>
                <Text style={styles.info}>{item.contact_email}</Text>
              </View>

            
                <Text style={{color:'black',
                  fontSize:13,marginTop:7,marginBottom:7}}>
                <Text 
                style={{fontWeight:'bold',color:'grey',marginTop:1}}>Synopsis Of Idea : </Text>
                {item.synopsis_of_idea}
                </Text>
                
         

             
                <Text style={{color:'black',
                  fontSize:13,marginTop:7,marginBottom:7}}>
                <Text style={{fontWeight:'bold',color:'grey',marginTop:1}}>More Details Of Idea :  </Text>
                {item.more_detail_of_idea}
                </Text>
 




              <View style={styles.parent}>
                <Text style={styles.title}>How Much Capital Required: </Text>
                <Text style={styles.info}>{item.capital_required}</Text>
              </View> 

              <View style={styles.parent}>
                <Text style={styles.title}>Minimum Per Investor : </Text>
                <Text style={styles.info}>{item.minimum_per_investor}</Text>
              </View>

              <View style={styles.parent}>
                <Text style={styles.title}>Open to Fill Assesment : </Text>
                <Text style={styles.info}>{item.open_to_full_investment}</Text>
              </View>
              


         
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

	




const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },
  
  title:{
    color:'grey',
  fontWeight:'bold',
  marginTop:7,
  marginBottom:7,
  marginLeft:1
},
  info:{
    color:'black',
  fontSize:13,
  marginTop:1,
  marginBottom:1
},
parent:{
  flexDirection:'row',
  alignItems:'center'
}
  });