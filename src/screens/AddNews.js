import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import Snackbar from 'react-native-snackbar';
//import {  StackActions, NavigationActions} from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {  StackActions, NavigationActions} from 'react-navigation';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';




class AddNews extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      title:'',
      description:'',
      imagePhoto:null,
      imageSource:null
   
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
       
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Add News</Text>
     </View>
    ),
  });

  isValid(){
    const { title, description , imageSource } = this.state;

      let valid = false;

        if (title.toString().trim().length > 0 && 
        description.toString().trim().length > 0 && 
        imageSource != null 
        ) {
              valid = true;
             
          }

          if(title.toString().trim().length == 0){
      
            // Platform.OS === 'android' 
            //           ?   ToastAndroid.show('Enter Title', ToastAndroid.SHORT)
            //           : Alert.alert("Enter Title!")

            Snackbar.show({
              title: "Enter Title",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });
            return false;
          }

          else  if(description.toString().trim().length == 0){
      
            // Platform.OS === 'android' 
            //           ?   ToastAndroid.show('Enter description', ToastAndroid.SHORT)
            //           : Alert.alert("Enter description!")

                      
            Snackbar.show({
              title: "Enter description",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


            return false;
          }

          else  if(imageSource == null ){

            // Platform.OS === 'android' 
            // ?   ToastAndroid.show('Enter news Image', ToastAndroid.SHORT)
            // : Alert.alert("Enter news Image")

            Snackbar.show({
              title: "Enter news Image",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


           return false;
          }

        
        return valid

  }



  addNews(){
    if(this.isValid()){


      console.log("Making posts")
 
     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
         var formData = new FormData();
 
         formData.append('user_id',item);
         formData.append('title',this.state.title);
         formData.append('description',this.state.description);
         formData.append('img',this.state.imagePhoto);
         
 
                       this.setState({loading_status:true})
                       let url = urls.base_url +'api/create_news'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
 
                     }).then((response) => response.json())
                           .then((responseJson) => {
                   this.setState({loading_status:false})
                  
             
              
              // Platform.OS === 'android' 
              // ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
              // : Alert.alert(responseJson.message)

              Snackbar.show({
                title: responseJson.message,
                duration: Snackbar.LENGTH_SHORT,
                backgroundColor:'black',
                color:'red',
                action: {
                  title: 'OK',
                  color: 'green',
                  onPress: () => { /* Do something. */ },
                },
              });

              
 
              
                         if(!responseJson.error){

                          this.props.navigation.navigate('News');
                          const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'News' })],
                          });
                          this.props.navigation.dispatch(resetAction);
                              
                              
                             }
                             else{
 
                              
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                             
                      //        Platform.OS === 'android' 
                      //  ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      //  : Alert.alert("Try Again !")

                      Snackbar.show({
                        title: "Try Again !",
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });
 
 
                           });
       }
       else{
         ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });
    }
  }

  addPhoto() {
    const options = {
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
 
    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};

        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };
       

        this.setState({
          imageSource: source ,
          imagePhoto:photo,
 
        }); 
        
       

       
      }
    });
 }

 addPhotoCamera() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchCamera(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     

      this.setState({
        imageSource: source ,
        imagePhoto:photo,

      }); 
      
     

     
    }
  });
}
addPhotoGallery() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchImageLibrary(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     

      this.setState({
        imageSource: source ,
        imagePhoto:photo,

      }); 
      
     

     
    }
  });
}


  



  componentWillMount(){
   
  
  }


    render(){

  
      const ImageSheet= () =>{
     
        return (
          <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={200}
          duration={0}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              borderTopLeftRadius:15,
              borderTopRightRadius:15
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType='fade'
          closeOnDragDown={true}
          minClosingHeight={10}

>
            <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>

                <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                      <Image style={{ height: 40, width: 40}}
                      resizeMode='contain'
                      source={require('../assets/logo.png')}>
                      </Image>

                      <Text style={{fontWeight:'bold',fontSize:16}}>Choose Image Source ! </Text>
                
                </View>


                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
              flex:2}}>

                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.addPhotoCamera()}
                  style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:'white'}}>Camera</Text>
                  </TouchableOpacity>
                  </View>


                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.addPhotoGallery()}
                  style={{width:'100%',backgroundColor:'white',padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:colors.color_primary}}>Gallery</Text>
                  </TouchableOpacity>
                  </View>
              
              </View>
            </View>
</RBSheet>

        )
      }
    
     
        return(
          <ImageBackground
          source={require('../assets/news_bg.jpg')}
          resizeMode="cover"
          style={styles.imagebackground}>

          <SafeAreaView style={styles.container}>
          <ImageSheet></ImageSheet>

          
     

      

        <ScrollView style={{width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

       <View style={styles.outsideview}>


             <TextField
              label='Title'
              value={this.state.title}
              tintColor={colors.color_primary}
              baseColor='white'
              textColor='white'
              onChangeText={(title) => this.setState({ title})}
              ref={(input) => { this.title = input; }}
              onSubmitEditing = {() => this.description.focus()}
            />

         <TextField
              label='Description'
              value={this.state.description}
               multiline={true}
              tintColor={colors.color_primary}
              baseColor='white'
              textColor='white'
              onChangeText={(description) => this.setState({ description})}
              ref={(input) => { this.description = input; }}
              onSubmitEditing = {() => this.description.focus()}
            />

            </View>

            {
              this.state.imagePhoto == null
              ? null
              :
              <View style={{width:'60%',height:Dimensions.get('window').height * 0.2,
              backgroundColor:'white'}}>

                <Image style={{width:'100%',height:'100%'}}
                resizeMode='stretch'
                source={this.state.imageSource}/>

              </View>
            }

            <View style={{flex:2,flexDirection:'row',alignItems:'center'}}>

                  <TouchableOpacity style={{flex:1}} onPress={() => {this.RBSheet.open()}}>
                  <View style={{
                    width: this.state.imagePhoto == null ? '90%' : '70%', height: 50, backgroundColor: 'white', justifyContent: 'center',
                    alignItems: 'center', alignSelf: 'center', marginTop: 50, borderRadius: 5,
                    shadowColor: 'gray',
                    shadowRadius: 10,
                    shadowOpacity: 1,
                    elevation:10

                  }}>
                    <Text style={{ color: 'blue', fontWeight: 'bold' }}>Add Photo</Text>
                  </View>
                </TouchableOpacity>
                    {
                      this.state.imagePhoto == null
                      ? null
                      :
                      
                    <TouchableOpacity style={{flex:1}} onPress={() => {
                      this.setState({imagePhoto:null,imageSource:null})
                    }}>
                      <View style={{
                        width: '70%', height: 50, backgroundColor: 'white', justifyContent: 'center',
                        alignItems: 'center', alignSelf: 'center', marginTop: 50, borderRadius: 5,
                        shadowColor: 'gray',
                        shadowRadius: 10,
                        shadowOpacity: 1,
                        elevation:10

                      }}>
                        <Text style={{ color: 'blue', fontWeight: 'bold' }}>Remove Photo</Text>
                      </View>
                    </TouchableOpacity>
                    }
              

            </View>


            <TouchableOpacity style={{width:'100%'}} onPress={() => {this.addNews()}}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 30, borderRadius: 5,
              shadowColor: 'gray',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Post News</Text>
            </View>
          </TouchableOpacity>


        </ScrollView>

    

          
              
    
            {this.state.loading_status &&
              <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
          ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>
          </ImageBackground>
         

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddNews);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    imagebackground:{
    position: 'absolute',
    flex: 1,
    backgroundColor:'rgba(0,0,0,0.45)',
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,zIndex:-2
  },

  outsideview:{ 
    width: '90%', 
    justifyContent: 'center',
     alignSelf: 'center' ,
     marginBottom:30},

   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });