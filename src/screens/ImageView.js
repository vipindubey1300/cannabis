import React from 'react';
import { colors, urls } from '../Globals';
import Snackbar from 'react-native-snackbar';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';

//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'

import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



class ImageView extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      image_url:''
    
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'black',
    headerStyle: {
      backgroundColor: 'black',
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
            
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{ navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

          
     </View>
    ),
  });










  componentWillMount(){

    let result = this.props.navigation.getParam('result')
    var url = result['image_url']
    this.setState({image_url:url})
  
  
  }


   
 

    render()
    {


  
      
        return(
          <SafeAreaView style={styles.container}>

    

          <View style={{backgroundColor:'black',width:'100%',
          flex:1,justifyContent:'center',alignItems:'center'}}
          >

            <Image source={{uri:this.state.image_url}}
            resizeMode='contain'
            style={{width:'100%',height:Dimensions.get('window').height * 0.7}}/>
                



            </View>

           


            
    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <PacmanIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ImageView);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });