import React from 'react';
import { colors,urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import Snackbar from 'react-native-snackbar';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ToastAndroid,
  TouchableWithoutFeedback
} from 'react-native';



import MaterialTextInput from '../components/MaterialTextInput';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import {getFCMToken} from '../config/DeviceTokenUtils';
import { error } from 'react-native-gifted-chat/lib/utils';


 class Login extends React.Component {


  constructor(props) {
    super(props);
    this.state={
      email: '',
      password: '',
      loading_status:false,
      deviceToken:'',
      password_visible:false
    };
  }



  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: {
      color : 'black',
      flex: 1,
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 17,


  },
  headerLayoutPreset: 'center',
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: 'white',

          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 5,
  elevation:10,//for android
    },
 // headerTitle: () => <Text>Login</Text>,
title:"Login",

    headerLeft: () => (
      <TouchableOpacity onPress={()=>{

       navigation.goBack()
      }}>
        <Image source={require('../assets/left-arrow.png')} style={{width:20,height:20,marginLeft:15}} resizeMode='contain'/>

        </TouchableOpacity>
    ),
    headerRight: () => (
      <View></View>
    ),
  });


  handleEmailchange = (text) => {
    		this.emailInput.setState({text});
        this.setState({email:text});
      }


      handlePasswordchange = (text) => {
    		this.passwordInput.setState({text});
        this.setState({password:text});
    	}


  
componentWillMount(){
  getFCMToken().then(response => {  
    this.setState({
      deviceToken : response
    },()=>{
      console.log("Login FCM TOKEN----------",this.state.deviceToken)
    })
   
});
}

  componentDidMount() {

    StatusBar.setBackgroundColor(colors.status_bar_color)
    StatusBar.setBarStyle('light-content', true);

  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  isValid(){
    const { email, password } = this.state;


let valid = false;

    if (email.length > 0 && this.validateEmail(email.trim()) && password.toString().trim().length > 0) {
      valid = true;
    }

else if (email.trim().length === 0) {
  // Platform.OS === 'android'
  // ?  ToastAndroid.show('Enter email', ToastAndroid.SHORT)
  // : Alert.alert('Enter email')
  Snackbar.show({
    title: 'Enter Email',
    duration: Snackbar.LENGTH_SHORT,
    backgroundColor:'black',
    color:'red',
    action: {
      title: 'OK',
      color: 'green',
      onPress: () => { /* Do something. */ },
    },
  });
  return false;
}
else if(!this.validateEmail(email.trim())){
  // Platform.OS === 'android'
  // ?  ToastAndroid.show('Enter valid email', ToastAndroid.SHORT)
  // : Alert.alert('Enter valid email')

  Snackbar.show({
    title: 'Enter valid email',
    duration: Snackbar.LENGTH_SHORT,
    backgroundColor:'black',
    color:'red',
    action: {
      title: 'OK',
      color: 'green',
      onPress: () => { /* Do something. */ },
    },
  });


  return false;
}
else if(password.toString().trim().length == 0){
  // Platform.OS === 'android'
  // ?  ToastAndroid.show('Enter password', ToastAndroid.SHORT)
  // : Alert.alert('Enter password')

  Snackbar.show({
    title: 'Enter password ',
    duration: Snackbar.LENGTH_SHORT,
    backgroundColor:'black',
    color:'red',
    action: {
      title: 'OK',
      color: 'green',
      onPress: () => { /* Do something. */ },
    },
  });


  return false;
}

  return valid

  }



  login() {
   //  console.log("SDFSDFSDFDSFSDF",this.nameInput.getInputValue() )
    if(this.isValid()){
      this.setState({loading_status:true})
      var formData = new FormData();
       // var token = this.checkPermission()
       formData.append('email', this.state.email);
       formData.append('password', this.state.password);
       formData.append('device_token', this.state.deviceToken);
       Platform.OS =='android'
       ?  formData.append('device_type',1)
       : formData.append('device_type',2)


      // console.log("FFF",JSON.stringify(formData))


               let url = urls.base_url +'api/login'
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
             }).then((response) => response.json())
                   .then((responseJson) => {
                       this.setState({loading_status:false})

                      // console.log("FFF",JSON.stringify(responseJson))


                    if (!responseJson.error)

                   {
                    var  user_id = responseJson.result.id
                    var name = responseJson.result.name
                    var email = responseJson.result.email
                    var image = responseJson.result.user_image
                    var investor = responseJson.result.investor_flag
                    var entrepreneur = responseJson.result.entrepreneur_flag


                    AsyncStorage.multiSet([
                      ["user_id", user_id.toString()],
                      ["email", email.toString()],
                      ["name", name.toString()],
                       ["image", image.toString()],
                       ["investor", investor.toString()],
                       ["entrepreneur", entrepreneur.toString()],

                     ]);


                       this.props.add({ user_id, name,image,email,investor,entrepreneur });


                        this.props.navigation.navigate('HomePage');

                       }else{

                        Snackbar.show({
                          title: responseJson.message,
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });

                          //  Platform.OS === 'android'
                          //  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          //  : Alert.alert(responseJson.message)
                         }
                   }).catch((error) => {
                             this.setState({loading_status:false})
                            //  Platform.OS === 'android'
                            //  ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                            //  : Alert.alert("Try Again !")

                            Snackbar.show({
                              title: error.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',

                            });

                   });
    }
  }

  backBtn() {
    this.props.navigation.pop()
  }

  forgot() {
    this.setState({
      email:'',
      password:''
    })
    this.props.navigation.navigate('Forgot')
  }

  register() {
    this.props.navigation.navigate('Register')
  }

  render() {
    let { email } = this.state;
    let { password } = this.state;
    const { visible } = this.state;

    return (
      <SafeAreaView style={styles.container}>








        <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
        showsVerticalScrollIndicator={false}
       contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>


          <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                  <Image style={{ height: 160, width: 160, marginTop: 30 }}
                    source={require('../assets/logo.png')}
                  ></Image>
          </View>



          <Text style={{ marginTop: 20, fontWeight: 'bold', fontSize: 18 ,alignSelf:'flex-start',marginLeft:20}}>
            Welcome Back!
         </Text>

          <Text style={{ marginTop: 5, fontSize: 13,alignSelf:'flex-start',marginLeft:20 }}>
            Login to your existing Cannabis Expressions Account
          </Text>

          <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' ,marginBottom:30}}>
          <TextField
              label='Email Address'
              value={email}
              keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
              onChangeText={(email) => this.setState({ email })}
              autoCapitalize='none'
              ref={(input) => { this.email = input; }}
              onSubmitEditing = {() => this.password.focus()}
            />


<View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
						<View style={{ width: '100%' }}>
            
            <TextField
              label='Password'
              value={password}
              maxLength={16}
              secureTextEntry={!this.state.password_visible}  
                            onChangeText={(password) => this.setState({ password })}
              ref={(input) => { this.password = input; }}
              onSubmitEditing = {() => this.login()}
            />
						</View>
						<View style={{ marginLeft: -23 }}>
							<TouchableWithoutFeedback onPress={() => this.setState({ password_visible: !this.state.password_visible })}>
								{
									this.state.password_visible
										?
										<Image source={require('../assets/eye.png')} style={{ width: 30, height: 30, marginLeft: 0 }} resizeMode='contain' />
										:
										<Image source={require('../assets/eye-cross.png')} style={{ width: 30, height: 30, marginLeft: 0 }} resizeMode='contain' />
								}

							</TouchableWithoutFeedback>
						</View>
					</View>

          



{/*
          <MaterialTextInput
             placeholder="Email"
           //  ref={(input) => { this.emailInput = input; }}
             ref={ref => (this.emailInput = ref)}
             onSubmit = {() => this.passwordInput.focus()}
             onChangeText={(text) => this.handleEmailchange(text)}/>


          <MaterialTextInput
             placeholder="Password"
             ref={ref => this.passwordInput = ref}
             onSubmit = {() => this.submit()}
             onChangeText={(text) => this.handlePasswordchange(text)}/>


              */}

          </View>



          <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' }}>
            <Text style={{ textAlign: 'center',color:'grey' }}>
              By clicking 'Login' you agree to the
              </Text>
         </View>

         <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
         <Text style={{ color: colors.color_primary, fontWeight: "bold" }}>Terms of Use</Text>
         <Text style={{ color: 'grey', marginLeft:5,marginRight:5}}>and</Text>
         <Text style={{ color: colors.color_primary, fontWeight: "bold" }}>Privacy Policy</Text>
         </View>





          <TouchableOpacity style={{width:'100%'}} onPress={() => this.login()}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 50, borderRadius: 5,
              shadowColor: 'grey',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Login</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.forgot()}>
            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 ,marginBottom:20}}>
              <Text style={{ color: 'blue', fontWeight: 'bold' }}>Forgot Password ?</Text>

            </View>
          </TouchableOpacity>

          <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginBottom:20}}>

          <Text style={{ color: 'black', marginLeft:5,marginRight:5}}> Don't have an account?</Text>
          <Text style={{ color: colors.color_primary, fontWeight: "bold" }}
          onPress={()=> this.props.navigation.navigate("Register")}> Register</Text>
          </View>



        </ScrollView>


        {this.state.loading_status &&
          <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
          ]}>
          <WaveIndicator color={colors.color_primary}/>
          </View>
        }
      </SafeAreaView>


    )
  }
}

//array of styles in indicator...means all items will be there


const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    add: (userinfo) => dispatch(addUser(userinfo)),
   // enterpreneur: () => dispatch(addUser()),


  }
}


export default connect(null, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',

  },
  buttonLargeContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40
  },
  primaryButton: {
    backgroundColor: '#FF0017',
    height: 45
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  lottie: {
    width: 100,
    height: 100
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }



});
