
import React from 'react';
import { colors,urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import Snackbar from 'react-native-snackbar';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  ActivityIndicator
} from 'react-native';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';

export default class ResetPassword extends React.Component {
  state = {
    email: '',
    password: '',
    cpassword: ''
  };

  constructor(props) {
    super(props);
  }



  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { 
      color : 'black', 
      flex: 1,
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 17,
    
      
  },
  headerLayoutPreset: 'center',
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: 'white',
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 5,
  elevation:10,//for android
    },
 // headerTitle: () => <Text>Login</Text>,
title:"Reset Password",
  
    headerLeft: () => (
      <TouchableOpacity onPress={()=>{
       
       navigation.goBack()
      }}>
        <Image source={require('../assets/left-arrow.png')} style={{width:20,height:20,marginLeft:15}} resizeMode='contain'/>
        
        </TouchableOpacity>
    ),
    headerRight: () => (
      <View></View>
    ),
  });

  componentWillMount(){

    var result  = this.props.navigation.getParam('result')
    var email = result["email"]
    this.setState({email:email})
   } 
  
  
   isValid() {
       
  
    let valid = false;
  
    if ( this.state.password.trim().length > 0 && 
    this.state.cpassword.trim().length > 0) {
      valid = true;
    }
  
   if(this.state.password.trim().length === 0){
  
    
  
  
      // Platform.OS === 'android' 
      // ?   ToastAndroid.show("Enter new password", ToastAndroid.SHORT)
      // : Alert.alert("Enter new password")

      Snackbar.show({
        title: "Enter new password",
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
      });
  
  
      return false;
    }
    else if (this.state.password.trim().length < 8 || this.state.password.trim().length > 16) {
     
    
  
      // Platform.OS === 'android' 
      // ?   ToastAndroid.show("New Password should be 8-16 characters long", ToastAndroid.SHORT)
      // : Alert.alert(" New Password should be 8-16 characters long")

      Snackbar.show({
        title: 'New Password should be 8-16 characters long',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
      });
  
  
      return false;
    }
  
    else if(this.state.cpassword.trim().length === 0){
  
     // ToastAndroid.show('Enter confirm password', ToastAndroid.SHORT);
  
  
      // Platform.OS === 'android' 
      // ?   ToastAndroid.show("Enter confirm password", ToastAndroid.SHORT)
      // : Alert.alert("Enter confirm password")

      Snackbar.show({
        title: "Enter confirm password",
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
      });
  
  
      return false;
    }
    else if (this.state.password.trim().toString() != this.state.cpassword.trim().toString()) {
     
      // Platform.OS === 'android' 
      // ?   ToastAndroid.show("Password and confirm password should match", ToastAndroid.SHORT)
      // : Alert.alert("Password and confirm password should match")


      Snackbar.show({
        title: 'Password and confirm password should match',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
      });
  
  
      return false;
    }
   
  
    return valid;
  }
  
  
  onReset(){
  
         
          if(this.isValid()){
  
  
                var formData = new FormData();
  
  
                formData.append('email',this.state.email);
                formData.append('new_password', this.state.password)
                formData.append('confirm_password', this.state.cpassword);
  
                              this.setState({loading_status:true})
                              let url = urls.base_url +'api/reset_password'
                              fetch(url, {
                              method: 'POST',
                              headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'multipart/form-data',
                              },
                          body: formData
  
                            }).then((response) => response.json())
                                  .then((responseJson) => {
                          this.setState({loading_status:false})
                       
                                      if(!responseJson.error){
  
                                        this.props.navigation.navigate("Login")
                                      
                                        Snackbar.show({
                                          title: responseJson.message,
                                          duration: Snackbar.LENGTH_SHORT,
                                          backgroundColor:'black',
                                          color:'red',
                                          action: {
                                            title: 'OK',
                                            color: 'green',
                                            onPress: () => { /* Do something. */ },
                                          },
                                        });
                                   
                                    
                                       
                                     
                                    }
                                    else{
  
                                     
                                      Snackbar.show({
                                        title: responseJson.message,
                                        duration: Snackbar.LENGTH_SHORT,
                                        backgroundColor:'black',
                                        color:'red',
                                        action: {
                                          title: 'OK',
                                          color: 'green',
                                          onPress: () => { /* Do something. */ },
                                        },
                                      });
                                    }
                                  }).catch((error) => {
                                    this.setState({loading_status:false})
                                    
                                      // Platform.OS === 'android' 
                                      // ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                      // : Alert.alert("Connection Error !")

                                      Snackbar.show({
                                        title: "Connection Error !",
                                        duration: Snackbar.LENGTH_SHORT,
                                        backgroundColor:'black',
                                        color:'red',
                                        action: {
                                          title: 'OK',
                                          color: 'green',
                                          onPress: () => { /* Do something. */ },
                                        },
                                     
                                      });
                                  });
            
  
            
  
  
          }
  
  }
  
  
  

  render() {
    
    return (
      <SafeAreaView style={styles.container}>



        <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
        showsVerticalScrollIndicator={false}
       contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

         
          <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                  <Image style={{ height: 160, width: 160, marginTop: 30 }}
                    source={require('../assets/logo.png')}
                    resizeMode='contain'
                  ></Image>
          </View>



      
          <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' ,marginBottom:20}}>
            <TextField
              label='Email Address'
              editable={false}
              value={this.state.email}
              onChangeText={(email) => this.setState({ email })}
            />

            <TextField
            label='New Password'
            value={this.state.password}
            secureTextEntry={true}
            onChangeText={(password) => this.setState({ password })}
          />

          <TextField
            label='Confirm Password'
            value={this.state.cpassword}
            secureTextEntry={true}
            onChangeText={(cpassword) => this.setState({ cpassword })}
          />

          
          </View>


             
             


       {/*    <TouchableOpacity style={{width:'100%'}} onPress={() => this.onReset()}>*/}
       <TouchableOpacity style={{width:'100%'}} onPress={() => this.onReset()}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 20, borderRadius: 5,
              shadowColor: 'gray',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Submit</Text>
            </View>
          </TouchableOpacity>

         

        </ScrollView>


        {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
          <WaveIndicator color={colors.color_primary}/>
          </View>
        }
      </SafeAreaView>


    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  buttonLargeContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40
  },
  primaryButton: {
    backgroundColor: '#FF0017',
    height: 45
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }

});