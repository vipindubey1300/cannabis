import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';



class Search extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      peoples:[],
      search_text:''
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
         
         
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                  <TouchableOpacity onPress={()=>{ navigation.navigate("Home") }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Search</Text>
            </View>
    ),
  });



  onSearch(){
    
    var formData = new FormData();
  
    formData.append('keyword',this.state.search_text);
   
                  this.setState({loading_status:true})
                  let url = urls.base_url +'api/search_user'
                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData

                }).then((response) => response.json())
                      .then((responseJson) => {
                 this.setState({loading_status:false})
       

                            console.log(responseJson)

                            // Snackbar.show({
                            //   title: responseJson.message,
                            //   duration: Snackbar.LENGTH_SHORT,
                            //   backgroundColor:'black',
                            //   color:'red',
                            //   action: {
                            //     title: 'OK',
                            //     color: 'green',
                            //     onPress: () => { /* Do something. */ },
                            //   },
                            // });


                          if(!responseJson.error){

                          this.setState({peoples: responseJson.data})

                 

                           
                          }
                        else{

                         
                            
                        }
                      }).catch((error) => {
                        this.setState({loading_status:false})
                        Snackbar.show({
                          title: 'Try Again !',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });

                      });
  }





  componentWillMount(){
  
  
  }

  next(user_id){
    //this.props.navigation.navigate("SellerProfile")

    if(user_id == this.props.user.user_id){
      this.props.navigation.navigate("MyProfile")
    }
    else{
      let obj={
        'user_id':user_id
      }
        this.props.navigation.navigate("SellerProfile",{result:obj})
    }
    
   
 }

renderItem = ({item, index}) => {
    return(
      <TouchableOpacity onPress={()=> this.next(item.id)}>
      <View style={{width:Dimensions.get('window').width * 1,padding:6,margin:7,flexDirection:'row',alignItems:'center'}}>

        <Image source={{uri:urls.base_url + item.user_image}} 
                    style={{width:30,height:30,borderRadius:15,
                    overflow:'hidden',marginRight:15}} 
                    resizeMode='cover'/>
                    
                  {
                       item.seller_flag == 1
                       ?
                       <View style={{position:'absolute',bottom:25,left:20}}>
                         <Image source={require('../assets/tag.png')} 
                                  style={{width:20,height:20}} resizeMode='contain'/>

                   </View>

                          :null
                     }

          <Text>{item.name}</Text>


       </View>

       </TouchableOpacity>



    )
   }


  



   FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#aaa9ad",
          marginTop:3,
          marginBottom:3
        }}
      />
    );
  }

   s(){
    new Promise((resolve, reject) => {
			this.props.change({ user_id : 4, name : 'dssdsdfdsf', image:'image', email:'email' })
		})
		.then( success => {
			Alert.alert("Info", "Successfully Saved User Profile Information!");
		//	this.props.navigation.navigate('MainStackNavigation')
		})
		.catch( error => {
			console.log(error)
			Alert.alert("Error", 'Failed To Save Profile Information, Please try Again!')
		})
	}
    
   
 

    render(){
        return(
          <SafeAreaView style={styles.container}>

    
    
            <KeyboardAwareScrollView style={{backgroundColor:'#EBFAFF',   }}
            showsVerticalScrollIndicator={false}
           //contentContainerStyle={{justifyContent:'center',alignItems:'center'}}
           >

           <Text style={{margin:15,alignSelf:"center",fontSize:20,fontWeight:'bold'}}>Search for user here </Text>

           <View style={{flexDirection:'row',alignItems:'center',width:null,
           borderColor:'grey',borderRadius:20,padding:4,borderWidth:0.5,marginTop:0,
           marginRight:10,marginLeft:10}}>

              <TextInput
            value={this.state.search_text}
            keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
            onChangeText={(search_text) => {
              this.setState({ search_text : search_text },()=>{
                if(this.state.search_text.length > 0){
                  this.onSearch()
                }
                else{
                  this.setState({peoples:[]})
                }
              })
            }}
            style={{flex:5,height:40}}
            autoCapitalize='none'
            placeholderTextColor={'black'}
            placeholderTextColor={'grey'}
            placeholder={'Search here...'}
          />
                <TouchableOpacity onPress={()=> this.onSearch()}>
                  <View style={{flexDirection:'row',
                  alignItems:'center',backgroundColor:'blue',justifyContent:'center',paddingBottom:6,paddingTop:6,
                  paddingLeft:10,paddingRight:10,flex:1,borderRadius:15,margin:6}}>

                  <Text style={{color:'white'}}>Search</Text>

                  </View>

                  </TouchableOpacity>
                


           </View>

           {
             this.state.peoples.length > 0
             ?

                  
              <FlatList
              style={{width:'100%',padding:4}}
              data={this.state.peoples}
            // ItemSeparatorComponent = { this.FlatListItemSeparator }
              showsVerticalScrollIndicator={false}
              renderItem={(item,index) => this.renderItem(item,index)}
            />
            : 
            this.state.loading_status ?
            null
            :

                <View style={{justifyContent:'center',alignItems:'center',height:'60%'}}>
            <Image source={require('../assets/no-user.png')} 
              style={{width:200,height:200,alignSelf:'center',marginTop:Dimensions.get('window').height * 0.27}} resizeMode='contain'/>
              </View>
                

           }


      
             
    
            </KeyboardAwareScrollView>

    
          
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Search);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });