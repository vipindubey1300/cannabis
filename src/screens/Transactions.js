import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions} from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



class Transactions extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      transactions:[]
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
         
         
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                  <TouchableOpacity onPress={()=>{ 
                   // navigation.navigate(navigation.getParam('route_name','Home')) 

                         navigation.navigate('Home')
                    
                        navigation.dispatch(StackActions.reset({
                        index: 0,
                        key: 'HomePage',
                        actions: [NavigationActions.navigate({ routeName: 'Home' })],
                        }))
                       
                
                    
                    }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Cannabis Transactions</Text>
            </View>
    ),
  });

 

  
  getTransactions(){
                  var formData = new FormData();
                
                   formData.append('user_id',this.props.user.user_id);
                   this.setState({loading_status:true})

                 //  console.log("G-----",JSON.stringify(formData))
                      let url = urls.base_url +'api/transaction_list'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                      body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                
                               
                                     this.setState({transactions:responseJson.data})

                            }
                            else{

                             
                               
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
  }






  componentWillMount(){
      this.getTransactions()
  
  }

  checkSubscriptionType(item){
    if(item.subscription_type == 2){
      return(
        <Text>Investor</Text>
      )
    }
    else if(item.subscription_type == 3){
      return(
        <Text>Entrepreneur</Text>
      )
    }
    else if(item.subscription_type == 4){
      return(
        <Text>Chat </Text>
      )
    }
    else if(item.subscription_type == 5){
      return(
        <Text>News</Text>
      )
    }
    
  }

  //1 seller

//   subscription_type =2 subscribe  for investor, 
//   subscription_type =3 subscribe for enterpreneur, 
//   subscription_type =4 subscribe for chat,
// subscription_type =5 subscribe for news,
renderItem = ({item, index}) => {
    return(
      <TouchableOpacity onPress={()=> {}}>
      <View style={{width:Dimensions.get('window').width * 1,padding:6,marginBottom:2,marginTop:2}}>

      <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>

          {/* left*/}
          <View style={{flexDirection:'row',
          justifyContent:'flex-start',alignItems:'center',}}>

            


                     <Image source={require('../assets/dollar.png')}
                    style={{width:35,height:35}} resizeMode='contain'/>
            
               <View style={{marginLeft:10}}>
                 <Text style={{fontWeight:'bold'}}>Paid For : <Text style={{fontWeight:'100'}}>{this.checkSubscriptionType(item)}</Text></Text>
                 <Text style={{fontWeight:'bold'}}>Amount : <Text style={{fontWeight:'100'}}>$ {item.amt}</Text></Text>
                 <View style={{flexDirection:'row',alignItems:'center'}}>
                 <Text style={{fontWeight:'bold'}}>Pack Status :</Text>
                 
                 {
                   item.status == 1
                   ?
                   <Text style={{color:'green'}}> Active</Text>
                   :
                   <Text style={{color:'red'}}> Expired</Text>
                 }

                 </View>

                
               </View>
              

          </View>
           
                {/* right  end*/}
                <View>
                  <Text style={{fontWeight:'bold'}}>Date: <Text style={{fontWeight:'100'}}>{item.created_at.substring(0,11)}</Text></Text>
                </View>
      </View>

          
    


</View>
</TouchableOpacity>



    )
   }

   FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#aaa9ad",
          marginTop:3,
          marginBottom:3
        }}
      />
    );
  }

  
    render(){
        return(
          <SafeAreaView style={styles.container}>

    
    
            <ScrollView style={{backgroundColor:'#EBFAFF',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

           {
             this.state.transactions.length > 0
             ?

                  
              <FlatList
              style={{marginBottom:10,width:'100%',padding:4}}
              data={this.state.transactions}
               ItemSeparatorComponent = { this.FlatListItemSeparator }
              showsVerticalScrollIndicator={false}
              renderItem={(item,index) => this.renderItem(item,index)}
            />
                : 
                this.state.loading_status ?
                null
                :

                <View style={{justifyContent:'center',alignItems:'center',height:'90%'}}>
                  <Image source={require('../assets/no-transactions.png')} 
                    style={{width:200,height:200,alignSelf:'center',
                    marginTop:Dimensions.get('window').height * 0.27}} resizeMode='contain'/>
              </View>
                

           }


      
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Transactions);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });