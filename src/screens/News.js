import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {WaveIndicator} from 'react-native-indicators';
import { Overlay } from 'react-native-elements';




class News extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
       news:[],
       newsModalVisible:false
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
           
      
            
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{ navigation.navigate("HomePage") }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>News</Text>
     </View>
    ),
  });

  
  getNews(){
    this.setState({loading_status:true})
    var formData = new FormData();
 
    formData.append('user_id',this.props.user.user_id);


                      let url = urls.base_url +'api/news'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log("NEWS",JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                
                                   //attaching a key to all objects 
                                   var p = responseJson.data
                                   p.map(item=>{
                                     Object.assign(item,{smiley:false})
                                   })
   
                                  
                                  this.setState({news:p})
                               
                              

                            }
                            else{

                             
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                      //       Platform.OS === 'android' 
                      // ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      // : Alert.alert("Try Again !")

                      Snackbar.show({
                        title: 'Try Again',
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });

                      this.setState({loading_status:false})
                          });
  }


  checkNewsRenewel(){
   
    var formData = new FormData();
 
    formData.append('user_id',this.props.user.user_id);


                      let url = urls.base_url +'api/package_statuss'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
               
               console.log("NEWS",JSON.stringify( responseJson.status.news_flag))
          
                              if(!responseJson.error){
                                var chat_flag = responseJson.status.notify_chat
                                var seller_flag = responseJson.status.notify_seller
                                var investor_flag = responseJson.status.notify_investor
                                var entrepreneur_flag = responseJson.status.notify_entrepreneur
                                var news_flag = responseJson.status.notify_news


                                {
                                  news_flag == 1
                                  ?
                                  this.setState({
                                    newsModalVisible:true
                                  })
                                  : 
                                  null
                                }

                               
                               
                              

                            }
                            else{

                            
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                      //       Platform.OS === 'android' 
                      // ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      // : Alert.alert("Try Again !")

                      Snackbar.show({
                        title: 'Try Again',
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });

                         this.setState({loading_status:false})
                          });
  }



next(item){

  console.log("NEWSSSSSSSSss")

  let obj={
    'news_detail':item
  }

  this.props.navigation.navigate("NewsDetail",{result:obj})
}


likePosts(news_id,like_id){
  AsyncStorage.getItem("user_id").then((item) => {
    if (item) {
      var formData = new FormData();

      formData.append('user_id',item);
      formData.append('news_id',news_id);
      formData.append('like_type',like_id);
      

                    this.setState({loading_status:true})
                    let url = urls.base_url +'api/like_news'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                this.setState({loading_status:false})
                console.log(JSON.stringify(responseJson))
      //   ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                            if(!responseJson.error){

                              
                              var p = this.state.news
                              p.map(item=>{
                               if(item.id == news_id){
                                 var like = item.likes_count + 1
                                Object.assign(item,{like_flag:like_id,
                                  likes_count:(responseJson.likes_count == undefined || responseJson.likes_count == null ||
                                  responseJson.likes_count == 0 )?
                                like : responseJson.likes_count})
                               }
                              })

                             this.setState({news:p})
                          }
                          else{
                            var p = this.state.news
                            p.map(item=>{
                             if(item.id == news_id){
                               var like = item.likes_count + 1
                              Object.assign(item,{
                                likes_count:(responseJson.likes_count == undefined || responseJson.likes_count == null) ?
                              like : responseJson.likes_count})
                             }
                            })

                           this.setState({news:p})

                           
                            Snackbar.show({
                              title: responseJson.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})
                          
                        

                    Snackbar.show({
                      title: "Try Again !",
                      duration: Snackbar.LENGTH_SHORT,
                      backgroundColor:'black',
                      color:'red',
                      action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                      },
                    });


                        });
    }
    else{
      ToastAndroid.show("User not found !",ToastAndroid.LONG)
    }
  });
}





checkNewsStatus(){

  AsyncStorage.getItem("user_id").then((item) => {
    if (item) {
      var formData = new FormData();

      formData.append('user_id',item);
     
      

                    this.setState({loading_status:true})
                    let url = urls.base_url +'api/news_eligibility'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                this.setState({loading_status:false})
                //console.log(JSON.stringify(responseJson))
      //   ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                            if(!responseJson.error){

                              
                              this.props.navigation.navigate("AddNews")
                          }
                          else{

                           
                            this.props.navigation.navigate("NewsPackages")
                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})
                          
                        

                    Snackbar.show({
                      title: "Try Again !",
                      duration: Snackbar.LENGTH_SHORT,
                      backgroundColor:'black',
                      color:'red',
                      action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                      },
                    });


                        });
    }
    else{
      ToastAndroid.show("User not found !",ToastAndroid.LONG)
    }
  });

}



  componentWillMount(){
    this.checkNewsRenewel()
   this.getNews()
  
  }


  _checkCondition(item){
    let img;
    if(item.like_flag == 1){
        img = require('../assets/p1.png')
    }else if (item.like_flag  == 2){
      img = require('../assets/p2.png')
    }else if (item.like_flag  == 3){
      img = require('../assets/p3.png')
    }
    else if (item.like_flag  == 4){
      img = require('../assets/p4.png')
    }else{
      img = require('../assets/p1.png')
    }

    
    return img;   
};

renderItem = ({item, index}) => {
    return(
      <TouchableWithoutFeedback onPress={()=>{this.next(item)}}>
     

          <View style={{width:'100%',height:170 ,marginLeft:6,flex:1,backgroundColor:colors.color_primary,marginRight:6,
          marginBottom:20,
          marginTop:20,borderColor:'black'}}>

          <Text style={{color:'white',fontWeight:'bold',margin:3}}>Title : 
                <Text style={{color:'white',margin:3}}>{item.title}</Text>
          </Text>


           <ImageBackground
            source={{uri : urls.base_url + item.img}}
            resizeMode='stretch'
            style={{height:'100%'}}>

          
          
            

          

            <View style={{flexDirection:'row',
            justifyContent:'space-between',
            alignItems:'center',margin:4,height:null}}>


              {/* <Text style={{flex:0.7,color:'white',fontWeight:'bold',margin:3}}>{item.title}</Text> */}


              
          
            
            </View>

            <View style={{width:'55%',flexDirection:'row',alignItems:'center',
            justifyContent:'flex-start',margin:6}}>



                  <View style={{flexDirection:'row',width:'90%',alignItems:'center'}}>
                  <Image source={require('../assets/clock.png')}
                              style={{width:17,height:17,marginRight:6}} resizeMode='contain'/>

                  <Text style={{color:'white',fontSize:13}}>{item.created_at}</Text>
            
                </View>


                {/* <View style={{flexDirection:'row',width:'90%',alignItems:'center'}}>

                <Text style={{color:'black',fontSize:13,fontWeight:'bold'}}>Posted By :  </Text>

                  <Text style={{color:'white',fontSize:13}}>{item.posted_by}</Text>
            
                </View> */}


         </View>


        

           <Text style={{margin:5,color:'white',flex:1,fontWeight:'bold'
           }}>{item.description.toString().substring(0,70)} ...</Text>

           <View style={{width:'100%',backgroundColor:'white',
           height:1.5,marginTop:5,marginBottom:5}}></View>


           <View style={{width:'100%',flexDirection:'row',alignItems:'center',
           justifyContent:'space-between',margin:6,flex:0.3}}>

                  <TouchableOpacity 
                  activeOpacity={0.6}
                  style={{flexDirection:'row',alignItems:'center',marginTop:-10}}
                  onPress={()=> this.likePosts(this.state.news[index].id,1)}
                  onLongPress={()=> {
                   
                     this.state.news[index].smiley = true
                     this.setState({news:  this.state.news})
                  }}>
                  
                      <Image source={this._checkCondition(item)}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:10}}  />

                      <Text style={{color:'white'}}>{item.likes_count} likes</Text>

                      </TouchableOpacity>



                      <TouchableOpacity activeOpacity={0.6}
                   style={{flexDirection:'row',alignItems:'center'}}
                   onPress={()=> {}}
                  onLongPress={()=> {
                    
                  }}>
                  
                  <Text style={{color:'white'}}>{item.comment_count} comments</Text>

                      <Image source={require('../assets/comment.png')}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:15}}  />

                      

                      </TouchableOpacity>



           </View>

           


           {
                  item.smiley
                  ?
                  <View style={{position:'absolute',
                  bottom:10,left:10,
                  backgroundColor:'white',
                  width:null,flexDirection:'row',
                   alignItems:'center',borderRadius:10,
                  padding:5,borderWidth:1}}>
    

                  <TouchableOpacity onPress={()=> {
                    this.likePosts(this.state.news[index].id,1)
                    this.state.news[index].smiley = false
                    this.setState({posts:  this.state.news})
                  }}>
                  <Image source={require('../assets/p1.png')}
                  style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                  </TouchableOpacity>
                
                  <TouchableOpacity onPress={()=> {
                    this.likePosts(this.state.news[index].id,2)
                    this.state.news[index].smiley = false
                    this.setState({news:  this.state.news})
                  }}>
                  <Image source={require('../assets/p2.png')}
                  style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={()=> {
                    this.likePosts(this.state.news[index].id,3)
                    this.state.news[index].smiley = false
                    this.setState({news:  this.state.news})
                  }}>
                  <Image source={require('../assets/p3.png')}
                  style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                  </TouchableOpacity>


                  <TouchableOpacity onPress={()=> {
                    this.likePosts(this.state.news[index].id,4)
                    this.state.news[index].smiley = false
                    this.setState({news:  this.state.news})
                  }}>
                  <Image source={require('../assets/p4.png')}
                  style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                  </TouchableOpacity>


                  </View>
                  : null
                }
           </ImageBackground>

            </View>
          
      </TouchableWithoutFeedback>
    )
   }


  



   FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.4,
          width: "101%",
          backgroundColor: "grey",
        }}
      />
    );
  }

   
 

    render()
    {

      const NewsModal = ()=>{
        return(
         
          <Overlay
          isVisible={this.state.newsModalVisible}
          windowBackgroundColor="rgba(0, 0, 0, .5)"
          overlayBackgroundColor="white"
          width={300}
          height='auto'
          
        >
  
          <View style={{alignItems:'center',justifyContent:'center'}}>
                <View style={{padding:15,borderRadius:30,
                borderColor:'grey',borderWidth:1,marginTop:-35,
                backgroundColor:'white',elevation:4,shadowOpacity:0.5,marginBottom:15}}>
                <Image source={require('../assets/logo.png')} 
                  style={{width:30,height:30}} resizeMode='contain'/>
                
                </View>
  
                <Text style={{textAlign:'center',fontWeight:'bold',margin:6}}>Package Expired.</Text>
                <Text style={{textAlign:'center',color:'grey'}}>Please renew your News Package.</Text>
  
  
  
                <View style={{flexDirection:'row'}}>
                <TouchableOpacity onPress={()=> {
                  this.setState({newsModalVisible:false})
                  this.props.navigation.navigate('NewsPackages')
                 
                }}
                style={{backgroundColor:colors.color_primary,
                paddingLeft:20,
                paddingRight:20,
                paddingTop:6,
                paddingBottom:6,
                alignItems:'center',justifyContent:'center',margin:10}}>
                    <Text style={{color:'white',fontWeight:'bold'}}>Renew Now</Text>
  
                </TouchableOpacity>
  
                <TouchableOpacity onPress={()=> {
                  this.setState({newsModalVisible:false})
                 
                }}
                style={{backgroundColor:'red',
                paddingLeft:30,
                paddingRight:30,
                paddingTop:6,
                paddingBottom:6,
                alignItems:'center',justifyContent:'center',margin:10}}>
                    <Text style={{color:'white',fontWeight:'bold'}}>Cancel</Text>
  
                </TouchableOpacity>
          </View>
          </View>
        </Overlay>
        )
      }
  
      
        return(
          <SafeAreaView style={styles.container}>
          <NewsModal/>

            <ScrollView style={{backgroundColor:'#EBFAFF',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>




           
          <FlatList
          style={{marginBottom:10,width:'100%',padding:4}}
          data={this.state.news}
         // ItemSeparatorComponent = { this.FlatListItemSeparator }
          showsVerticalScrollIndicator={false}
          renderItem={(item,index) => this.renderItem(item,index)}
        />
             
    
            </ScrollView>

              {/* floating action button */}
                     
              <View style={{position:'absolute',bottom:20,right:10,left:10,}}>
                    
                  <TouchableOpacity style={{width:'100%'}} onPress={()=> {
                    this.checkNewsStatus()
                  }}>
                    <View style={{width:'100%',height:null,padding:10,
                  borderColor:'grey',borderWidth:0.5,borderRadius:6,alignSelf:'center',
                  justifyContent:'center',alignItems:'center',marginBottom:15,backgroundColor:colors.color_primary,marginTop:15,alignSelf:'center'}}>
                    <Text style={{color:'white'}}>Publish Your Cannabis Related News Item +</Text>
                  </View>
                  </TouchableOpacity>
              </View>

             {/* floating action button  end*/}

              
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(News);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });