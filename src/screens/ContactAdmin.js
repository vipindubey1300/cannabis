import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import DateTimePicker from 'react-native-modal-datetime-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { throwStatement } from '@babel/types';



class ContactAdmin extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      subject:'',
      message:''
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
            
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{ navigation.navigate("HomePage") }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Contact Admin</Text>
     </View>
    ),
  });

   foo =  (id,name,email,image) => {
  
    this.props.change({ user_id : id, name : name, image:image, email:email });
   }
  
  save1 = async () => {
   
    await this.foo("2","sadf","sadfksdfllflflfl","asdfdsf")
  
    let a = JSON.stringify(this.props.user)
    console.log("val----gggg-------",a)  
  }


isValid() {
    const {subject,message } = this.state;

    let valid = false;

    if (subject.toString().trim().length > 0 
      &&  message.toString().trim().length > 0
      ) {
      valid = true;
    }



    else if (subject.toString().trim().length == 0){
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter subject', ToastAndroid.SHORT)
      //   : Alert.alert('Enter subject')

      Snackbar.show({
        title: "Enter subject !",
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
      });
      return false;
    }

    else if (message.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter message', ToastAndroid.SHORT)
      //   : Alert.alert('Enter message')

        Snackbar.show({
          title: "Enter message !",
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
        });
      return false;
    }
 
    

    return valid
  }

 



  contactAdmin(){
    console.log("Get profile")
   if(this.isValid()){
     AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       var formData = new FormData();

        formData.append('user_id',item);
        formData.append('message', this.state.message);
        formData.append('subject', this.state.subject);
     
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/contact_admin'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);

          


                              if(!responseJson.error){
                               
                                //   Platform.OS === 'android' 
                                // ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                // : Alert.alert(responseJson.message)

                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });

                                this.props.navigation.navigate("HomePage")

                               
                            }
                            else{

                             
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });



                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
   }
    
  }



 async componentWillMount(){
  

  
  }



   
 

    render()
    {


  
      
        return(
          <SafeAreaView style={styles.container}>

    

         
    
    
            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

  <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' ,marginBottom:30,marginTop:30}}>
  

               <TextField
              label='Subject'
              value={this.state.subject}
              onSubmitEditing = {() => this.message.focus()}
              onChangeText={(subject) => this.setState({ subject})}
              ref={(input) => { this.subject = input; }}
            
            />

            <TextField
            label='Message'
            value={this.state.message}
            multiline={true}
            onChangeText={(message) => this.setState({ message})}
            ref={(input) => { this.message = input; }}
            onSubmitEditing = {() => this.contactAdmin()}
          />



          </View>


          <TouchableOpacity style={{width:'100%'}} onPress={() =>{this.contactAdmin()}}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 50, borderRadius: 5,
              shadowColor: 'gray',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Send</Text>
            </View>
          </TouchableOpacity>


         
    
    
             
    
            </ScrollView>

             
    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ContactAdmin);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });