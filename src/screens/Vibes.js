import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import Snackbar from 'react-native-snackbar';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



class Vibes extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      users:[],
    
     
    };


  }

  
  static navigationOptions = ({ navigation }) => {
    return {
       header: () => null
    } 
}

  





  getVibes(id){
    console.log("Get profile")
   AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       var formData = new FormData();

        formData.append('user_id',id);
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/vibes_list'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){
                              

                                this.setState({
                                  users:responseJson.data
                                })

                                var count = responseJson.count
                                let t = count + " Vibes"
                                this.props.navigation.setParams({title: t });


                            }
                            else{
                               let t ="0 Vibes"
                                this.props.navigation.setParams({title: t });

                             
                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'UNDO',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: "Try Again",
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'UNDO',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
    
  }


  vibeUser(user_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       
  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('follow_user_id',user_id);
        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/vibe_user'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(formData))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
            //  Platform.OS === 'android' 
            //                     ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
            //                     : Alert.alert(responseJson.message)



                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
  
  
                              if(!responseJson.error){
  
                                var p = this.state.users
                                p.map(item=>{
                                 if(item.id == user_id){
                                
                                  Object.assign(item,{vibing_flag:1})
                                 
                                 }
                                })
                                this.setState({users:p})
                            }
                            else{
  
                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }
  unVibeUser(user_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       
  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('follow_user_id',user_id);
        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/unvibe_user'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
            //  Platform.OS === 'android' 
            //                     ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
            //                     : Alert.alert(responseJson.message)



                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
  
  
                              if(!responseJson.error){
  
                                var p = this.state.users
                                p.map(item=>{
                                 if(item.id == user_id){
                                
                                  Object.assign(item,{vibing_flag:0})
                                 
                                 }
                                })
                                this.setState({users:p})
                            }
                            else{
  
                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  componentWillMount(){
    // let t = "The window title";
    // this.props.navigation.setParams({title: t });
  
    console.log("NAVIGATION-----",this.props.navigation.dangerouslyGetParent().getParam('id'))
    let id = this.props.navigation.dangerouslyGetParent().getParam('id')
    this.getVibes(id)
  }

  next(user_id){
    //this.props.navigation.navigate("SellerProfile")

  
  
      let obj={
        'user_id':user_id
      }
        this.props.navigation.navigate("SellerProfile",{result:obj})
    
    
   
 }



  renderItem = ({item, index}) => {
    return(
      <TouchableWithoutFeedback onPress={()=>{this.next(item.user_id)}}>
      <View style={{width:'100%',padding:6,marginBottom:2,marginTop:2}}>

       <View style={{width:'100%',height:null,flexDirection:'row',
      justifyContent:'space-between',alignItems:'center'}}>

                  <View style={{height:null,flexDirection:'row',
                    justifyContent:'flex-start',alignItems:'center'}}>
                    
    


                    <Image source={{uri:urls.base_url + item.user_pic}}
                     style={{width:30,height:30,borderRadius:15,overflow:'hidden'}} 
                     resizeMode='cover'/>

                     {
                       item.seller_flag == 1
                       ?
                       <View style={{position:'absolute',bottom:20,left:16}}>
                         <Image source={require('../assets/tag.png')} 
                                  style={{width:20,height:20}} resizeMode='contain'/>

                   </View>

                          :null
                     }

               
                    {
                  
                  <Text style={{marginLeft:15}}>{item.user_name}</Text>
                    }
                  
                    </View>



                    <TouchableOpacity onPress={()=> {this.vibeUser(item.user_id)}}>
                    <View style={{padding:6,height:null,width:null,
                      justifyContent:'center',alignItems:'center',
                      backgroundColor:item.vibing_flag == 0 ?  '#CDCDCD' : null,
                      margin:3,flex:1.6}}>
                      {
                        item.vibing_flag == 0
                        ?  <Text>Vibe +</Text>
                        :  
                        <View style={{flexDirection:'row',
                      justifyContent:'flex-start',
                      alignItems:'center',marginLeft:3,marginRight:-5}}>
                  {/* <TouchableOpacity onPress={()=>{this.unVibeUser(item.user_id)}}>
                    <View style={{justifyContent:'center',
                    alignItems:'center',
                    paddingBottom:5,
                    paddingTop:5,
                    paddingLeft:10,
                    paddingRight:10,backgroundColor:'red'}}>
                    <Text style={{color:'white'}}>Unvibe</Text>

                    </View>
                    </TouchableOpacity> */}


                    <TouchableOpacity onPress={()=>{}}>
                    <View style={{justifyContent:'center',
                    alignItems:'center',
                    paddingBottom:5,
                    paddingTop:5,
                    paddingLeft:10,
                    paddingRight:10,backgroundColor:'green',marginLeft:10}}>
                    <Text style={{color:'white'}}>Vibing</Text>

                    </View>
                    </TouchableOpacity>
            

            </View>
                      }
                     
                    </View>
                    </TouchableOpacity>
    
       </View>

      



     </View>

  
     
      </TouchableWithoutFeedback>
    )
   }

   
   FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "101%",
          backgroundColor: "#aaa9ad",
          margin:3
        }}
      />
    );
  }
 

    render()
    {


  
      
        return(
          <SafeAreaView style={styles.container}>

    

            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>


       
{
             this.state.users.length > 0
             ?

                  
                            
                <FlatList
                style={{marginBottom:10,width:'100%',padding:4}}
                data={this.state.users}
                ItemSeparatorComponent = { this.FlatListItemSeparator }
                showsVerticalScrollIndicator={false}
                renderItem={(item,index) => this.renderItem(item,index)}
              />

                : 
                this.state.loading_status ?
                null
                :

                <View style={{justifyContent:'center',alignItems:'center',height:'90%'}}>
                  <Image source={require('../assets/no-vibes.png')} 
                    style={{width:200,height:200,alignSelf:'center',
                    marginTop:Dimensions.get('window').height * 0.27}} resizeMode='contain'/>
              </View>
                

           }



            </ScrollView>

            
    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Vibes);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });