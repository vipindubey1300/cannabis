import Snackbar from 'react-native-snackbar';


export const showMessage = async (message) => {
  Snackbar.show({
    title: message,
    duration: Snackbar.LENGTH_SHORT,
    backgroundColor:'black',
    color:'red',
    action: {
      title: 'OK',
      color: 'green',
      onPress: () => { /* Do something. */ },
    },
  });
}
