import firebase from 'react-native-firebase';


var firebaseConfig = {
  apiKey: "AIzaSyDp_eNxU3ihsZDLlfoohqdHsPcFjMF7OAY",
  authDomain: "cannabis-6cd74.firebaseapp.com",
  databaseURL: "https://cannabis-6cd74.firebaseio.com",
  projectId: "cannabis-6cd74",
  storageBucket: "cannabis-6cd74.appspot.com",
  messagingSenderId: "548315214056",
  appId: "1:548315214056:web:356bd9e20cff61af85d95d"
};

class FirebaseSDK {
  constructor() {
    if (!firebase.apps.length) {
      //avoid re-initializing
      firebase.initializeApp(firebaseConfig);
      console.log("YES app")
    }
    else{
      console.log("NO app")
    }
  }
 
}

// login = async (user, success_callback, failed_callback) => {
//   await firebase
//     .auth()
//     .signInWithEmailAndPassword(user.email, user.password)
//     .then(success_callback, failed_callback);
// };



createAccount = async user => {
  firebase
    .auth()
    .createUserWithEmailAndPassword(user.email, user.password)
    .then(
      function() {
        console.log(
          'created user successfully. User email:' +
            user.email +
            ' name:' +
            user.name
        );
        var userf = firebase.auth().currentUser;
        userf.updateProfile({ displayName: user.name }).then(
          function() {
            console.log('Updated displayName successfully. name:' + user.name);
            alert(
              'User ' + user.name + ' was created successfully. Please login.'
            );
          },
          function(error) {
            console.warn('Error update displayName.');
          }
        );
      },
      function(error) {
        console.error('got error:' + typeof error + ' string:' + error.message);
        alert('Create account failed. Error: ' + error.message);
      }
    );
};

// uploadImage = async uri => {
//   console.log('got image to upload. uri:' + uri);
//   try {
//     const response = await fetch(uri);
//     const blob = await response.blob();
//     const ref = firebase
//       .storage()
//       .ref('avatar')
//       .child(uuid.v4());
//     const task = ref.put(blob);

//     return new Promise((resolve, reject) => {
//       task.on(
//         'state_changed',
//         () => {

//         },
//         reject
//         () => resolve(task.snapshot.downloadURL)
//       );
//     });
//   } catch (err) {
//     console.log('uploadImage try/catch error: ' + err.message);
//   }
// };

updateAvatar = url => {

  var userf = firebase.auth().currentUser;
  if (userf != null) {
    userf.updateProfile({ avatar: url }).then(
      function() {
        console.log('Updated avatar successfully. url:' + url);
        alert('Avatar image is saved successfully.');
      },
      function(error) {
        console.warn('Error update avatar.');
        alert('Error update avatar. Error:' + error.message);
      }
    );
  } else {
    console.log("can't update avatar, user is not login.");
    alert('Unable to update avatar. You must login first.');
  }
};



const firebaseSDK = new FirebaseSDK();
export default firebaseSDK;


/**
 * There are multiple ways of doing this, since state update is a async operation, so to update the state object, we need to use updater function with setState.

1- Simplest one:

First create a copy of jasper then do the changes in that:

this.setState(prevState => {
  let jasper = Object.assign({}, prevState.jasper);  // creating copy of state variable jasper
  jasper.name = 'someothername';                     // update the name property, assign a new value                 
  return { jasper };                                 // return new object jasper object
})
Instead of using Object.assign we can also write it like this:

let jasper = { ...prevState.jasper };
2- Using spread operator:

this.setState(prevState => ({
    jasper: {                   // object that we want to update
        ...prevState.jasper,    // keep all other key-value pairs
        name: 'something'       // update the value of specific key
    }
}))
Note: Object.assign and Spread Operator creates only shallow copy, so if you have defined nested object or array of objects, you need a different approach.

Updating nested state object:
Assume you have defined state as:

this.state = {
  food: {
    sandwich: {
      capsicum: true,
      crackers: true,
      mayonnaise: true
    },
    pizza: {
      jalapeno: true,
      extraCheese: false
    }
  }
}
To update extraCheese of pizza object:

this.setState(prevState => ({
  food: {
    ...prevState.food,           // copy all other key-value pairs of food object
    pizza: {                     // specific object of food object
      ...prevState.food.pizza,   // copy all pizza key-value pairs
      extraCheese: true          // update value of specific key
    }
  }
}))
Updating array of objects:
Lets assume you have a todo app, and you are managing the data in this form:

this.state = {
  todoItems: [
    {
      name: 'Learn React Basics',
      status: 'pending'
    }, {
      name: 'Check Codebase',
      status: 'pending'
    }
  ]
}
To update the status of any todo object, run a map on the array and check for some unique value of each object, in case of condition=true, return the new object with updated value, else same object.

let key = 2;
this.setState(prevState => ({

  todoItems: prevState.todoItems.map(
    el => el.key === key? { ...el, status: 'done' }: el
  )

}))
Suggestion: If object doesn't have a unique value, then use array index.
 */