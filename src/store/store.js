import { createStore } from "redux";
import rootReducer from "../reducers/rootReducer";


//redux is for redux  function only

// react-redux is a Redux binding for React.
// It’s a small library for connecting Redux and React in an efficient way.
//the most important method you’ll work with is connect.

// the state in redux comes from reducers.
//  Let’s make it clear: reducers produce the state of your application

const store = createStore(rootReducer);


//state of the whole application lives inside the store
//create a store for wrapping up the state

export default store;
