

export function addUser(payload) {
  return { type: "ADD_USER", payload }
};

export function removeUser(payload) {
  return { type: "REMOVE_USER", payload }
};

export function changeUser(payload) {
  return { type: "UPDATE_USER", payload }
};

export function investorUser() {
  return { type: "INVESTOR_USER" }
};
export function entrepreneurUser() {
  return { type: "ENTREPRENEUR_USER" }
};

//type property is nothing more than a string
//reducer will use that string to determine how to calculate the next state.

//payload is data with action send to reducer


// the Redux store is like a brain: it’s in charge for orchestrating all the moving parts in Redux
// the state of the application lives as a single, immutable object within the store
// as soon as the store receives an action it triggers a reducer
// the reducer returns the next state